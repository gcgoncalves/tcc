#!/usr/bin/perl
no utf8;
use bytes;
use DB_File;

my ($bindir, $sep) = $0 =~ /^(.*)(\\|\/).*/;
$binDir = $bindir;

$ENV{PATH} = "$bindir:$bindir$sep../../bin:$ENV{PATH}";
$ENV{DANGRAM_BIN} = "$bindir";
$ENV{DANGRAM_ETC} = $etcdir = $etcDir = "$bindir$sep../etc";
$ENV{DANGRAM_LEX} = $lexdir = $lexDir = "$bindir$sep../lex";


LoadPorStat();
#LoadPorVal();
#LoadPorOrto();
#LoadPorFrames();

sub LoadPorVal() {
  my $TA = tie %porval, "DB_File", "$lexDir/por.val.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/por.val") || open(FH, "< /home/eckhard/portpars/PALTRAD.atom");
  while (<FH>){

    #    if (! /^$/ && ! /^\#/) {
    if (/^(.+?) (.+)/) { # reading from por.val
      $porval{$1} =$2;
    }
    elsif (m/^(.*?)\#.*?\#(.*?)[0-9]+$/) { # reading from PALTRAD.atom
      $word =$1;
      $tags =$2;
      $word =~ s/-$//;
      $tags =~ s/<b-/</; # abbreviations
      $tags =~ /^<([a-zA-Z]+)/; $pos =$1;
      if ($pos =~ /^(v|ADV|PRP|IN|KS|KC|NUM)/) {$pos = "\U$1";}
      elsif ($pos =~ /^s/) {$pos = "N";}
      elsif ($pos =~ /^n/) {$pos = "PROP";}
      elsif ($pos =~ /^a/) {$pos = "ADJ";}
      $tags =~ s/\./></; # isolate sem for nouns
      $tags =~ s/\^([^v])/><$1/g;
      $tags =~ s/<=.*?=>//;
      $porval{$word . "_$pos"} = $tags; # separate valency dictionary with one line per word class
      
#      print "--$word $pos $tags\n";
    }
  }
  return $porval;
}

sub LoadPorStat() {
  my $ptfreq;
  my $TA = tie %ptfreq, "DB_File", "$lexDir/pt_stat_basepos.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/pt_stat_basepos") || open(FH, "</home/eckhard/parsers/por/lex/pt_stat_basepos");
  while (<FH>) {
    if (! /^\#/ && /^(.*?) (.*)$/) {
      $ptfreq{$1} =$2;
    }
  }
  open(FH, "<$lexDir/pt_stat_fullform") || open(FH, "</home/eckhard/parsers/por/lex/pt_stat_fullform") ;
  while (<FH>) {
    if (! /^\#/ && /^(.*?) (.*?)$/) {
      $ptfreq{$1} =$2;
#      if (/^light/) {print "--$1 $2 " . $ptfreq{$1} . "\n";}
    }
  }

  my $brfreq;
  my $TB = tie %brfreq, "DB_File", "$lexDir/br_stat_basepos.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/br_stat_basepos") || open(FH, "</home/eckhard/parsers/por/lex/br_stat_basepos");
  while (<FH>) {
    if (! /^\#/ && /^(.*?) (.*)$/) {
      $brfreq{$1} =$2;
    }
  }
  open(FH, "<$lexDir/br_stat_fullform") || open(FH, "</home/eckhard/parsers/por/lex/br_stat_fullform") ;
  while (<FH>) {
    if (! /^\#/ && /^(.*?) (.*?)$/) {
      $brfreq{$1} =$2;
#      if (/^light/) {print "--$1 $2 " . $brfreq{$1} . "\n";}
    }
  }

  my $ptbrfreq;
  my $TC = tie %ptbrfreq, "DB_File", "$lexDir/ptbr_stat_basepos.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  %ptbrfreq = %ptfreq;
  foreach (keys %brfreq) {
    if (! $ptfreq{$_}) {$ptbrfreq{$_} =$brfreq{$_};}
    else {$ptbrfreq{$_} =$ptfreq{$_} + $brfreq{$_};}
  }

  open(FH, "<$lexDir/ptbr_stat_formpos") || open(FH, "</home/eckhard/parsers/por/lex/ptbr_stat_formpos") ; # both pt and br
  while (<FH>) {
    if (! /^\#/ && /^(.*?) (.*?)$/) {
      $ptbrfreq{$1} =$2;
#      if (/^light/) {print "--$1 $2 " . $ptbrfreq{$1} . "\n";}
    }
  }
  return $ptfreq, $brfreq, $ptbrfreq;
}

sub LoadPorOrto() {
  my $ptorto;
  my $TA = tie %ptorto, "DB_File", "$lexDir/pt_orto.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/portuguese") || open(FH, "</home/eckhard/dict/portuguese");
  while (<FH>) {
    chop;
    $ptorto{$_} =1;
#    print "--$_\n";
  }
  my $brorto;
  my $TA = tie %ptorto, "DB_File", "$lexDir/br_orto.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/brazilian") || open(FH, "</home/eckhard/dict/brazilian");
  while (<FH>) {
    chop;
    $brorto{$_} =1;
#    print "--$_\n";
  }
  return $ptorto, $brorto;
}

sub LoadPorFrames() {
  my $frames;
  my $TA = tie %frames, "DB_File", "$lexDir/framelex_pt.db", O_CREAT|O_RDWR|O_TRUNC|O_EXCL;
  open(FH, "<$lexDir/framelex_pt");
  while (<FH>) {
#    chop;
    if (/^(.+_[A-Z]+) (<.+)$/) {
      $frames{$1} =$2;
#      print "--$_\n";
    }
  }
  return $frames;
}

