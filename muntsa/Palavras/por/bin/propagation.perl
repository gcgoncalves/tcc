#!/usr/bin/perl -w
$|=1;
while (<>) {

    s/�\+\@/\@/g; # tilf�jning af manglende @tags
    if (s/ �\+mente//) {
	s/ADJ/<mente> ADJ/;
    }
    s/<mente> <mente>/<mente> /; # should be superfluous, wher does the second 'mente' come from???

# EUA-China

    if (s/ �ADJ//) {
	s/ (N|PROP)[A-Z ]+/ ADJ M\/F S\/P/g;
    }

# general replace operator

    if (s/ �([^= ]+)=>([^ \n]+)//) {
	$gammel = $1;
	$ny = $2;
	s/$gammel/$ny/;
    }
    s/\" (.*) �(<[^ <>]+>)/\" $2 $1/; # moves newly mapped <...> to its right place
    s/\" (.*) �(<[^ <>]+>)/\" $2 $1/; # moves newly mapped <...> to its right place
    if (/\"que\".*\@&[^\"\[]+\@/) {
	s/( \@&[^\"\[]+)( \@[^ \n]+)/$2$1/; # new @-readings for 'que' SPEC with only @&
    }
    if (/SPEC .*\@SUB \@\&/) {
	s/<rel> SPEC [^\330\@]+\@SUB \@\&FS-N</KS !�41968� \@SUB \@\&FS-<ADVL/; # Botava-se um pouco de farinha, ou ra��o - que agora usa-se mais a ra��o
    }
# PRED within np
    if (s/�N<PRED//g) {s/\@<PRED/\@N<PRED/;}

# attributive participle clauses
    if (s/ <ap>//g) {
#	s/V PCP/V ICLPCP/; # not any more ...
    } 
    if (s/�pcp-icl//g) {
	s/\@([^ &]+)/\@\&ICL-$1/g; # all @-readings as @&ICL-readings
	s/\@/\@IMV \@/; # 1 @IMV
    }
    if (s/ �\@A<ADVL//) {s/\@A</\@A<ADVL/;}

    if (s/ �PS//g) {s/PR\/PS/PS/;}
    if (s/ �PR//g) {s/PR\/PS/PR/;}

    if (s/ �PCP0//g) {s/ M S//;}

    if (s/ �M//g) {s/M\/F/M/;}
    if (s/ �F//g) {s/M\/F/F/;}
    if (s/ �S//g) {s/S\/P/S/;}
    if (s/ �P//g) {s/S\/P/P/;}
    if (s/ �3S//g) {s/3S\/P/3S/;}
    if (s/ �3P//g) {s/3S\/P/3P/;}

### first (noun) part of polylexical proper noun
    if (/\"[^=]*(Carlos|Jorge|al|ar|�|�|el|eu|[^�]o|on|or|uiz*|um)[=].*PROP M\/F/) {s/M\/F/M/; s/S\/P/S/;} # Ciro=Gomes
    if (/\"[^=]*(a|ade|��o|d�o|i�o|s�o)[=].*PROP M\/F/) {s/M\/F/F/; s/S\/P/S/;} # Maria=Bernadete

    if (/\"O=.*PROP M\/F/) {s/M\/F/M/; s/S\/P/S/;} # Ciro=Gomes
    if (/\"Os=.*PROP M\/F/) {s/M\/F/M/; s/S\/P/P/;} # Ciro=Gomes
    if (/\"A=.*PROP M\/F/) {s/M\/F/F/; s/S\/P/S/;} # Ciro=Gomes
    if (/\"As=.*PROP M\/F/) {s/M\/F/F/; s/S\/P/P/;} # Ciro=Gomes

### second (adjectival) part of polylexical proper noun
    if (/=[^=]*(eu|o|or)\".*PROP M\/F/) {s/M\/F/M/; s/S\/P/S/;} # Maria=Bernadete
    if (/=[^=]*([e�]ia|o[nrs]?a)\".*PROP M\/F/) {s/M\/F/F/; s/S\/P/S/;} # ..=Europeia, ikke: MCM=�frica

    if (s/ �INF1S//g) {s/0\/1\/3S/1S/;}
    if (s/ �INF3S//g) {s/0\/1\/3S/3S/;}
    if (s/ �INF0S//g) {s/ 0\/1\/3S//;}

    if (s/ �V1S//g) {s/1\/3S/1S/;}
    if (s/ �V3S//g) {s/1\/3S/3S/;}


    if(s/ �ACC//g) {s/ACC\/DAT/ACC/;}
    if(s/ �DAT//g) {s/ACC\/DAT/DAT/;}
    if(s/ �NOM//g) {s/NOM\/PIV/NOM/;}
    if(s/ �PIV//g) {s/NOM\/PIV/PIV/;}

    if ((/ista[\]\"]/ && /[ <](attr|prof|H)[> ]/ || /DERS -ista/) && /\@(N<(PRED)*|>N|<SC|SC>|<PRED|PRED>)/) {
	s/ N / ADJ /; # -ista as adjective 
	s/<(attr|prof|H|)> //g;
    }

### revise too-early from-N conclusions in haremE.cg

    if (/<prop>/) {
      if (/<HH>/ && ! /<NER:(org|suborg|media|admin|party|group)/) {s/<NER:.*?>/<NER:org>/;}
    }

    s/  +/ /g;
    print;
}





