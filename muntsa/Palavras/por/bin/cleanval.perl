#!/usr/bin/perl -w
# buffer =0:
$|=1;
while (<>) {
  s/( <[^> ]+->)(.*?=>)/$2$1/; # protects e.g. <�pera-> (through movement) against operation in next line
  s/ <.*? <=.*?=>( .*?<=.*?=>)/$1/; # imagem-ficha --> sems maintained only for 1. noun !!
  s/ \<ud>//g; # only relevant for morphology
  s/(\<vq>(\<\+interr>)*)/$1 <v-cog> <vq>/g;
    s/\"[^\" ]+\" +\"/\"/; # "haver" "h�" PRP efter REPLACE

#    if (/istas*[ \t]/ && /[ <](H?attr|H?prof|H)[> ]/ && /\@(N<(PRED)*|>N|<SC|SC>|<PRED|PRED>)/) { # har aldrig virket pga. manglende \" efter base form, hvis den overhovedet skal virke, skal <Hideo> etc. med, og der skal checkes for om ordet har dependenter der indikerer N-l�sning alligevel
    if (/[^\-]ista[\"\]] / && /\@(N<|<PRED|PRED>)/ && s/<(Hattr|Hprof|Hideo|H)>/<attr> <h> <jh>/g) { #

	s/ N / ADJ /; # -ista as adjective 
    }

# beskytter <...> i PROP

    if (/PROP/) {s/<([a-zA-Z]+)>/<NAME$1>/g;}

# clean_val

# tilf�jer tags for refleksivitet-disambiguering

    if (/ [12][SP] ACC/) {
	s/PERS/<refl> PERS/;
    }
# tilf�jer tags for instantieret HUM-subjekt (<sH>) og NON-HUM-subjekt (<sN>)
    if (/ <v(H|N)> /) {;}
    else    {s/( V .*\@.MV)/ <sH> <sN> <sA>$1/g;}
# tilf�jer tag for inkorporeret personalpronomen
    s/( V .* VFIN )/ <ink>$1/; # ???
# tilf�jer tag for inkorporeret verb i konjunktion
    if (/\@SUB/ && ! /\@&FS/ && ! /\@SUBJ/) {
	s/\@SUB/<el-verb> \@SUB/g; # but: always creates <el-verb> if used with visl-verb-as-head-notation
    }
# tilf�jer tags for instantieret HUM-NP-HEAD (<jh>) og NON-HUM-NP-HEAD (<jn>) for adjektiver
    if (/ <(j?h|nh|j?a|j?b)> /) {
        s/<h>/<h> <jh>/g;
        s/<nh>/<nh> <jn>/g;
    }
    else {s/( <*ADJ>* )/ <jh> <jn>$1/g;}
# tilf�jer <hum> til que-rel
    s/(\"que\".*<rel>)(.*SPEC.*\@\&FS)/$1 <que-hum> <que-n>$2/g;
# tilf�jer finite main clause mulighed for finitte verber
    if (! /@[&\#]/) {
	if (/(\@F(MV|AUX))/) {s/\" /\" <fmc> /g;}
    }
    else {
#	s/FS-ACC>>/FS-QUOTE>/g;
#	s/FS-<<ACC/FS-<QUOTE/g;
    }
# tilf�jer passive marker potential for participles
    s/( V PCP [^\@]+ \@(IAUX|IMV) \@\&ICL-AUX<)/ <pass>$1/g;

# tilf�je tegns�tningsordklasse
    s/^(\"<(\$[^0-9a-zA-Z]*)>\")/$1\n \"$2\" PU/g; # ikke: numbers

# protect syntax-@

s/(( <.*?>)+)( [A-Z].*?)( \@.*)/$3$4$1/g;;
s/\@/\@�/g;

# indf�je atomare semer
    s/<\=(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.)(.*=>)/\@\=$1 \@\=$2 \@\=$3 \@\=$4 \@\=$5 \@\=$6 \@\=$7 \@\=$8 \@\=$9 \@\=$10 \@\=$11 \@\=$12 \@\=$13 \@\=$14 \@\=$15 \@\=$16 <\=$17/;
    while (/<\=./) {
	s/<\=\=>//;
	s/<\=(.)/\@\=$1 <\=/;
    }
# mark semtags for second round of CG, portcg.sem

if (/ N /) {
    s / <([ABHLV].*?|an[a-z]*?|(a[mcx]|act|activity|amount|bar|build|c[cm]|clo[AH]|coll?|conv?|cord|cur|dance|dir|domain|drink|dur|event|f|food|fight|fruit|furn|game|genre|geom|group|inst|ism|ling|mach|mat|meta|mon|month|occ|per|part|piece|percep|pict|pos|process|sem|sick|sit|state|sport|talk|temp|therapy|tool|tube|unit|wea)(-[a-z]+?)?)>/ \@�$1/g;
}



# g�r valens-tags synlig for CG
    s/ (<DER[SP]) / $1_/g; # make <DERS -al [ATTR]> one tag
    s/(_[^ ><]+) \[/$1_\[/g;

    s/ <([^> ]+)>/ \@%$1/g;

# clean_val3

#    s/\" (.*)( (N|PROP|SPEC|DET|PERS|V*ADJ|V*ADV|V|NUM|PRP|KC|KS|IN|V*PP|VNP|PU) [^\@]*)\@/\"$2\ \@�$3 $1 \@/g; # standard
#    s/\" (.*)( (N|PROP|SPEC|DET|PERS|V*ADJ|V*ADV|V|NUM|PRP|KC|KS|IN|V*PP|VNP|PU) [^\@]*)\@/\"$2 $1 \@/g; # new

# make KC og 'como' i 'tanto...como' mappable
    if (/( KC |como.*parkc-2)/ && ! /[<%]DEP:/) { # not for already dep'ized input
      if (! /\"nem\"/) {s/ \@\%parkc-[12]//g;} # these are mapped in portval.latin (avoid doubling) -- but they can't be mapped on 'nem' which has <nao+KOMMA> as a @ tag!
      s/(\@�|��)/&/g;
    }
    print;
}

