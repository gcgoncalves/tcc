#!/usr/bin/perl -w
$|=1;
$prefunc ="";

while (<>) {
    if (/<hyfen>/ && ! /^[^ \t]+\-[ \t]/) {
	s/^([^ \t]+)([ \t])/$1-$2/g;
    }

    s/^\n//;
    s/^(entre)=([^= \t]+)=(e)=([^= \t]+).*(\[[^ =\]]+)[^ \]]+(\]( +<\*>)*).*PP.*(\@[^ \n]+).*/$1\t$5$6 PRP $8\n$2\t\[$2\] NUM M\/F P \@P<\n$3\t\[$3\] <co-prparg> KC \@CO\n$4\t\[$4\] NUM M\/F P \@P</g; # Entre 1999 e 2000 aconteceu ...
    s/^([^= \t]+)=(a|para)=([^= \t]+).*(\[[^ \]=]+)[^ \]]+(\]( +<\*>)*).*<num_[apr]+_num.*((NUM.*) \@[^ \n]+).*/$1\t$4$5 $8 \@P<\n$2\t\[$2\] PRP $prefunc\n$3\t\[$3\] $7/g; # Subir de 50 a 60 por cento
    m/(\@[A-Z<>]+)/;
    $prefunc=$1;


    if (/\*/) {
	s/\*([a-z])/\U$1/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
	s/\*�/�/g;
    }
    if (/<\*>/) {
	s/<\*>//g; # fjerner ALLE <*> fra linjen, ogs� ved flere l�sninger
	$_ = ucfirst($_);
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
	s/^�/�/;
    }
#    if (/ALT xxx/) {s/\[[^\]]+\] /\[xxx\] /g;} # replaces unknown words' base forms
    if (/<DER/ && /^([^ ]+) ALT xxx/) {
	$word = $1;
	if (! / V /) {
	    s/\[[^\]]+\] /\[$word\] /g;  # replaces unknown words' base forms
	    s/s(\][^\t]* N )/$1/g;
	    s/as*(\][^\t]* ADJ )/o$1/g;
	    s/os(\][^\t]* ADJ )/o$1/g;
	}
	else {
	    s/([^r])(\] [^\t\[]*-([^> ]+r)[ >])/$1$3$2/g;
	}
    }

    if (! /^B[ \t]/) {s/ \[B(-Rare)*\]//g;} # removes (rare) Brazilian readings from the the morphology

    s/  +/ /g;
    print;
}






