#!/usr/bin/perl -w

use DB_File;

my ($bindir, $sep) = $0 =~ /^(.*)(\\|\/).*/;
$binDir = $bindir;
$lexDir = "$bindir$sep../portpars";

#print "****$binDir $lexDir\n";
# tidies up after prp.pre | preprocessor

$title =0;
$title2 =0;
$head =0;

tie %proptype, "DB_File", "$lexDir/names.var.db", O_RDONLY; # |O_EXLC;
tie %polylex, "DB_File", "$lexDir/polylex.adj.db", O_RDONLY; # |O_EXLC;

#print $proptype{"NEC"} . "\n";

### compensate for preprocessor's lacking ability to chop off plural- and feminine-endings for complex (=,-) adjectives

while (<>) {
  s/r-�([smt]e|l[ao]s?|[nv]os)(ia[sm]?|�ais|�amos)-(�[a-z]+)-?/r$2-�$1-$3/g; # cavar-se-lhe-iam -> (prp.pre) cavar-seiam-lhe -> (prp.post) cavariam-se-lhe ... but I can't remember all those possible forms
  s/-�e-�(\w)/-e-$1/g; # aquele entra-e-sai
  s/([^\$])-�/$1HYFEN/g;
#  print "--$_\n";

  @words = split /�/;
  foreach (@words) {
    $word =$_;
    s/HYFEN/-/g;
    if ($polylex{$_} || s/e?s$// && $polylex{$_} || s/a$/o/ && $polylex{$_}) { # evt.  || /^(al�m|aqu�m|bem|com?|ex|gr�o?|p�s|pr�|pr�|rec�m|sem|vice)-/ || /^(ab|ad|ante|anti|arqui|auto|circum|com|contra|entre|extra|hiper|infra|inter|intra|mal|neo|ob|pan|proto|pseudo|semi|sob|sobre|sub|super|supra|ultra)-/ -- prefixes exempted if they have obligatory or partially obligatory (before certain letters) hyphen
    }
    else {$_ =$word;}
  }
  $_ = join '�', @words;
  s/HYFEN/-�/g;

  s/�\$�/\"/g; # normalizing post-word " (had to be protected against preprocessor not recognizing xxx-yyy hypnenated words)
    s/���/\./g; # abbreviations from prp.pre
    s/^([^�])/�$1/; # extra � on line start
    s/[ _]>/>/g; # sgml-tilpasning
    s/<\�>/\$\�/g;
    s/(\$\">�[^�]+=[^�]+)�\$(\"�)/$1$2/g; # reattach right hand quotes that preprocessor has left isolated on next line (instead of attaching to the word) because of polylex-treatment with =
  s/�\$=�removeme�\$=//g; # necessary preprocessorblock from prp.pre to prevent fusion of socio-cultural while at the same time maintaining marcha-atr�s as a unit -- can have =" in the end, therefore: don't ask for � at the end of the replacement

# interference between clitics and polylexicals

    s/([^\$]-�[a-z����������]+)=/$1�/g; # amava- a=menos

# travess�o

    s/\$-�\$-/\$TRAVESSAO/g;

# English apostrophe

    s/([a-z])�\$[\�\']�s�/$1\'s�/g;
    s/([a-z])�\$\'�s$/$1\'s/g;

# Portuguese apostrophe

    s/(�\*?co)�\$\'/$1\'/g; # co'

# protect uppercase and hyphenation in names

    s/(\*[a-z�����������]+\-)�([a-z�����������]+A?\-)�\*(.)/$1$2\u$3/g; # Vilar-o-Velho
    s/\-�\*(.)/-\u$1/g; # Vilar-o-Velho

# protect dots in names

    s/(\*mrs?)�\$\.�\*/$1\.=\*/g; # Mr. Miller
  s/�i�\$\.�e�\$\./�i\.e\./g;

# protect contractions within hyphenated words 'chefe-do-c�u'

    s/(�(da|do)s*)-�/$1A-�/;
    s/([a-z�����������,] \*[a-z�����������=]+)=de�([oas]+)x�\*(.)/$1=d$2=\u$3/g; # Para Al�m do Poder ... risky
    s/([a-z�����������,] \*[a-z�����������=]+)=em�([oas]+)x�\*(.)/$1=n$2=\u$3/g; # Para Al�m do Poder ... risky

#    s/\`�<([^�\'\"]*)\'(>�\$\")/$1$2/g; # single Diana quotes within double Diana quotes
    s/�(no|vo)Y(las*|lo*s)�/�$1s-\n$2�/g; # preprocessor error regarding mostra-no-la -> mostra noYla
    s/(�(acam|aclam|adam|afam|am|amalgam|autoproclam|bram|cham|clam|conclam|declam|derram|desafam|descam|desmam|desram|destram|difam|embalsam|escam|esparram|exclam|gam|gram|infam|inflam|mam|proclam|program|recam|reclam|reprogram|tram|esprem|gem|prem|tem|trem|comprim|deoprim|deprim|descomprim|dirim|esgrim|exim|exprim|imprim|oprim|perim|redim|reimprim|reprim|suprim)o)s(-�(lh[eao]s?|[nv]os|[mt][eao]s?)�)/$1$3/g; # preprocessor error regarding clitics ambiguity, even 'temos?' IS ambiguous between two lexemes only before 'nos': 1S (temer) and 1P (ter)
    s/fff/�/g; # resplits fusion-protected expressions, e.g. um dado que
    s/���/=/g; # line-initial names in capital letters, fused from folha-corpus
    s/(�[a-z]*(eee+|iii+|rrr+|sss+)[a-z]*)�/$1qqqq�/g; # prrriii protected against verb-reading
    s/(o[ui][^ �]*)oi�/$1wwww�/; # impossible endings that might trigger endless loop in morfanalyse (oi <-> ou)
    s/�(\**([�\216]|era|foi))�que�/�$1=queqqqq�/g; # only speech data
    s/^(\**([�\216]|era|foi))�que�/$1=queqqqq�/g; # only speech data
    s/�\$\.�\$\.�\$\.�/�\$\.\.\.�/g;
    s/�\$\((�[^�]+�)\$\)/�\$\.\-$1\$\-\./g; # creates "hyphen-like parenthesis (( )) in case of single-words inside parenthesis: a Dire��o da Organiza��o dos Professores do Porto (DOP)
    s/�\$\((�\*[^\)\$]+�)\$\)/�\$\.\-$1\$\-\./g; # creates "hyphen-like parenthesis (( )) in case of names inside parentheses: GATT (Acordo geral de ...)
    s/�\$\(((�(\$[0-9-]+|\$,|e))+�)\$\)/�\$\.\-$1\$\-\./g; # creates "hyphen-like parenthesis (( )) in case of numbers in parentheses: (3-6, 6-3 e 6-0)

    s/�\$\- *�\$\-�/�\$\-\-�/g;
    s/�\$\- *�\$\-�/�\$\-\-�/g;
    s/^\$\- *�\$\-�/\$\-\-�/g;
    s/�[ǫ]�/�\$\">�/g; # handles mac �
    s/�[Ȼ]�/\"�/g; # handles mac and latin �, attaching it to preceding word
    s/^[ǫ]�/\$\">�/g; # handles mac � at line start
#	print "her: $_\n";
	s/(�(de|\$,)�\$[0-9]+)�\$�$/$1�/g; # linebreak removed in 10 a 17 
	s/(\*[a-z][A-Z]+\-)�\*([a-z][A-Z]+�)/$1\u$2/g; # ECI-UFMG as ONE abbreviation
    s/�de�(o|a|os|as|es[st][ao]s*|i[st]+o|aquel[ae]s*|aquilo|aqui|a�|el[ae]s*|outr[oa]s*|uma*|uns)x�/�d$1�/g; # reassembles do, da etc. where preprocessor could NOT find composite prepositions ... ([^�\*]+) replaced
    s/�em�([^�\*]+)x�/�n$1�/g;
    s/�a�a([^�\*]*)x�/��$1�/g;
    s/�a�(o|os)x�/�a$1�/g;

# following lines now handles in PALTRAD.latin lexically

#    s/�(([ei]s+t*)*(o|a|os|as))x�/�$1�/g;
#    s/�(el[ea]s*)x�/�$1�/g;
#    s/�(outr[ao]s*)x�/�$1�/g;
#    s/�(uma*|uns)x�/�$1�/g;
#    s/�((aqu[ei]l)*(o|a|es*|as))x�/�$1�/g;
#    s/�(a�|a�|aqui)x�/�$1�/g; # 'a�' also in Mac-ASCII

    s/�(([ei]s+t*)*(o|a|os|as))x(\"?�)/1�$1=2sam$4/g;
    s/�(el[ea]s*)x(\"?�)/1�$1=2sam$2/g;
    s/�(outr[ao]s*)x(\"?�)/1�$1=2sam$2/g;
    s/�(uma*|uns)x(\"?�)/1�$1=2sam$2/g;
    s/�(aquilo|aquel[ae]s?)x(\"?�)/1�$1=2sam$2/g;
#    s/�((aquil|aquele?)*(o|a|s|as))x�/1�$1=2sam�/g;
    s/�(a�|a�|aqui)x(\"?�)/1�$1=2sam$2/g; # 'a�' also in Mac-ASCII
    s/�([ao]s?)�(qual|quais)x(\"?�)/1�$1=$2=2sam$3/g;

	s/2sam\"�/2sam�\$\"�/g;

### the following lines are very odd, originally they even had ([a-z])*. They fuse (28 contos) into one $[28contos] block??

#    s/\$\[�\$([0-9]+)�([a-z]*)�\$\]/\$\[$1$2\]/g;
#    s/\$\(�\$([0-9]+)�([a-z]*)�\$\)/\$\[$1$2\]/g;

#    s/\$([0-9]+)�([a-z])�/\$\[$1$2\]�/g; # very dangerous: em 1992 e 1993

    s/\/�\$\~�/\/\�xtildex/g; # protect ~
    s/�\$\&�([a-z]+)�\$\;/�\&$1\;/g;

    s/�\$\:�\$\-�\$([\)\(])/�:-$1/g; # smiley
    s/�\$\:�\$([\)\(])/�:$1/g; # smiley
    s/�\$\(�\$\)/�()/g; # abra�os
    s/�<(arg[0-9])>/�\$<�$1�\$>/g; # <arg1>

    s/(�\$\@((�[a-zA-Z0-9]+-)?�[a-zA-Z0-9]+�\$\.)+(�[a-zA-Z0-9]+))/�$1�/g; # e-mail addresses
    if (s/(https?�\$[:_](�\$\/)+((�[a-zA-Z0-9]+\-)?�[a-zA-Z0-9]+�\$[\.\/](�\$\~)?)+(�html*|(�[a-z]+-)?�[a-z]+(�\$\/)?))/�$1�/g) {s/(http�\$)_/$1:/g;} # url-adresses
    s/(www�\$\.((�[a-zA-Z0-0]+-)?�[a-zA-Z0-9]+�\$[\.\/](�\$\~)?)+(�html*|(�[a-zA-Z0-9]+-)?�[a-zA-Z0-9]+(�\$\/)?))/�$1�/g;
    s/([^\/\\\.\_]�)(([a-zA-Z0-9]+�\$[\/\\\.\_]|[A-Za-z0-9]�\$\:�\$[\/\\])((�[a-zA-Z0-9]+-)?�[a-zA-Z0-9]+�\$[\/\\\.\_])*�(html*|txt|doc|exe|gif|jpeg|ps|pdf|\$\.))�/$1�$2��/g;
    if (s/�(([A-Za-z]�\$\:|\$[\/\\]�[a-z]+)�\$[\/\\](�([a-z\-]+|meus�[a-z]+)�\$[\/\\])*�([a-z\"\-]+|\$\.)(\$[\/\\])?)�/��$1��/g) {s/([\\\/])�meus�/$1meus=/g;}
#	print "her: $_\n";
	s/((�[a-z]+�\$\.)+�[a-z]+�\$:�\$[0-9]+)�/��$1��/g; #proxy.alunos.di.fc.ul.pt:3128

    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/�([^��]*?)[�\$]+([^�])/$1�$2/g;
    s/[��]//g;
#	print "****** $_\n";
	s/(\.[a-z]+)�(\/)/$1$2/g;

    s/�\$\.�(html?|txt|doc)�/\.$1�/g; # uncomplete html-chaining
    s/(\/[a-z\-]+)�\$\/�/$1\/�/g; # uncomplete .../..../
    s/(\\[a-z\-]+)�\$\\�/$1\\�/g; # uncomplete ...\....\

	s/�\$([\-\.])�([A-Za-z0-9\.\-]+\@)/$1\u$2/g; # hyphen and dot allowed before @ in e-mail addresses
	s/�\$([\-\.])�\*([A-Za-z0-9\.\-]+\@)/$1\u$2/g; # hyphen and dot allowed before @ in e-mail addresses

	s/�\$([\-\.])�([A-Za-z0-9\.\-]+\@)/$1\u$2/g; # hyphen and dot allowed before @ in e-mail addresses
	s/�\$([\-\.])�\*([A-Za-z0-9\.\-]+\@)/$1\u$2/g; # hyphen and dot allowed before @ in e-mail addresses

    s/-�([A-Za-z0-9\.\-]+\@)/-$1/g; # hyphen allowed before @ in e-mail addresses

    s/xtildex/~/g; # normalise ~

# proper noun filter (names, NER)

    if (/^�?<titulo/ || /^�?<t>/) {$title = 1;}
    elsif (/^�?<\/titulo/ || /^�?<\/[pst]>/) {$title =0;}
    if (/�?<s[ _]head/) {$head = 1;}
    elsif (/�?^<\/s/) {$head =0;}

    if ($title ==1 || $head ==1) {s/�\*/\n\*/g;} # protect block letter titles, e.g. <t> Delegado do Governo andaluz

    s/(�\$[\.\:\;\!\?\)\�\�] *�(\$[\"\-\�]+>*�|<[^�]+> *�)*|�\$TRAVESSAO�)(\*(as*|ainda|ao*s*|apenas|at[�\216]|cartas*|casos*|como*|conforme|de|d[ao]s*|diz|[�\216]|e[mu]*|e[nm]quanto|era|foi|ih|inclusive|j[�]|mai?s|muito|n[ao]s*|n[�\213]o|nem|ningu[�\216]m|[�\227]|onde|o[su]*|�|para|pel[ao]|quando|quem|segundo|s[ei]m*|sobre|s[�\227]|tamb[�\216]m|uma*|[�\210]|[a-ce-z][a-z������������]|[a-z������������])�)/$1===$3/g; # protects sentence-initial smallwords, de, da etc. tolerated because of "Da Matta" etc.
    s/^ *(\*(as*|ainda|ao*s*|apenas|at[�\216]|cartas*|casos*|como*|conforme|de|d[ao]s*|desde|diz|[�\216]|efec?tivamente|e[mu]*|e[nm]quanto|era|foi|freq[u�]entamente|ih|inclusive|j[�]|mai?s|muito|n[ao]s*|n[�\213]o|nem|ningu[�\216]m|[�\227]|onde|o[su]*|�|para|pel[ao]|quando|quem|segundo|s[ei]m*|sobre|s[�\227]|tamb[�\216]m|uma*|[�\210]|[a-ce-z][a-z������������]|[a-z������������]|dois|tr�s|quatro|cinco|seis|sete|oito|nove|dez|vinte|trinta|q[u�]arenta|cinq[u�]enta|sessenta|settenta|oitenta|noventa)�)/===$1/g; # protects sentence-initial smallwords, de, da etc. tolerated because of "Da Matta" etc.
    s/^(�*(\$[\"\-\�]+>*�|<[^�]+> *�)+|� *)(\*(as*|ainda|ao*s*|apenas|at[�\216]|cartas*|casos*|como*|conforme|de|d[ao]s*|desde|diz|[�\216]|efec?tivamente|e[mu]*|e[nm]quanto|era|foi|freq[u�]entamente|ih|inclusive|j[�]|mai?s|muito|n[ao]s*|n[�\213]o|nem|ningu[�\216]m|[�\227]|onde|o[su]*|�|para|pel[ao]|quando|quem|segundo|s[ei]m*|sobre|s[�\227]|tamb[�\216]m|uma*|[�\210]|[a-ce-z][a-z������������]|[a-z������������]|dois|tr�s|quatro|cinco|seis|sete|oito|nove|dez|vinte|trinta|q[u�]arenta|cinq[u�]enta|sessenta|settenta|oitenta|noventa)�)/$1\===$3/g; # protects sentence-initial smallwords, supposes break after <s> ---> denne tredje version brugt af runcorp.diana i �jeblikket
#    s/\*(al-|d[aei]|[ei]l|l[aeo]|the|v[ao]n)===/\*$1/g; # reparere overblokering mod propria
    s/===\*(al-|d[aei]|[ei]l|in|l[aeo]|on|of|the|v[ao]n)�/\*$1�/g; # reparere overblokering mod propria

    s/d�\$\'/d\'/g;
#################################### NAMES names NER #########
#	print "her: ---$_+++\n";

s/(�\*e\.)�(coli�)/$1=$2/;
    s/(�\*[^A-Z0-9\$\=�]*?)�(d[aeio]s*=[A-Z\*])/$1=$2/g; # Marques da=Silva, ikke: Macau do=que
	s/(�\*[a-z][A-Z]+?)�(\*[a-z����������])/$1=$2/g; # MS Word
    s/(�\*[^A-Z0-9\$\=�]*?)�(\*[^A-Z0-9\$\=�]*?=)/$1=$2/g; # names growing right
    s/(�\*[^A-Z0-9\$\=�]*?)�(\*[^A-Z0-9\$\=�]*?=)/$1=$2/g; # names growing right

    s/(�\$\">*�)((\*[^A-Z0-9\$\=�]*?�)+)(\*[^A-Z0-9\$\=�]*?\")/$1�$2$4��/g; # Depois de "Os Evangelistas", de "Les Aventures Singuli�res"

	s/�(et\.al\.)/=$1/g;
	s/(�\*[^A-Z0-9\$\=�]*?)�\$(,)�(\*[^A-Z0-9\$\=�]*?=et\.al\.)/$1=$2=$3/g; # names growing left from et.al.
	s/(�\*[^A-Z0-9\$\=�]*?)�\$(,)�(\*[^A-Z0-9\$\=�]*?=et\.al\.)/$1=$2=$3/g; # names growing left from et.al.
	s/(�\*[^A-Z0-9\$\=�]*?)�\$(,)�(\*[^A-Z0-9\$\=�]*?=et\.al\.)/$1=$2=$3/g; # names growing left from et.al.
	s/(et\.al\.)�\$\.-�\$([0-9\:p,]+)�\$-\./$1=($2)/g; # et.al. (2004)


#  next 3 lines: non-greedy-? removed at last * due to visl-perl problems

    s/^((\*[^A-Z0-9\$\=\"�]*�)+(((\$[\'\&]|al-|[au]nd|d[aeio]s*|d\'|et|in|las*|los*|of|on|the|v[ao][nm]|y|\&+)�)+(\*[^A-Z0-9\$\=�]*�)+)+)/�$1�/g;

    s/�((\*[^A-Z0-9\$\=\"�]*�)+(((\$[\'\&]|al-|[au]nd|d[aeio]s*|d\'|et|in|las*|los*|of|on|the|v[ao][nm]|y|\&+)�)+(\*[^A-Z0-9\$\=�]*�)+)+)/��$1�/g; # de, da, do fjernes evt. pga. fx. o Projeto=Oz�nio do Instituto=Nacional de Pesquisas=Espaciais, Grupo Teatro Terap�utico do Hospital J�lio de Matos -> jf. brydning nedenfor, 'a' fjernet pga. faz Deus a Portugal 
#	print "her: ---$_+++\n";

    s/�((\*[^A-Z0-9\$\=\"�]*�)+(\*[^A-Z0-9\$�]*�)+)/��$1�/g; # pure upper case strings without de/da/von
#	print "her: ---$_+++\n";


#    s/�((\*[b-dfnp-z�����]\.�)+(\*[^A-Z0-9\$�]*?�)+)/��$1�/g; # C. D. Amaro

    s/�((\*[^A-Z0-9\$\=\"�]*�)+(d[eao]s*=)*([A-Z�������������\*][^A-Z0-9\$�]*?�)+)/��$1�/g; #  upper case string + name already recognized by preprocessor, such as da=Costa

s/�((v[ao]n|di)�(\*[^A-Z0-9\$\=�]*?�)+)/��$1�/g;
s/�((de)�(la)�(\*[^A-Z0-9\$\=�]*?�)+)/��$1�/g;
s/�((\*[^A-Z0-9\$\=�]*?-�)+)\$([0-9]+�)/��$1$3�/g; # Nimbus-7, Eco-92


s/(\$\`�)(\**[a-z]+[b-df-hjknpqtv-y]�(\**[a-z]+�)+)(\$\')/$1�$2�$4/g; # o ` red fish '
s/(\$\`�)(\**[a-z]+�(\**[a-z]+[b-df-hjknpqtv-y]�)+)(\$\')/$1�$2�$4/g; # o ` red fish '
s/(\$\`�)(\**[a-z]*(k|sch|sz|w|y|������|[b-df-hj-mp-tv-z][b-df-hj-np-tv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3)[a-z]*�(\**[a-z]+�)+)(\$\')/$1�$2�$6/g; # o ` red fish '
s/(\$\`�)(\**[a-z]+�(\**[a-z]*(k|sch|sz|w|y|������|[b-df-hj-mp-tv-z][b-df-hj-np-tv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3)[a-z]*�)+)(\$\')/$1�$2�$6/g; # o ` red fish '

    s/�([^��]+)�([^��]+)�(([^��]+)?�)/�$1$2$3/g; # overlapping ��: Servi�o de Informa��es Estrat�gicas de Defesa -- ��*servi�o�de��*informa��es�*estrat�gicas��de�*defesa��

#
    s/^� *\*([a-z������������]+-�se�)/�\$M�$1/g; # Prevents "Reenreda-se" from being read as a noun, $M stands for "majuskel"



s/�=*(\**(um|dois|tr�s|quatro|cinco|seis|sete|oito|nove)?�?(mil|milh�o|milh�es)�((cento|(duzent|trezent|quatrocent|quinhent|seiscent|setecent|oitocent|novecent)[ao]s)�)?(e�(vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa)�)?(e�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|mei[oa])�)?)/��\$$1�/g; # fx 1998, extra $ protects against morfanalyse, will be NUM in cg2adapt
#  print "her: ---$_+++\n";
s/�=*(\**(cento|mil|um�milh�o|(duzent|trezent|quatrocent|quinhent|seiscent|setecent|oitocent|novecent)[ao]s)�e�((vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|mei[oa])�)+)/��\$$1�/g; # fx 327
s/�=*(\**(vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa)�e�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|mei[oa])�)/��\$$1�/g; # fx 52
s/�=*(\**(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove)�e�(mei[oa])�)/��\$$1�/g; # fx 52

	s/(mil��?)\$([a-z])/$1$2/g; # avoiding $ in the middle of a number

#    print "her: $_$title\n";
    if (/t�tulo�\$\:/) {$title2 =1; print "now\n";}
    elsif ($title2 ==1 && /\*/) {
	s/(\$\">�)([^\"]+\")/$1�$2�/g;
	$title2=0;
    }

  s/�([^�]*�)/$1/g; # overlapping fusions, especially numbers
  s/(�[^�]*)�/$1/g; # overlapping fusions, especially numbers

#	s/�(\*(rua)�(dos?|das?)�\*[a-z�����]+)�/��$1��/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/�([^��]*?)�([^�])/$1=��$2/g;
    s/[��]//g;

    s/([\*A-Z][a-z������]+)�\*([ivx][IVX]*)�/$1=\u$2�/g; # D. Fernando II

    s/=�\$\'=�/\'/g; # Avenida=Duque=D=$'=Avila
    s/=�\$\&=/=�&=/g; # Banco=Totta=$&=A�ores
 	s/(&=�?\*[^�]+)�\$\.-�\$([0-9\:p,]+)�\$-\./$1=($2)/g; # Santos & Sarmento (2004)

  s/(=�\*[a-z]+)-�/$1-/g; # Li Bun-hui, Li Wen-ming

# number fuser

# numbers in two parts
	s/�\$\.-�\$([0-9\-\+=]+)�\$-\.�\$([0-9\-\+=])/�\$1xxx($1)=$2/g; # telephone numbers
#	print "her: $_\n";
    s/�\$\+�\$([0-9\-\+=]*[0-9])�\$([0-9\/\-\+=]*[0-9])/�\$1xxx\+$1=$2/g; # 2 1/2, 2 000
    s/�(\$(1xxx)?\(?[0-9\-\+=\)]*[0-9])�\$([0-9\/\-\+=]*[0-9])/�$1=$3/g; # 2 1/2, 2 000
	s/=([0-9][0-9])�\$([0-9][0-9])=/=$1=$2=/g; # telephone numbers in double digits
	s/([A-Z][0-9]+)�\$(\+�\*?[a-z�����])/$1$2/; # optimizado para IE5+
#	print "her: $_\n";


# not read headlines as proper nouns, - but not split what preprocessor has assembled (S�o=Paulo), only what prp.post has marked with ==


#    s/(([�\.\:\;\?\!\)��]|<title>|<t>)�[^�<]+(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
#    s/(([�\.\:\;\?\!\)��]|<title>|<t>)�[^�<]+(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
#    s/(([�\.\:\;\?\!\)��]|<title>|<t>)�[^�<]+(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
#    s/(([�\.\:\;\?\!\)��]|<title>|<t>)�[^�<]+(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
#    s/(([�\.\:\;\?\!\)��]|<title>|<t>)�[^�<]+(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;

    s/(([�\:\;\?\!\)��]|<title>|<t>)(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�0-9]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
    s/(([�\:\;\?\!\)��]|<title>|<t>)(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�0-9]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
    s/(([�\:\;\?\!\)��]|<title>|<t>)(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�0-9]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
    s/(([�\:\;\?\!\)��]|<title>|<t>)(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�0-9]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g;
    s/(([�\:\;\?\!\)��]|<title>|<t>)(�[\*\$][^�]+)*)=�([^�]+(�[\*\$][^�0-9]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$4/g; # ikke med tal til sidst: Werder Bremen-Duisburg, 5-1

    s/=�/=/g; # back to unprotected

    s/�(\**entre)�\$([0-9][0-9a-z���\-\=]*)�(e)�\$([0-9a-z���\-\=]+)/�\$$1=$2=$3=$4/g; # entre 7 e 8
#    s/�(\**de)�\$([0-9][0-9a-z���\-\=]*)�(para)�\$([0-9a-z���\-\=]+)/�\$$1=$2=$3=$4/g; # de 7 a 8
    s/�\$([0-9][0-9a-z���\-\=]*)�(a|para)�\$([0-9a-z���\-\=]+)/�\$$1=$2=$3/g; # 7 a 8, cave: det skal v�re [0-9][0-9a-z���\-\=]*, ikke [0-9a-z���\-\=]+, fordi $-- fra Dianas corpora ellers giver kluder!!

    s/(�\$[0-9][0-9\-]*)�\*i([IVXLM\-]+�)/$1I$2/g; # 14-XII-32
    s/(�\$[0-9][0-9\-]*)�\*v([IVXLM\-]+�)/$1V$2/g; # 14-XII-32
    s/(�\$[0-9][0-9\-]*)�\*x([IVXLM\-]+�)/$1X$2/g; # 14-XII-32

    s/�(\**entre)�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)�(e)�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)/�\$$1=$2=$3=$4/g; # entre 7 e 8
#    s/�(\**de)�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)�(a|para)�(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)/�\$$1=$2=$3=$4/g; # de 7 a 8
    s/�(\**uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)�(a|para)�\$*(uma*|dois|duas|tr�s|quatro|cinco|seis|sete|oito|nove|dez|onze|doze|treze|quatorze|catorze|quinze|dez[ae]sseis|dez[ae]ssete|dezoito|dez[ae]nove|vinte|trinta|quarenta|cinq[u�]enta|sessenta|oitenta|noventa|cem|mil|zero)/�\$$1=$2=$3/g; # sete a oito

    s/<t>�==*�/<t>�/g; # removes odd "" in <t>"" (produced in some way by the preceding 5 lines - not read headlines as proper nouns)

#    s/([�\.\:\;\?\!\(\)��>]�[^�<]+(�[\*\$][^�]+)*)=([^�]+(�[\*\$][^�]+)*�\$*[��\.\:\;\?\!\(\)��<])/$1��$3/g;

    s/�//g;

# bryde for lange propria:
    s/=do=(\*(acordo|centro|congresso|[IVX]+=Congresso|[IVX]+=Encontro|gabinete|grupo|hospital|instituto|laborat�rio|minist�rio|movimento|museo|n�cleo|parque|partidoplano|programa)[=�])/�do�$1/g;
    s/=da=(\*(�rea|associa��o|c�mara|direc?��o|faculdade|guarda|junta|juventude|liga|lei|miss�o|ordem|oficina|orchestra|organiz��o|plataforma|regi�o|universidade)[=�])/�da�$1/g;
    s/=de=(\*(corrup��o)[=�])/�de�$1/g;
# bryde usandsynlige propria
    s/(\*[^=�]+)=(\*(deus|vossa=[^�]+))/$1�$2/g;
#    s/�(\*(avenidas*|ruas*))=/�$1�/g; # m�ske alligevel bedre at betragt Avenida Marton som eet navn ....
  if (! /^�?</) {
    s/(=[^ �]{37,}?)=(d[ao]s?)=/$1�$2�/g; # breaks hyper-long name candidates
    s/(=[^ �]{40,}?)=/$1�/g; # breaks hyper-long name candidates
  }
    s/===//g; # smallword protection removed
    s/=\*([a-z])/=\U$1/g;

# foreign word compound filter

    s/(�[^�]*(k|sch|sz|w|y|������|[b-df-hj-np-tv-z][b-df-hj-np-tv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3[b-df-hjknpqtv-y])[^�]*-)�/$1/g;
    s/(�[^�]*(k|sch|sz|w|y|������|[cf-hjkpqstv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3[b-df-hjknpqtv-y])[^�]*-)�/$1/g;

    s/-�([^�]*(k|sch|sz|w|y|������|[b-df-hj-mp-tv-z][b-df-hj-np-tv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3[b-df-hjknpqtv-y])[^�]*�)/-$1/g;
    s/-�([^�]*(k|sch|sz|w|y|������|[cf-hjkpqstv-z][b-df-hj-np-tv-z][b-df-hjkmnpqstv-z]+|([a-df-qt-z])\3[b-df-hjknpqtv-y])[^�]*�)/-$1/g;

    s/(\$\.\-)([^�])/$1�$2/g;
    s/(\$\-+)([^�\.-])/$1�$2/g; # ikke bryde $--
    s/(\$\-+)([^�\.-])/$1�$2/g; # ikke bryde $--

# I&D
    s/QQQ/&/g;

# broken numbers

    s/(�\$[0-9\_]+)�\$(0+�)/$1_$2/g;
    s/(�\$[0-9\_]+)�\$(0+�)/$1_$2/g;
    s/(�\$[0-9\.-]+)[�\n]\*dDD([a-zA-Z][�\n])/$1$2/g; # refuse 2.0c
    s/[�\n]\*dDD//g; # VVER-440
    s/DDD/=/g;
	s/-ddd/-=/g;
	s/-\$/-/g;


# book and film titles, fused from prp.pre by ���
    s/\$\`�<([^�\']+)>�\$\'/\$\`�\*\l$1�\$\'/g; # `Nova=Ordem=Mundial' 
    s/\$\"�<([^ >�]+)��([^ >�]+)>�\$\">*/\$\">�\*\l$1=$2\"/g; # both left and right quote with only 1 ��
    s/\$\"�<([^ >�]+)��/\$\">�\*\l$1=/g; # left quote with more than 1 ��
    s/��([^ >�]+)>�\$\">*/=$1\"/g; # right quote with more than 1 ��

#    s/��([^ >�]+)>/=$1\"/g;
    s/��/=/g;   # remove preprocessorprotecting <...> from around book and film titles
    s/-=/-/g; # hyphenated words don't need =
    s/d\'=/d\'/g; # "Fados=d'=Aqu�m=e=d'Al�m-=Mar"
    s/�d�\$\'�/�d'�/g; # as obras d'esta casa

# Italian and English ':  commedia dell'arte, don't, won't, o'Neill

    s/�(aren|couldn|dell|don|won)�\$\'�/=$1\'/g;
    s/[�=]((\*?[mM]rs?\.?=)?\*?[oO])�\$\'�/�$1\'/g;
    s/�(\*l)�\$\'�/�$1\'/g; # L'amoroso
    s/�(\*l[aoe]s?)�/�$1=/g;
    s/(=[A-Z�����][a-z��������]+)�d([A-Z�����])/$1=d\'$2/g; # Ant�nio Jos� d'�vila
    s/([\*=-][a-zA-Z]+)�\$\'�s�/$1\'s�/g; # Queen's

# odd names not recognised by preprocessor or prp.post

    s/7�e�\*meio�/7=e=Meio�/g;
    s/=(Comercial)�e�\*(industrial)/=$1=e=\u$2/g; # Associa��o Comercial e Industrial da ...
    s/([=\*][A-Za-z����������][a-z]+)�(du|for|of)�\*/$1=$2=\*/g; # Central Bureau for Astronomical Telegrams, �ditions du Lombard
    s/(=(de|para|sobre)=[A-Z�����][a-z�������]+)�(e)�\*([a-z�������]+�)/$1=$3=\u$4/g; # Instituto=Nacional=de=Metereologia e Geof�sica

    s/(�ri[ao])�(d[eo])�(\*?[Ee]stado)=/$1=$2=$3�/g; # secret�ria de Estado Madelein ...
    s/(�ri[ao])�(s[eo])�(\*?[Ee]stado)/$1=$2=$3/g; # secret�rio de Estado
    s/=(Sobre)�([oa]s?)�([A-Z\*])/=$1=$2=$3/g; # *confer��ncia=Nacional=de=Ac���o=Sobre=o=Trabalho=Infantil

    s/\*(sant[ao]|s�o|[a-z]C)[\n�]\*([a-z�����])/\*$1=\u$2/g; # Santa Cruz ???? der burde v�re � - ikke \n !!!
    s/(\*[^�]+)�\*(internaciona|nacional)/$1=\u$2/g; # Cruz Vermelha Nacional

    s/(=d[ao]s?=[A-Z][a-z�����]+)�\*([a-z])/$1=\u$2/g; # Ant�nio=Lopes=da=Costa Almeida

#    print ">>>$_---";

# roads and product numbers
#
#    s/(�\*[a-z][A-Z]+)�\$([0-9])/$1=$2/g; # EN 11
#
# cars
#
#    s/�\*(alfa|alfa=Romeo|airbus|antonov|astra|audi|austin|bentley|boeing|bMW|bugatti|buick|celica|chevrolet|chrysler|citro[e�]n|civic|daihatsu|d\/S[=\-]|eagle|escort|ferrari|fiat|fokker|ford|golf|honda|hyundai|jaguar|kadett|lada|lancia|lexus|lotus=Esprit|maserati|mazda|mercedes|mitsubishi|m\/?S[=\-]|nissan|oldsmobile|opel|passat|peugeot|piper|pontiac|porsche|renault|rolls[=\-]Royce|rover|saab|sea|seat|sedan|skoda|subaru|suzuki|tatra|toyota|uno|vauxhall|volkswagen|volvo|vW)�\$([0-9]+|[0-9]+[,\.][0-9]+)�/�\*$1=$2�/g; # Peugeot 309 GTi
#
#    s/([\*A-Z][a-z]+|=[0-9]+)�\*([abglstx][GLSTXi]+[0-9]*(-[A-Z][a-z]+)?|combi|maxi|sedan|stationcar|van)�/$1=\u$2�/g; # Opel Astra GSi

# repair uppercase after late fusion

    s/(=|-|[A-Za-z][\'\.])\*([a-z])/$1\u$2/g;
    s/(=|-|[A-Za-z][\'\.])\*�/$1�/g;
    s/(=|-|[A-Za-z][\'\.])\*�/$1�/g;
    s/(=|-|[A-Za-z][\'\.])\*�/$1�/g;
    s/(=|-|[A-Za-z][\'\.])\*�/$1�/g;
    s/(=|-|[A-Za-z][\'\.])\*�/$1�/g;

### genopl�sning: special cases
#	print "her: $_\n";
	s/em=si�([a-z]+mol)/em�si�$1/g;
	s/=(Tel\.|E\-?mail|Fax|Box)/�\*\l$1/g;
	s/\*(avenida|rua)=/\*$1=�/g; # protecting Rua do Senado

### enkeltords-genopl�sning


    @words = split /[ \n�]/;
    for (@words) {
	$wordalt= $wordnu;
	$wordnu = $_;
	$upperprop = $_; $upperprop =~ s/^\*(.)/\u$1/;
#	print "her: $upperprop $proptype{$upperprop}--\n";
#	print "her: $upperprop--$wordalt++\n";
	if ($wordalt && $wordalt =~ /<O:/ && /^\*/ && ! $proptype{$upperprop}) {
	    s/^\*//; # Maybe for cartas, but not for coral?? cartas has its own prp.post_cartas, without active use of wordalt/wordnu
	}
	if (! $proptype{$_} && ! $proptype{$upperprop}) {
#	    print "$_-";
	    if (/^(.+=d[ao])=([^=]+)$/ && $proptype{$2}) {
		$second =$proptype{$2};
		if ($second =~ /[oi]/) {
		    s/^(.+)=(d[ao])=([^=]+)$/$1 $2 \*\l$3/g; # director de marketing para a Europa da Logitech
		}
	    }
	    if (/^(\*[a-z][A-Z]+)=([0-9]+)$/ && $proptype{$1}) {
		$first =$proptype{$1};
		if ($first =~ /[oi]/ && ! ($first =~ /[v]/)) {
		    s/^(.+)=([0-9]+)$/$1 $2/g; # vendeu aos EUA 70 avi�es .... ikke: BMW=605
		}
	    }
	    if (/^(\*de)=([^=]+)$/ && $proptype{$2}) {
		$second =$proptype{$2};
		if ($second =~ /[tci]/) {
		    s/^(\*de)=([^=]+)$/$1 \*\l$2/g; # De Moscovo chega ...
		}
	    }
	    elsif (/^(\*[ao])=([^=]+)$/ && $proptype{$2}) {
		$second =$proptype{$2};
		if ($second =~ /[tc]/) {
		    s/^(\*[ao])=([^=]+)$/$1 \*\l$2/g; # 1999: A Nig�ria come�a
		}
	    }
	    s/=E=(Aos?|D[ao]s?)=([A-Z])/ e \l$1 \*\l$2/g;
	    s/=(Aos?|[DN][ao]s?|Em)=([A-Z])/ \l$1 \*\l$2/g;
	    s/=(As?|Ap�s|Aos?|[DN][ao]s?|E|Mas|Os?|Ou)$/ \*\l$1/g;
	    s/(\*(pr�ncipe|pal�cio)s?)=([A-Z])/$1 \*\l$3/g;	    
	}
	else {
#	    print "\her---$proptype{$upperprop}";
	}

  }
    $_ = join ' ', @words;
    s/ /\n/g;

    s/=�/=/g; # back to unprotected


# remove extra � on line start

    s/^�//g;
    print;
}









