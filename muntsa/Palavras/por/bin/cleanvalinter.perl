#!/usr/bin/perl -w

$| =1;

while (<>) {

  s/\@%/�%/g;
  s/\@=/�=/g;
  s/\@�/�\@/g; # protect syntax

  if (/ N /) {
    s/(( +\@�[^ \n]+)+)( +.*)/$3$1/; # place semtags at lineend for cg-disambiguation (so REMOVE or SELECT don't discard other things toghether with them.
    s/(( +\@�[^ \n]+)+)( +.*)/$3$1/;
    s/(( +\@�[^ \n]+)+)( +.*)/$3$1/;
    s/(( +\@�[^ \n]+)+)( +.*)/$3$1/;

# s/\@�/�/g; # � will be the specific CG prefix in cleanval.second, so e.g. SELECT can be used without danger
}

    print;
}
