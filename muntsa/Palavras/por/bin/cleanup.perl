#!/usr/bin/perl -w
$|=1;
while (<>) {
#    s/\012/�/g;
    s/�\t/�  /g; # tab from vislcg messes up cleanup.perl
    s/<Correct!>//g;
    s/<\?>//g;

# double equal analyses (e.g. EC and ec- based)

  s/(�.*?)\1�/$1�/g;


# re-remove hyphen before clitics if it didn't result in a verb

    s/-(>\"� [^�]+)<hyfen> ([^�]+ (N|ADJ|ADV|PRP|PROP) [^�]+�)(\"<lhe-?>\")/$1$2$4/g; # tev�- lhe deu ..

    s/(\"<)([a-z])([^�]+�[^�]+<\*>[^�]+ PROP)/$1\u$2$3/g;

    s/(\"[^�\@\"]+)<\*1> +<hyfen>/\*1$1<hyfen>/g; # prepare for hyfen-fusing with first word-quote

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+)\"[^�]* EC *[^�]*�\"\<[^\"]+>\"[^�]*�[^�]+\"(e|ou)\"[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\"([^ ]+)\")/-$3-$4 <$2->/g; # inter- e intra-disciplinares -> fused

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"[^�]*<\*>[^�]* EC *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* (N|PROP|ADJ|ADV|V) )/-$3$2-$4\" <$2-> <\*>$5/g; # attaches hyphenated prefixes to their mother words, salvages <*> in first-part
    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"[^�]* EC *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]*) <\*>([^�]* )(N|PROP|ADJ|ADV|V)/-$3$2-$4\" <$2->$5$6$7/g; # attaches hyphenated prefixes to their mother words, removes second-part <*>

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"[^�]* EC *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\"([^ ]+)\")([^�]* (N|PROP|ADJ|ADV|V) )/-$3 <$2->$5/g; # attaches hyphenated prefixes to their mother words
    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"[^�]* EC *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\"([^ ]+)\")([^�]* (N|PROP|ADJ|ADV|V) )/-$3 <$2->$5/g; # attaches hyphenated prefixes to their mother words

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"([^�]* <col> +(ADJ)) *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* (N|ADJ)( F S| [M\/?F]+ P) [^�]*)/-$5$2-$6\" <$2-> $3$9/g; # attaches hyphenated expressions type color-ADJ-N: "azul-amarelos", read as adjectives with singular base form, inflected case
    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"([^�]* <col> +(ADJ)) *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* (N|ADJ) [^�]*)/-$5$2-$6\" <$2-> $3 M\/F S\/P/g; # attaches hyphenated expressions type color-ADJ-N: "amarelo-c�dmio", read as adjectives with singular base form, uninflected case

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"[^�]* (EC|ADJ|ADV|V) *[^�]*�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* (N) )/-$4$2-$5\" <$2->$6/g; # attaches hyphenated expressions ending in nouns: "grandes-homens", with singular base form from first part and inflexion taken from second part
    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"([^�]* (N|PROP) *[^�]*)�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* )(N) [^�]*/-$5$2-$6\" <$2-> $7$3/g; # attaches hyphenated expressions ending in nouns: "cobras-capelo", with singular base form and inflexion taken from first part if N/PROP, plus evt. <*2> from second part

    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"([^�]* (N|PROP) *[^�]*)�\"\<([^\"]+>\"[^�]*�[^�]+\")([^ ]+)\"([^�]* (ADJ|PCP) [^�]*)/-$5$2-$6\" <$2-> $3/g; # attaches hyphenated expressions type N - ADJ: "alhos-franceses", read as nouns with singular base form
    s/-( ALT [^> ]+)*>\"[^�]*�[^�]+\"([^ ]+?)-?\"([^�]* )(EC|ADJ|PCP|ADV) *[^�]*�\"\<\*?.([^\"]+>\"[^�]*�[^�]+)\"((.)[^ ]+)\"([^�]* (ADJ|PCP) )/-$7$5\"$2-$6\" <$2->$3$8/g; # attaches hyphenated expressions type ADJ-ADJ: "azul-verde"

    s/-( ALT [^> ]+)*(>\"[^�]*�[^�]+\"[^ ]+?)-?\"([^�]* (N|PROP|ADJ|PCP|ADV) *[^�]*)�\"\<([^\"]+)>\"[^�]*�[^�]+\"([^ ]+)\"[^�]* (PRP) [^�]*/-$5$2-$5\" <-$6-prp>$3/g; # attaches hyphenated prepositions: "termo-de-combate"
    s/-( ALT [^> ]+)*(>\"[^�]*�[^�]+\"[^ ]+)\"( +<-[^> ]+\-prp>[^�]* (N|PROP|ADJ|PCP|ADV) *[^�]*)�\"\<([^\"]+)>\"[^�]*�[^�]+\"([^ ]+)\"[^�]* (N|PROP|ADJ|ADV|V|SPEC) [^�]*/-$5$2$6\" <-$6>$3/g; # attaches hyphenated prepositions: "termo-de-combate"
    s/-( ALT [^> ]+)*(>\"[^�]*�[^�]+\"[^ ]+)\"( +<-[^> ]+\-prp>[^�]* (N|PROP|ADJ|PCP|ADV) *[^�]*)�\"\<([^\"]+)>\"[^�]*�[^�]+\"([^ ]+)\"[^�]* (N|PROP|ADJ|ADV|V|SPEC) [^�]*/-$5$2$6\" <-$6>$3/g; # attaches hyphenated prepositions: "termo-de-combate-final"

    s/\*1(\"[^�\@]+)<hyfen>/$1<\*1> <hyfen>/g; # re-establishing 1. quotes on on-fused words
    s/\*1->/-> <\*1>/g; # 1. quote on fused word
    s/\*1-/-/g; # tidying up, god knows why ...: "homem*1-morcego"

    s/(<(pr�-*|anti-*)>[^�]+)PROP [MF\/]+ [SP\/]+/$1ADJ M\/F S\/P/g; # anti-g�s, pr�-Ieltsin

    s/�\"<para>\"[^�]*� +\"para\"[^�]+�\"<o(s?>\"[^�]*� +\"pr�\")/�\"<pr�$1/g;
#    s/�\"<para>\"[^�]*�[^�]+�\"<o(s?>\"[^�]*� +\"pr�\")/�\"<pr�$1/g;

    s/ainda\=que>\"[^�]*� +\"\=ainda\+que\"[^�]*�/ainda>\"�  \"ainda\" ADV  !�1529�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/assim\=que>\"[^�]*� +\"\=assim\+que\"[^�]*�/assim>\"�  \"assim\" ADV  !�4609�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/�\=que>\"[^�]*� +\"\=�\+que\"[^�]*�/�>\"�  \"ser\" <foc> ADV  !�4609�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/logo\=que>\"[^�]*� +\"\=logo\+que\"[^�]*�/logo>\"�  \"logo\" ADV  !�30835�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/sempre\=que>\"[^�]*� +\"\=sempre\+que\"[^�]*�/sempre>\"�  \"sempre\" ADV  !�46109�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/mesmo\=que>\"[^�]*� +\"\=mesmo\+que\"[^�]*�/mesmo>\"�  \"mesmo\" DET M S  !�54188�\n\"<que>\"\n  \"que\" <rel> SPEC M S !�41963�\n/g;
    s/bem\=como>\"[^�]*� +\"\=bem\+como\"[^�]*�/bem>\"�  \"bem\" ADV  !�6340�\n\"<como>\"\n  \"como\" <rel> <prp> <ks> <igual> ADV !�53946�\n/g;
    s/a\=menos\=que>\"[^�]*� +\"\=a\=menos\+que\"[^�]*�/a=menos>\"�  \"a=menos\" <KOMP> <corr> ADV  !�4609�\n\"<que>\"\n  \"que\" <komp> <corr> KS !�41968�\n/g;

# ambiguous fused particles
    s/no>\"[^�]*� +\"\=em\+o\"( (<\*> )*)[^�]*�/em>\"�  \"em\"$1<sam-> PRP \n\"<o>\"\n  \"o\" <-sam> <art> DET M S \n/g;
    s/nos\>\"[^�]*� +\"\=em\+os\"( (<\*> )*)[^�]*�/em>\"�  \"em\"$1<sam-> PRP \n\"<os>\"\n  \"o\" <-sam> <art> DET M P \n/g;
    s/desse>\"[^�]*� +\"\=de\+esse\"( (<\*> )*)[^�]*�/de>\"�  \"de\"$1<sam-> PRP \n\"<esse>\"\n  \"esse\" <-sam> <dem> DET M S \n/g;
    s/desses>\"[^�]*� +\"\=de\+esses\"( (<\*> )*)[^�]*�/de>\"�  \"de\"$1<sam-> PRP \n\"<esses>\"\n  \"esse\" <-sam> <dem> DET M P \n/g;
    s/deste>\"[^�]*� +\"\=de\+este\"( (<\*> )*)[^�]*�/de>\"�  \"de\"$1<sam-> PRP \n\"<este>\"\n  \"este\" <-sam> <dem> DET M S \n/g;
    s/destes>\"[^�]*� +\"\=de\+estes\"( (<\*> )*)[^�]*�/de>\"�  \"de\"$1<sam-> PRP \n\"<estes>\"\n  \"este\" <-sam> <dem> DET M P \n/g;
#    s/nele>\"[^�]*� +\"\=em\+ele\"( (<\*> )*)[^�]*�/em>\"�  \"em\"$1<sam-> PRP \n\"<ele>\"\n  \"ele\" <-sam> PERS M 3S NOM\/PIV \n/g;
#    s/neles>\"[^�]*� +\"\=em\+eles\"( (<\*> )*)[^�]*�/em>\"�  \"em\"$1<sam-> PRP \n\"<eles>\"\n  \"eles\" <-sam> PERS M 3P NOM\/PIV \n/g;
    s/pra>\"[^�]*� +\"\=para\+a\"( (<\*> )*)[^�]*�/para>\"�  \"para\"$1<sam-> PRP \n\"<a>\"\n  \"a\" <-sam> <art> DET F S \n/g;
    s/<pr�>\"[^�]*� +\"\=para\+o\"( (<\*> )*)[^�]*�/<para>\"�  \"para\"$1<sam-> PRP \n\"<o>\"\n  \"o\" <-sam> <art> DET M S \n/g;
    s/mas>\"[^�]*� +\"\=me\+as\"[^�]*�/me>\"�  \"eu\" <sam-> PERS M\/F 1S DAT \n\"<as>\"\n  \"a\" <-sam> PERS F 3P ACC \n/g;
    s/consigo>\"[^�]*� +\"\=com\+si\"[^�]*�/com>\"�  \"com\" <sam-> PRP \n\"<si>\"\n  \"se\" <-sam> <refl> PERS M\/F 3S\/P PIV \n/g;
    s/do\=que>\"[^�]*� +\"\=de\+o\+que\"[^�]*�/de>\"�  \"de\" <sam-> PRP \n\"<o>\"\n  \"o\" <-sam> <art> DET M S \n\"<que>\"\n  \"que\" <rel> SPEC M S  !�41963�\n/g;
    s/posto\=que>\"[^�]*� +\"\=posto\+que\"[^�]*�/posto>\"�  \"posto\" N M S  !�40228�\n\"<que>\"\n  \"que\" <rel> SPEC M S !�41963�\n/g;
#    s/�/\012/g;

### normalize inflected base forms from fused double pronouns (fx mas: me+as/o, where only second base form is given in lexicon)

    s/\"me\"/\"eu\"/g;
    s/\"te\"/\"tu\"/g;
    s/\"lhe\"/\"ele\"/g;
    s/\"lhes\"/\"eles\"/g;

    s/<mais>\"[^�]*�[^�]*�\"<de>\"[^�]*�[^�]*�([^�]*�[^�]* NUM [^�]*�)/<mais\=de>\"\n  \"mais=de\" <quant> ADV !�54681�\n$1/g;
    s/<mais>\"[^�]*�[^�]*�\"<do\=que>\"[^�]*�[^�]*�([^�]*�[^�]* NUM [^�]*�)/<mais\=do\=que>\"\n  \"mais=do=que\" <quant> ADV !�54681�\n$1/g;
    s/<menos>\"[^�]*�[^�]*�\"<de>\"[^�]*�[^�]*�([^�]*�[^�]* NUM [^�]*�)/<menos\=de>\"\n  \"menos=de\" <quant> ADV !�54682�\n$1/g;
    s/<menos>\"[^�]*�[^�]*�\"<do\=que>\"[^�]*�[^�]*�([^�]*�[^�]* NUM [^�]*�)/<menos\=do\=que>\"\n  \"menos\=do\=que\" <quant> ADV !�54682�\n$1/g;
    s/<como>\"[^�]*�[^�]*�\"<se>\"[^�]*�[^�]* KS [^�]*�/<como\=se>\"\n  \"como=se\" KS !�45777�\n/g;
    s/<assim>\"[^�]*�[^�]*�\"<que>\"[^�]*�[^�]* <rel> +ADV [^�]*�/<assim\=que>\"\n  \"assim=que\" <rel> <ks> ADV !�4620�\n/g;

# fuse propria

    s/\"<([^>]+)>\"[^�]*� +\"([^�\"]+)\"( +<\*>[^�]* )(PROP [^�\@]+)�\"<[^>]+>\"[^�]*� +\"([^�\"]+)\" +(<\*>)?([^�]* )PROP [^�\@]*�/\"<$1=$5>\"\n  \"$2=$5\"$3$7$4 !�0�\n/g; # also: Botelho da=Costa without <*> on the second part
#    print "#-------$_-------\n";
    s/\"<([^>]+)>\"[^�\n]*[�\n] +\"([^�\n\"]+)\"( +<\*>[^�\n]* )(PROP [^�\n\@]+)[�\n]\"<[^>]+>\"[^�\n]*[�\n] +\"([^�\n\"]+)\" +(<\*>)?([^�\n]* )PROP [^�\n\@]*[�\n]/\"<$1=$5>\"\n  \"$2=$5\"$3$7$4 !�0�\n/g; # also: Botelho da=Costa without <*> on the second part
    s/<HEUR> +([^�]*<HEUR> )/$1/g; # double HEUR from propria fusion
    s/-=/-/g; # Etapas=Do=Crescimento P�s-=NATAL

    s/\"<�=que>\"[^�]*� +\"�=que\"[^�]*<v-ks>[^�]*�/\"<�>\"�  \"ser\" <vK> V PS 3S IND VFIN  !�46272�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/\"<foi=que>\"[^�]*� +\"�=que\"[^�]*<v-ks>[^�]*�/\"<foi>\"�  \"ser\" <vK> V PR 3S IND VFIN  !�46272�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;
    s/\"<era=que>\"[^�]*� +\"�=que\"[^�]*<v-ks>[^�]*�/\"<era>\"�  \"ser\" <vK> V PR 3S IND VFIN  !�46272�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;

    s/\"<(�|foi|era)=que>\"[^�]*� +\"=ser\+que\"[^�]*ADV_KS[^�]*�/\"<$1>\"�  \"ser\" <foc> ADV !�46272�\n\"<que>\"\n  \"que\" <\+IND\/SUBJ> KS !�41968�\n/g;

    s/<\/comment>�<comment_more>/_/g;
    s/(\"<nos)\*\*>/$1>/; # first person plural
    s/(\"<n[ao]s*)\*>/$1>/; # third person

    s/^\"<\$\.>\"�(\"<)/$1/g; # corrects extra . after abbreviation with interfering <autor> � etc

    print;
}





