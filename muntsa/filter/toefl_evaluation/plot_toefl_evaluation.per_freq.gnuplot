## plot toefl results for keep_only_relevant_triples and filter_by _counts

## file format: 
## thesaurus       found_in_th     accuracy        cv      tokens_in_th_optimistic types_in_th_optimistic  accuracy_optimistic     cv_optimistic  total   first_not_found others_not_found        none-found      target_not_found
# cosine.max_triples_10.sorting_by_lmi    1       0       inf     1766    990     0.777463        0.0127311       6643    360     1405    4877    755
# cosine.max_triples_20.sorting_by_lmi    2       0.5     0.707107        3037    1290    0.731314        0.0109989       6643    671     2364 3606    755


# files to plot: toefl_results.$suf.sorting_by_$sort.csv, toefl_results.$suf.csv


#set terminal postscript color landscape enhanced dashed lw 1 "Times" 24
set terminal postscript landscape enhanced dashed lw 1 "Times" 20

set pointsize 1.2                             # larger point!
set xlabel '{/Times-Italic th} ' offset 0,0.5
set xr [1:50] 
set xtics font "Times, 16"
set logscale x


set key bottom

##############
# make a plot with P, R and F1 for each thesaurus
set yr [0:1]
do for [meas in "lin"] {
 do for [freq in "high mid low"] {

    set title "WBST task: P, R and F1 for ".freq." frequency verbs\n{/*0.8Filtering triples with frequency under {/Times-Italic th}}"   
    set ylabel '{/Times-Italic P, R, F1}' offset 2
    set output  "toefl_".meas."_p-r-f1_".freq."_freq.eps"

    plot "toefl_results.".meas.".".freq."_freq.csv" using 1:9 title 'Precision' with  linespoints, \
         "toefl_results.".meas.".".freq."_freq.csv" using 1:11 title 'Recall' with  linespoints, \
	 "toefl_results.".meas.".".freq."_freq.csv" using 1:13 title 'F1' with  linespoints

 } 
}




