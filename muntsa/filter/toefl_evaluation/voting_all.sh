#! /bin/bash

#### perform the voting among mins (among measures the agreement is too high)

# for now we compare always the minimum with the next one

## perform the voting with relative and absolute positions
for pos in absolute relative; do

 #### output the first line of accuracy files (one for average and one for best)
 printf "thesaurus\tfound_in_th\taccuracy\tcv\ttokens_in_th_optimistic\ttypes_in_th_optimistic\taccuracy_optimistic\tcv_optimistic\ttotal   \tfirst_not_found\tothers_not_found\tnone-found\ttarget_not_found\n"  > voting/all_voting_results.average.$pos.csv
 printf "thesaurus\tfound_in_th\taccuracy\tcv\ttokens_in_th_optimistic\ttypes_in_th_optimistic\taccuracy_optimistic\tcv_optimistic\ttotal\tfirst_not_found\tothers_not_found\tnone-found\ttarget_not_found\n"  > voting/all_voting_results.best.$pos.csv

 for suf in lin fullcosine; do

  echo 
  echo "-----------------------"
  echo $suf

  for voting in average best; do     # taking the highest postion or the average

    echo 
    echo voting $voting

    min1=1
    min2=3

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2
 
    echo 
    echo voting with $fname1 and $fname2

    # perform the voting with different values of n (value added to not-found elements)
    for n in -100 -50 -10 0 10 50 ; do

	../Software/voting_toefl.py toefl_positions.$fname1.csv toefl_positions.$fname2.csv -v $voting -n $n -p $pos > voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv
	cat voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$suf.min$min1"_and_"min$min2.add$n -f get_accuracy.awk >> voting/all_voting_results.$voting.$pos.csv

    done

    ## 
    min1=3
    min2=5

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    echo 
    echo voting with $fname1 and $fname2

    # perform the voting with different values of n (value added to not-found elements)
    for n in -100 -50 -10 0 10 50 ; do

	../Software/voting_toefl.py toefl_positions.$fname1.csv toefl_positions.$fname2.csv -v $voting -n $n -p $pos > voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv
	cat voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$suf.min$min1"_and_"min$min2.add$n -f get_accuracy.awk >> voting/all_voting_results.$voting.$pos.csv

    done

    ## 
    min1=5
    min2=10

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    echo 
    echo voting with $fname1 and $fname2

    # perform the voting with different values of n (value added to not-found elements)
    for n in -100 -50 -10 0 10 50 ; do

	../Software/voting_toefl.py toefl_positions.$fname1.csv toefl_positions.$fname2.csv -v $voting -n $n -p $pos > voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv
	cat voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$suf.min$min1"_and_"min$min2.add$n -f get_accuracy.awk >> voting/all_voting_results.$voting.$pos.csv

    done

    ## 
    min1=10
    min2=20

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    echo 
    echo voting with $fname1 and $fname2

    # perform the voting with different values of n (value added to not-found elements)
    for n in -100 -50 -10 0 10 50 ; do

	../Software/voting_toefl.py toefl_positions.$fname1.csv toefl_positions.$fname2.csv -v $voting -n $n -p $pos > voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv
	cat voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$suf.min$min1"_and_"min$min2.add$n -f get_accuracy.awk >> voting/all_voting_results.$voting.$pos.csv

    done
    ## 
    min1=20
    min2=50

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    echo 
    echo voting with $fname1 and $fname2

    # perform the voting with different values of n (value added to not-found elements)
    for n in -100 -50 -10 0 10 50 ; do

	../Software/voting_toefl.py toefl_positions.$fname1.csv toefl_positions.$fname2.csv -v $voting -n $n -p $pos > voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv
	cat voting/toefl_positions.$suf.min$min1"_and_"min$min2.$voting.$pos.add$n.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$suf.min$min1"_and_"min$min2.add$n -f get_accuracy.awk >> voting/all_voting_results.$voting.$pos.csv

    done

  done
 done
done

