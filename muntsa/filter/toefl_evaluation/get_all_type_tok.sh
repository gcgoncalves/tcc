#!/bin/bash

printf "thes\tall-types\tall-tokens\tcandidates-types\tcandidate-tokens\n"; 

for suf in fullcosine lin npmi_lin; do

  for i in 1 3 5 10 20 50; do 

	can_type=`cat  toefl_positions.$suf"_min"$i.csv  | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8}' | sort -u | wc -l`; 

	can_tok=`cat  toefl_positions.$suf"_min"$i.csv  | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8}'  | wc -l`;   

	all_type=`cat  toefl_positions.$suf"_min"$i.csv   | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8; if ($3!=-1 || $5!=-1 || $7!=-1 || $9!=-1) print $1 }' | sort -u | wc -l`;  

	all_tok=`cat  toefl_positions.$suf"_min"$i.csv   | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8; if ($3!=-1 || $5!=-1 || $7!=-1 || $9!=-1) print $1 }' | wc -l`;  

	printf "%s\t%s\t%s\t%s\t%s\n" $suf"_min"$i  $all_type $all_tok $can_type $can_tok; 

  done

done

printf "\-------- wordnet generated -------\n" 

## with WN thesaurus
for suf in wn_lch wn_wup; do
  for i in 1 3 5; do

	can_type=`cat  toefl_positions.$suf".min"$i.csv  | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8}' | sort -u | wc -l`; 

	can_tok=`cat  toefl_positions.$suf".min"$i.csv  | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8}'  | wc -l`;   

	all_type=`cat  toefl_positions.$suf".min"$i.csv   | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8; if ($3!=-1 || $5!=-1 || $7!=-1 || $9!=-1) print $1 }' | sort -u | wc -l`;  

	all_tok=`cat  toefl_positions.$suf".min"$i.csv   | awk '{if($3!=-1) print $2; if($5!=-1) print $4; if($7!=-1) print $6; if($9!=-1) print $8; if ($3!=-1 || $5!=-1 || $7!=-1 || $9!=-1) print $1 }' | wc -l`;  

	printf "%s\t%s\t%s\t%s\t%s\n" $suf".min"$i  $all_type $all_tok $can_type $can_tok; 

  done

done


