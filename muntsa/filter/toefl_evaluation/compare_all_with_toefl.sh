#! /bin/bash


DATA_DIR=/home/muntsa/RECERCA/MWE/data/filter_triples_by_counts/filter_triples_min_freq_50/shelves
BIN_DIR=/home/muntsa/RECERCA/Software/MWE

nois=0

# output the first line of the output (key)
printf "thesaurus\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\ttotal\tfirst_not_found\tothers_not_found\tnone-found\ttarget_not_found\n" > all_toefl_results.csv

fr=50 # minimum verb fequency

for suf in cosine lin ; do

  printf "min\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\tf1\ttotal\n" > toefl_results.$suf.csv

  for min in 1 2 3 4 5 6 7 8 9 10 15 20 30 50; do
	fname=$suf".min"$min

	echo 
	echo "processing $fname"
	$BIN_DIR/toefl_comparison.py $DATA_DIR/thesaurus_$fname"_nois"$nois.min_freq_$fr.shelf  wbst-nanews.v.test > toefl_positions.$fname.csv

        # get the accuraccy for this case  
        # warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, 
	#  for stupid problems..)    
	cat  toefl_positions.$fname.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$fname -f get_accuracy.awk >> all_toefl_results.csv
 
	## print just accuracy, number of tokens and number if types, also in a file only for this measure (to be used in gnuplot)
	cat  toefl_positions.$fname.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$fname -f get_accuracy.awk | awk -v k=$min 'BEGIN{OFS="\t"}{if ($5!=0) print k, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, 2*$9*$11/($9+$11), $13}' >> toefl_results.$suf.csv

	# ## old -- get the accuraccy for this case
	##cat  toefl_positions.$fname.csv | awk -v suffix=$fname -f get_accuracy.awk >> all_toefl_results.csv
  done
done

