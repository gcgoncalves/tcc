#! /bin/bash


DATA_DIR=/home/muntsa/RECERCA/MWE/data/filter_triples_by_counts/filter_triples_min_freq_50/shelves
BIN_DIR=/home/muntsa/RECERCA/Software/MWE

nois=0
freq_ranges="150,500"
freq_file="/home/muntsa/RECERCA/MWE/data/verb-counts/verb_counts.aprox.min_freq_50.csv"

# output the first line of the output (key)
for freq in low mid high; do
	printf "thesaurus\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\ttotal\tfirst_not_found\tothers_not_found\tnone-found\ttarget_not_found\n" > all_toefl_results.$freq"_freq".csv

done

fr=50 # minimum verb fequency

for suf in cosine lin ; do


  for freq in low mid high; do
  	printf "min\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\tf1\ttotal\n" > toefl_results.$suf.$freq"_freq".csv
  done

  for min in 1 2 3 4 5 6 7 8 9 10 15 20 30 50; do
	fname=$suf".min"$min

	echo
	echo "processing $fname"
#	$BIN_DIR/toefl_comparison.py $DATA_DIR/thesaurus_$fname"_nois"$nois.min_freq_$fr.shelf  wbst-nanews.v.test > toefl_positions.$fname.csv

	## separate toefl_positions.$fname.csv in low, mid and high freq ranges

	# separate not found:
	cat toefl_positions.$fname.csv  | grep "target_not_found" > toefl_positions.$fname.target_not_found.csv

	rm toefl_positions.$fname.*_freq.csv 
	cat toefl_positions.$fname.csv | grep -v "target_not_found" | awk -v franges=$freq_ranges -v freq_file=$freq_file -v suf=$fname 'BEGIN{while(getline<freq_file) freq[$1]=$2; split(franges,fr,",")}{if(freq[$1]<=fr[1]) print $0 >> "toefl_positions."suf".low_freq.csv"; else if (freq[$1]>fr[2]) print $0 >> "toefl_positions."suf".high_freq.csv"; else print $0>> "toefl_positions."suf".mid_freq.csv"} '


        # get the accuraccy for each freq range case  
        # warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, 
	#  for stupid problems..)   

	for freq in low mid high; do
		cat  toefl_positions.$fname.$freq"_freq".csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$fname -f get_accuracy.awk >> all_toefl_results.$freq"_freq".csv
 	
		## print just accuracy, number of tokens and number of types, also in a file only for this measure (to be used in gnuplot)
		echo "doing ..." toefl_positions.$fname.$freq"_freq".csv 
		cat  toefl_positions.$fname.$freq"_freq".csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -v suffix=$fname -f get_accuracy.awk | awk -v k=$min 'BEGIN{OFS="\t"}{if ($5!=0) print k, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, 2*$9*$11/($9+$11), $13}' >> toefl_results.$suf.$freq"_freq".csv

	done
  done
done


