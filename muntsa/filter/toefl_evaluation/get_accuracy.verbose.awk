BEGIN{first_unk=0; other_unk=0; ok=0; ko=0; okop=0; koop=0; all_unk=0; target_unk=0; OFS="\t"; lemma=""; types=0}
{
  print lemma
  print $0
  ## the file is sorted by the first col, so if $1 is different than lemma, it means we got a new type
  if (lemma!=$1 && ($3!=1000000 || $5!=1000000 || $7!=1000000 || $9!=1000000)) { ## only count lines were some elements were found
    lemma=$1
    types+=1
    ##print "---- new lemma ", lemma, types
  }

  if(match($0, "1000000")) { # some of the verbs were not found, we will count that line in the optimistic counts)
    if ($3==1000000 && $5==1000000 && $7==1000000 && $9==1000000) { # this can be the neignors were uunknown or the target lemma
      if ($10=="target_not_found") target_unk+=1 # the target lemma was not found
      else all_unk+=1
    }
    else {
	if ($3==1000000) first_unk+=1
    	else other_unk+=1

    	# anyway, see if the first one is better than others (optimistic count) (Only if they are not all -1, this is why we ar inside the if)
	# warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, for stupid problems..)
    	if ($3<=$5 && $3<=$7 && $3<=$9) {okop+=1; print "--------- mais um"}
	else { koop+=1}
    }
  }
  else { # if all were found in the thesaurus see if the first synonim is the best ranked (strict count)
    if ($3<=$5 && $3<=$7 && $3<=$9) ok+=1
    else ko+=1
  }
}
END{ # print
  all=ok+ko+first_unk+other_unk+all_unk
  print suffix, ok+ko, ok/(ok+ko), sqrt((1-ok/(ok+ko))/ok), ok/all, sqrt((1-ok/all)/ok),okop+koop+ok+ko, types, (okop+ok)/(ok+ko+okop+koop), sqrt((1-(okop+ok)/(ok+ko+okop+koop))/(ok+okop)),  (okop+ok)/all, sqrt((1-(okop+ok)/all)/(ok+okop)), all,first_unk, other_unk, all_unk, target_unk
}
