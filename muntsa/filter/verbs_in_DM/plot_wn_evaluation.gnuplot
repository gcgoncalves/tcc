## plot wn results

## file format:
##              k:      5        10      15      20     50       100                             
##              wn_meas:        path   wup         path     wup     path  ...
## min triple counts	verb freq	verbs	mean	stdev	stdev/sqrt(N)	mean  ...



#set terminal postscript color landscape enhanced dashed lw 1 "Times" 24
set terminal postscript landscape enhanced dashed lw 1 "Times" 20

set pointsize 1.2                             # larger point!
set xlabel '{/Times-Italic th} ' offset 0,0.5

set xr [1:50] 
set xtics font "Times, 16"
set logscale x

set ytics font "Times, 16"

#set key bottom #reverse 
set key top


do for [minfreq in "50"] {

  ### 
  # plot number of verbs (col 3) (lin and cosine measures are equal in that)
  unset yr

  set title "Number of verbs in WordNet\n{/*0.8Filtering triples with frequency under {/Times-Italic th}}"  
  set ylabel 'Number of verbs' offset 3
  set output  "number_of_verbs-in_WN-min_freq_".minfreq.".eps"

  plot    "results.all_verbs.lin.filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:3 title 'all verbs' with linespoints , \
	  "results.per_freq.lin.filter_triples_all_mins.min_freq_".minfreq.".high_freq.csv" using 1:3 title 'high frequent verbs' with linespoints , \
	  "results.per_freq.lin.filter_triples_all_mins.min_freq_".minfreq.".mid_freq.csv" using 1:3 title 'mid frequent verbs' with linespoints , \
	  "results.per_freq.lin.filter_triples_all_mins.min_freq_".minfreq.".low_freq.csv" using 1:3 title 'low frequent verbs' with linespoints 

  ##########
  ### now plot the wn measures
  set key bottom

  set yr [0:0.25] 

  do for [meas in "lin"] {


    set title "WordNet {/Times-Italic path} Similarity\n{/*0.8Filtering triples with frequency under {/Times-Italic th}}"
    set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  meas."-all_verbs-path-min_freq_".minfreq.".eps"
    set key bottom

    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:4 title 'k=5' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:10 title 'k=10' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:16 title 'k=15' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:22 title 'k=20' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:28 title 'k=50' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:34 title 'k=100' with linespoints

    # plor per freq (just 3 selected k: 5, 10, 50)
    k=5

	    ## plot average for each verb range
	    set title "{/Times-Italic s_{".meas."}}: WordNet {/Times-Italic path} for different frequency ranges. {/Times-Italic k=".k."}"  
           set ylabel '{/Times-Italic WN similarity}' offset 3
	    set output  meas."-freq_ranges-path-k".k."-min_freq_".minfreq.".eps"
	    plot "results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".high_freq.csv" using 1:4 title 'high frequent verbs' with linespoints , \
		"results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".mid_freq.csv" using 1:4 title 'mid frequent verbs' with linespoints , \
		"results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".low_freq.csv" using 1:4 title 'low frequent verbs' with linespoints 

   k=10 # field 10

	    ## plot average for each verb range
	    set title "WordNet {/Times-Italic path} Similarity for different frequency ranges, {/Times-Italic k=".k."}\n{/*0.8Filtering triples with frequency under {/Times-Italic th}}" 
	    set ylabel '{/Times-Italic WN similarity}' offset 3
	    set output  meas."-freq_ranges-path-k".k."-min_freq_".minfreq.".eps"
	    plot "results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".high_freq.csv" using 1:10 title 'high frequent verbs' with linespoints , \
		"results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".mid_freq.csv" using 1:10 title 'mid frequent verbs' with linespoints , \
		"results.per_freq.".meas.".filter_triples_all_mins.min_freq_".minfreq.".low_freq.csv" using 1:10 title 'low frequent verbs' with linespoints 





    set title "WordNet  {/Times-Italic wup}  for {/Times-Italic s_{".meas."}}"   
    set ylabel '{/Times-Italic wup}' offset 3
    set output  meas."-all_verbs-wup-min_freq_".minfreq.".eps"
    set key top

    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:7 title 'k=5' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:13 title 'k=10' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:19 title 'k=15' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:25 title 'k=20' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:31 title 'k=50' with linespoints , \
         "results.all_verbs.".meas.".filter_triples_all_mins.min_freq_".minfreq.".csv" using 1:37 title 'k=100' with linespoints


  }
}


###################
#### plot the stuff for each freq range (freq of the neighbours)

#do for [freq1 in "low mid high"] {


