# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"

import codecs

import logging
log = logging.getLogger(" "+__name__+"  --- \t")

##############################################
# SynonimList class

''' SynonimList: class to store the list of synonims for a given word
and their similarity. The list of synonims can contain all
related words depending on which similarities are taken into account.'''

##############################################
class SynonimList:

    #############################
    # constructor, create data structures
    #############################
    def __init__(self, lemma, add_negatives=False):
    
        log.debug(" initializing SynonimList with "+lemma)
        
        # store lemma for which we will study the synonim list
        self.lemma=lemma
        
        self.add_negatives=add_negatives
        
        # create a list of synonims: the elements of the list will be 
        #  tupples of (lemma, similarity), this is, which is the similarity
        #  of each lemma wrt the studied lemma. The lemma can
        #  be given as an id, or as lemma. It will be an string
        self.synonims=[]
        
        # store wether the list has sorted (some methods need them sorted and some don't)
        self.is_sorted=False
        
    #############################
    # alternative constructor: from a given list of tupples (useful for cuts of the existing lists)
    #############################
    def set_syns(self, syn_list, is_sorted):
        
        log.debug(" initializing SynonimList with a given list of synonims ")
        
        # create a list of synonims:
        self.synonims=syn_list
        
        # store wether the list has sorted (some methods need them sorted and some don't)
        self.is_sorted=is_sorted
        
    ########################
    # destructor
    ########################
    #def __del__(self):
    #    logging.info("  SynonimList: closing...")

    ########################
    # len : return the length of the list
    ########################
    def len(self):
        return(len(self.synonims))

    ########################
    # add_synonim: add an entry for the given synonim
    ########################
    def add_synonim(self,lemma,sim):
        
        if self.add_negatives or float(sim)>0.0:
            log.debug("    adding similarity for "+lemma+" "+str(sim))
            # add a tupple of (lemma, similarity). They are stored
            #  as given (strings, float, decimal.Clamped
            self.synonims.append((lemma,sim))
        
    ########################
    # add_synonim_from_dict: sim_dict contains a dictionary of lemma/similarity, we need
    #  to add to synonims the similarities for lemmas in the given seet of lemmas (lemmas_set)
    #
    ### WARNING: I am doing it like this because I implement synonim_list as a list of tupples
    # because I needed it to be like this. I am not sure it is still necessary, and given that
    # we are using shelve dictionary of dictionaries it may be easier to built everything
    # as dictionaries. But check the distance mesaures first, it was for sorting issues
    # I took the decision of suing tupples instead of dicts
    ########################
    def add_synonim_from_dict(self,sim_dict,lemmas_set):
        for k in sim_dict:
            if k in lemmas_set:
                self.add_synonim(k,sim_dict[k])
                
        ## waning: we store the dictionaries without zeros, so here we are also adding
        # only non-zero elements. This means that the list may be empty, if all similarities
        # were zero
        
        
    ########################
    # add_all_synonims_from_dict: sim_dict contains a dictionary of lemma/similarity, we need
    #  to add to synonims the similarities for  ALL lemmas
    ########################
    def add_all_synonims_from_dict(self,sim_dict):
        for k in sim_dict:
            self.add_synonim(k,sim_dict[k])
                
        ## waning: we store the dictionaries without zeros, so here we are also adding
        # only non-zero elements. This means that the list may be empty, if all similarities
        # were zero    

    ########################
    # sort_by_sim: sort the list of synonims **in_place** 
    #  by default the sorting is from more similar to less similar
    ########################
    def sort_by_sim(self, rev=True):
        
        #self.synonims contains tupples of (lemma, similarity). 
        #  sort it by the second element of the tupple (similarity)
        log.debug("   sorting by similarity.\n   Original list: "+str(self.synonims))
        self.synonims.sort(key=lambda tup: tup[1], reverse=rev)
        log.debug("\n          New list: "+str(self.synonims))
        
        # set is_sorted to true
        self.is_sorted=True
        
    ########################
    # sorted_by_sim: returns the list of synonims sorted by sim.
    #  it sorts from more to less similar, by default
    ########################
    def sorted_by_sim(self,rev=True):
        
        #self.synonims contains tupples of (lemma, similarity). 
        #  sort it by the second element of the tupple (similarity)
        log.debug("   sorting by similarity.\n   Original list: "+str(self.synonims))
        return sorted(self.synonims, key=lambda tup: tup[1], reverse=rev)
        
    ########################
    # get_sim_for_lemma(l): givena  lemma, return its similarity
    ########################
    def get_sim_for_lemma(self,l):
        ## note: if the lemma is not found in vi, we will get 0 similarity, since the null
        ## similarities are not stored in the thesaurus
        
        s=0.0 # if not found, the returned similarity will be zero
        
        for item in self.synonims: 
                if item[0] == l:
                    s=item[1]
                    break
        return s
    
    ########################
    # get_sim_list: returns the list of similarities
    ########################
    def get_sim_list(self):    
        return [float(j) for i,j in self.synonims]
    
        
    ########################
    # get_lemma_list: returns the list of lemmas (first key)
    ########################
    def get_lemma_list(self):
        return [i for i,j in  self.synonims]
    
    ########################
    # get_k_list: returns the list of synonyms with the k more similar ones
    #  the list has the same form that self.synonims (list of pairs)
    # k equal to -1 return the original list
    ########################
    def get_k_list(self,k): 
        
        log.debug("  create k list with k: "+str(k))
        
        # First of all, sort the list from more similar to less similar if it has not been sorted yet
        #  (since we may call this function with different k values, they may be already sorted).
        if not self.is_sorted: 
            self.sort_by_sim() # now self.synonims is sorted
        
        # if k=-1, return the whole list
        if k==-1:
            log.debug("  returning the original list: "+str(self.synonims))
            return self.synonims
        
        # if k>0, return a list with the k first elements os self.synonims
        log.debug("  returning k-list: "+str(self.synonims[0:k]))
        
        return self.synonims[0:k]
    
    ########################
    # get_lemma_position: get the position of a given lemma in the ranking
    #  of similarties. Note that this only makes sense if the synonims have been 
    #  sorted
    ########################
    def get_lemma_position(self, lemma):
        if not self.is_sorted: 
            self.sort_by_sim() # now self.synonims is sorted

        # OJU!! we want to give the ranking without taking ties into account,
        #  so it is not the postiion in the list, but we have to count how many
        #  different sims we have
        s=0
        i=0
        for l,sim in self.synonims:
            log.debug("   l, sim "+str(l)+", "+str(sim))
            if sim>s: # new similarity, ad one to the counts
                i+=1
                log.debug("       new similarity found "+str(s)+"  position: "+str(i))
            if l==lemma: # we found the lemma, return the current position
                log.debug("   target lemma found at position: "+str(i))
                return i
        
        # if we are still here, the lemma was not found! --> return nan
        return -1
