#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# read_shelf_thes.py: reads shelf file that contains a thesaurus and 
#   outputs it (for now, just ot check)
# 
# usage:
#  readshelfthes.py shelf_file --log level
###########################################################

import logging
log = logging.getLogger(" thesaurus2shelf.py)  --\t")

from optparse import OptionParser

#ifrom itertools import izip
import codecs
import sys
#import set

from contextlib import closing
import shelve

#####################
# get_all_keys(shelf_file): returns  the SET of keys in dict
def get_all_keys(shelf):
    keys=set()
    for k in shelf: keys.add(k)
    
    return keys

####################
# add_line2dict: given the shelf file and a line,
#  read the fields to add it to the shelf  dictionary
def add_line2dict(shelf,line,store_zeros=False):

    # split the line into fields
    linep = line.split() 
    
    #  linep[0] is the first lemma,  linep[2] the second, and linep[4] is the similarity
    #    linep[1] and linep[3] are the ids of the lemma we can use either the lemma or 
    #    the id. At the moment, I use the lemma, it makes debugging more readable
    if linep[0] not in shelf: # first time we see the lemma, need to create an empty dictionary that will contain similarities
        log.debug("     creating new entry for lemma " + linep[0])
        shelf[linep[0]]={}

    ## store zeros only if store_zeros is True (much more costly)
    # note that if all similarities are zero for a lemma the contents of shelf[lemma] will be an empty dictionary.
    if store_zeros or float(linep[4]) != 0.0: 

        log.debug("  adding similarity between " + linep[0]+" and "+linep[2]+": "+linep[4])
       
        # now add the contents
        log.debug("       .." + str(shelf[linep[0]]))

        shelf[linep[0]][linep[2]]=linep[4]
        log.debug("       ... added" + shelf[linep[0]][linep[2]])
 

