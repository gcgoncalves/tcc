# -*- coding: UTF-8 -*-

### functions to compute the overlap of two sequences ####

__author__="muntsa"
__date__ ="$06/05/2015$"

import logging
log = logging.getLogger(" "+__name__+"  --- \t")

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


##################
# check_overlap: checks the overlap of the two sequences for N from 1 to
#  L, this is, augmenting the window of elements we take into account
# Warning: The lists may have different sizes, we need to check up 
#  to the longest list
def check_overlap(s1,s2):
        
    overlap=[]
    
    # overlap list will have as many elements as the biggest list between s1 and s2
    for i in range (1,max(len(s1),len(s2))+1):
        
        log.debug("     sequence1 of N="+str(i)+"   "+str(s1[0:i]))
        log.debug("     sequence2 of N="+str(i)+"   "+str(s2[0:i]))
        
        log.debug("        overlaping list: "+str(intersect(s1[0:i], s2[0:i])))
        log.debug("          overlaping size: "+str(intersect_size(s1[0:i], s2[0:i])))
        
        overlap.append(intersect_size(s1[0:i], s2[0:i]))

    log.debug("   whole overlap list:  "+str(overlap)+"\n")

    return overlap


#################
# rel_overlap(overlap): given the overlap array,
#  compute the relative_overlap, which is a similar
#  array but with each elemnt divided by the index of the 
#  array. This measures how many elements are comon with 
#  respect to those that could be common. If the sequences
#  are identical, all rel_overlap elements will be 1, if it is 
#  random, it will start in 0 and end in 1
def rel_overlap(overlap):
    
    rel_overlap=[]
    for i in range (0,len(overlap)):
        rel_overlap.append(float(overlap[i])/float(i+1))

    return rel_overlap

#################
# rel_overlap_score(overlap): given the overlap array,
#  compute a global measure that gives the relative_overlap
#  computing the mean of the re_overlap array
def rel_overlap_score(overlap):
    
    return array_average(rel_overlap(overlap))

#################
# array_average: compute the average of the elements in a array
def array_average(a):
    tot=0.0
    for x in a:
        tot+=float(x)
    
    if len(a)==0:
        return 0.0
    else:
        return tot/float(len(a))

#################
# intersect: compute the overlaping elements of two lists
def intersect(a, b):
    return list(set(a) & set(b))

#################
# intersection_size: compute the size of overlaping elements of two lists
def intersect_size(a, b):
    return len(set(a) & set(b))



#################
# list_to_rank: convert a list of lemmas (sorted by sim) 
#   into a dict lemma-rank
def list_to_rank(lem_list):
    d={}
    l=len(lem_list)
    for i in range(0,l):
        d[lem_list[i]]=l-i
        
    return d

#################
# get_cosine: compute a score for similarity of ranks comparing the 
# ranks with a cosine similarity
def get_cosine(lemma_list1,lemma_list2):
    
    # if the lists are empty, just return 0
    if len(lemma_list1)==0 and len(lemma_list2)==0:
        return 0.0
    
    # the cosine is computed comparing two vectors built
    #  with the position of each lemma in the list, i.e. its ranking
    #  but having the first element the highest score, so length-position

    # the two ranks may have different lengths, so we take the longest
    #  one as length 
    leng=max(len(lemma_list1),len(lemma_list2))
    
    log.debug("  computing cosine for "+str(lemma_list1)+"  "+str(lemma_list2)+ "   max len: "+str(leng) )
    
    # convert each list of lemmas into a dict lemma-rank that will
    #  ease the comparison
    dict1=list_to_rank(lemma_list1)
    dict2=list_to_rank(lemma_list2)
    
    # compute cosine
    cos=0
    for l in dict1:
        if l in dict2:
            cos+=dict1[l]*dict2[l]
        # if the lemma is not in both lists, it is ignored (equivalent to add 0)

    # compute normalization factor
    #### warning: I am computing the normalizing factor summing only 
    ###  elements up to largest length of dicts, but I am not sure at all that it makes sense
    ### it may be more logical to add also the uncommon elements,
    ###  which will penalize in fact the cosine?
    norm=0
    for i in xrange(1,leng+1):
        norm+=i*i
    
    log.debug("  sum: "+str(cos)+"  norm: "+str(norm))
    return float(cos)/float(norm)
