#! /usr/bin/python3
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"

''' This implements class and methods to compare vectors of similarities
  obtained with different measures, thesholds, etc '''

import logging
log = logging.getLogger(" "+__name__+"  --- \t")

from decimal import Decimal
from copy import deepcopy

from synonim_list import SynonimList

# import file with overlap computing functions
import overlap
# and with kendall tau computing
## import kendall  # ---> this is buggy, I don't use it any more!

# better kendall tau-b 
import scipy.stats as ss


#################################################
# CompareSynLists: class for comparing two (or more, in the future??) lists
#  of synonims. The idea is that this class prepares the synonim lists,
#  calls the relevant functions to compare the lists and stores the results. 
#
# Preparing the lists includes setting the number of deciamls to be taken into
#  account, sorting them, removing zeros if necessary, setting threshold (to do)...
# 
#  The class also has methods that return the measures as strings to be printed in a file
#################################################
class CompareSynLists:
    
    #############################
    # constructor, gets the two lists of synonims to be 
    #  compared and prepares data
    #############################
    def __init__(self, v1, v2, prec, common_lemmas, count_zeros=False,equal_lengths=False):
        #self.overlap=[]
        #self.rel_overlap=0 ## to do: think what happens with empty vectors, 0 values, etc...
        
        ####
        # equal_lengths states if we want the two lists to be created with the same lenght. If so,
        #  the elements that have zero similarity in one list but not in the other will be added
        #  with sim 0 . If it is set to False, only the non-zero similarity will be added to the
        #  lists, what may imply creating losts with different lists.
        
        # count how many non-zero similarities the vectors have
        self.no_null=0
        # count if there are synonims that have similarity different than 0 in one list but not in the other (strange!)
        self.rare_null=0
        
        # prec states the number of decimals to be taken into account
        #  (4 bu default, i.e, 0.0001 precision. We will use Decimal
        #  to deal with similarities with the given precision
        self.pr=Decimal(10) ** (-1*int(prec))
        
        # set the value of 0.0, for selecting similarities greater than zero
        self.zero=Decimal('0.0').quantize(self.pr)
               
        # creates a new synonim list for each list with the desired precison
        #  and removes zeros if necessary
        ## waning: we stored the dictionaries without zeros, so the vectors only have
        # only non-zero elements. This means that the list may be empty, if all similarities
        # were zero, and that if we really want to take zeros into account we need
        # to add them
        log.debug("    creating lists to be compared...")
        self.l1=SynonimList(v1.lemma)
        self.l2=SynonimList(v2.lemma)

        ## convert each element of the list to a Decimal with the desired precision    
        #   if count_zeros is False (default), do not take into account lemmas with zero similarity 
        #    in both lists (given the choosen precision). The pure zeros are not in the
        #   list already, but there may be other similarities that are zero for determined
        #   precisions
        

        # in case we have lists with different sizes, first of all we will get the 
        #  intersection in terms of lemmas
        #    [i for i,j in v1.synonims] --> creates a list with just the first element of each tupple (tupples lemma, similarity)
        ### I changed this, because I am doing the intersection in "compare_thesaurus"
        ###  and becasue I am not storing zeros in the dicitonary, so the lemmas in the 
        ###  vectors are no longer representative of  the common lemmas of the thesarus. Instead, I
        ###  know the common lemmas from compare_thesaurus, so init() receives them as a parameter
        ## common_lemmas=set([i for i,j in v1.synonims]) & set([i for i,j in v2.synonims])
        ##log.debug("    getting common lemmas:\n"+str(common_lemmas))
        
        # store the total number of common lemma, which is the maximum number of synonims we may have
        # (that will be used just for output)
        self.max_len=len(common_lemmas)
       
        # create the similarity vectors with the given precision
        for l in common_lemmas: # get s1 and s2 for this lemma            

            osim1=v1.get_sim_for_lemma(l) # similarity as it is in the original thesaurus 
            osim2=v2.get_sim_for_lemma(l) # similarity as it is in the original thesaurus
            
            ## note: if the lemma is not found in vi, we will get 0 similarity, since the null
            ## similarities are not stored in the thesaurus
            
            s1=Decimal(osim1).quantize(self.pr) # set similarity1 for this lemma with the desired precision
            s2=Decimal(osim2).quantize(self.pr) # set similarity2 for this lemma
            
            # log.debug("      ..... similarites for "+str(l)+" s1:"+str(s1)+" s2:"+str(s2))
            
            ## if equal_lengths is True (old option, in fact, to have equally large lists)
            if equal_lengths:
                # add similarities different than zero in any of the two lists
                #if s1!=self.zero or s2!=self.zero:
                if s1>self.zero or s2>self.zero: ## I check greater than 0 rather than different, since some measures deliver negative similaritt  scores
                    
                    # we want to count how many similarities are different than zero. If we do not count
                    # zeros, it is just the size of the new created lists. If we are counting zeros, we have 
                    #  to count it now.
                    self.no_null+=1
                    
                    self.l1.add_synonim(l,s1)
                    self.l2.add_synonim(l,s2)
                    log.debug("      added common lemma: "+l+" s1: "+str(s1)+" s2: "+str(s2))
                    
                    # just to check, it is strange that one similarity is zero and the other is
                    #  bigger than zero, we will report that
                    # WARNINg: I originally compared in terms of cut similarities, i.e. with the selected 
                    #  precision, but I changed it to the original similarity to detect the cases in
                    #  which the similarities are really 0, not just smaller than the used prec.
                    ##if s1!=self.zero and s2==self.zero:
##                    if float(osim1)!=0 and float(osim2)==0:
##                        log.warning(" \n  ------> s1 is different than 0 but s2 is 0!!\n   original lemma: "+self.l1.lemma+"\n    compared to: "+l+" -- s1: "+str(osim1)+" s2: "+str(osim2))
##                        self.rare_null+=1
##                    if float(osim1)==0 and float(osim2)!=0:
##                        log.warning(" \n  ------> s2 is different than 0 but s1 is 0!!\n   original lemma: "+self.l1.lemma+"\n    compared to: "+l+" -- s1: "+str(osim1)+" s2: "+str(osim2))
##                        self.rare_null+=1
        
                elif count_zeros: # if count_zeros is True, we also add the zero similarity entries
                    self.l1.add_synonim(l,s1)
                    self.l2.add_synonim(l,s2)
                    log.debug("      added common lemma: "+l+" s1: "+str(s1)+" s2: "+str(s2))
                
            ## new option: we just add non-zero similarities to the list that has non-zero sim
            #  it may imply having different sizes in the lists, but we will manage it in another
            #  place. Note: with this approach I consider that the option count_zeros makes
            #  no sense, or at least should be thought a little bit more.
            else:
                if count_zeros: 
                    log.error(" We do not expect to have count_zeros=True when considering differenttly large lists!")
                    exit(1)
                
                if s1>self.zero: ## I check greater than 0 rather than different, since some measures deliver negative similaritt  scores
                    self.l1.add_synonim(l,s1)
                
                if s2>self.zero:
                    self.l2.add_synonim(l,s2)
    
    #############################
    # compare_permutation_lists: apply all measures to compute similarities
    #  assuming that the lists are one permutation of the other. This means that
    #  both of them have the same elements but in different orders (same size,
    #  same contents.
    #############################
    def compare_permutation_lists(self):
        
        log.debug("     ...... comparing two lists of synonims with the same size and elements....")
        
        if self.l1.len()!=self.l2.len():
            log.error(" compare_permutation_lists needs two lists with same size! Use other methods if the lists are different!")
            exit(1)
        
        ## note_ this function assumes that the order of the lists is the same in both lists according
        #  to lemmas, thus the lists must not have been sorted. This is old staff, I am just trying
        #  to keep it working, even if it is not very nice.
        # Give an error if the lists have been sorted
        if self.l1.is_sorted or self.l2.is_sorted:
            log.error("  compare_permutation_lists must NOT have the lists previously sorted. It needs both lists to follow the same lemma order")
            exit(1) 
        
        ## measure overlap
        # check overlap of both vectors (in terms of number of elements taken into account)
        #   [i for i,j in l1] --> creates a list with just the first element in each element of the list (tupples lemma, similarity)

        # to get overlap, sort given lists by similarity. What overlap function needs is the list of lemmas from
        #  more similar to less similar
        self.rel_overlap=self.compute.overlap([i for i,j in self.l1.sorted_by_sim()], [i for i,j in self.l2.sorted_by_sim()])
        
        ## compute kendall tau (compares the rankings)
        # we compute kendall tau b with scipy.stats.kendalltau (alias ss.kendalltau)
        # this funtion takes two arrays (or lists) as input, containing in each postition
        # the ranking of a given item. We can use directly similarity for ranking (the function
        # performs the sorting).
        ## we need to give to the function the lists of similarity, with each element of the list
        # being the similarity of the same lemma
        # Thus, what we give to the function are the values of similarity for each measure,
        # with the same order in terms of lemma. This is, in position i, both arrays must
        # have the similarity for the same lemma_i. This is ensured if the list has not been sorted
        # inplace. The function returns tau-b and its p-value
        
        # create the lists of similarities. In this case, since the lists has not been sorted, the
        #  syn_lists already have the same order.
        sim1=self.l1.get_sim_list()
        sim2=self.l2.get_sim_list()
        
        log.debug("      sim1 "+str(sim1))
        log.debug("      sim2 "+str(sim2))
        
        self.taub, self.taubpvalue=self.compute_kendall_tau(sim1,sim2)
        
        
    #############################
    # compare_lists(k): apply all measures to compute similarities taking into account
    #  only the first k elements when sorted by similarity
    #   k may be between 1 and infinite. If k is greater tha the elements in list
    #  all elements will be taken into account. If k=-1 all elements will be
    #  taken into account. k=-1 should be equivalent to the previous function
    #############################
    def compare_lists(self,k):
        
        log.debug("     ...... comparing the two lists of synonims for k: "+str(k))        
        
        # note, the two lists we have created at the moment should not contain zeros
        #  it is done creating the object with the default option equal_lengths=False
        
        # cut the two lists to get the top k values (also sorts them if necesary)
        cut_l1=SynonimList(self.l1.lemma)
        cut_l1.set_syns(self.l1.get_k_list(k),True)
        
        cut_l2=SynonimList(self.l2.lemma)
        cut_l2.set_syns(self.l2.get_k_list(k),True)
        
        # store the list of lemmas for each case, we will need them more than once
        lemmas1=cut_l1.get_lemma_list()
        lemmas2=cut_l2.get_lemma_list()
        
        # now we have two lists that may have different sizes (if there are zeros)
        # we need to compare them. We will compute taub , cosine and relative overlap.
        # for overlap, we can compute it even with different sizes, but for taub and cosine we
        # need to have the same sizes for both list. Thus, we study create two cases:
        #  the intersection and the union of the lists. In the case of the union, the elements
        #  that are in one list but not in the other will be considered 0.
        
        ## 1-measure overlap and for the given lists (with different sizes)
        # check overlap of both vectors (in terms of number of elements taken into account)
        # What overlap function needs is the list of lemmas from
        #  more similar to less similar
        ## note: the lists are already sorted by similarity, I just need to get the list of lemmas
        log.debug("  ------------ 1 --------------")
        log.debug("  get rel_overlap for the list as they are")
        rel_overlap=self.compute_rel_overlap(lemmas1, lemmas2)
        cosine=self.compute_cosine(lemmas1, lemmas2)

        ## 2- measure taub and overlap for the intersection set
        # get the union of lemmas
        log.debug("  ------------ 2 --------------")
        log.debug("  get rel_overlap and taub for the intersection of lemmas")
        intersection=set(lemmas1)&set(lemmas2)
        log.debug("       lemmas intersection: "+str(intersection))
        
        # compute kendall tau and overlap with just the common lemmas        
        #  oju with the order. For overlap I need to keep the order in lemmas1 and lemmas2
        # for taub I need to give the similarities for the same lemma in the same position

        # call overlap with the desired set of lemmas and it will create the lists
        # and make the computations
        rel_overlap_i=self.compute_rel_overlap_from_set(intersection, cut_l1, cut_l2)
        
        # call kendall tau with the desired set of lemmas and it will create the lists
        # and make the computations
        taub_i, pvalue_i=self.compute_kendall_tau_from_set(intersection, cut_l1, cut_l2)
        
        # call cosine
        cosine_i=self.compute_cosine_from_set(intersection,cut_l1, cut_l2)
              
        ## 3- measure taub for the union set. It does not make sense to
        # compute overlap or cosine for union, sniece they already take 
        # into account uncommon elements
        log.debug("  ------------ 3 --------------")
        log.debug("  get taub for the union of lemmas this is inf act the desired tau-b)")
        union=set(lemmas1)|set(lemmas2)
        log.debug("       lemmas union: "+str(union))
        
        # compute kendall tau and overlap with just the common lemmas        
        #  oju with the order. For overlap I need to keep the order in lemmas1 and lemmas2
        # for taub I need to give the similarities for the same lemma in the same position
        
        # call kendall tau with the desired set of lemmas and it will create the lists
        # and make the computations
        taub_u, pvalue_u=self.compute_kendall_tau_from_set(union, cut_l1, cut_l2)
        
        ## I decided that it does not make sense to compute rel overlap
        ## and cosine for union
        # call overlap with the desired set of lemmas and it will create the lists
        # and make the computations
        ##rel_overlap_u=self.compute_rel_overlap_from_set(union, cut_l1, cut_l2)
        # call cosine
        ##cosine_u=self.compute_cosine_from_set(union,cut_l1, cut_l2)        
        
        # done
        
        ## return all the measures
        # orginal lengths (1 and 2)  cut lengths (1 and 2), intersection length, union length, rel_overlap, rel_ovelap_intersection, 
        #   rel_ovelap_union, taub_intersection, pvalue_taub_intersection, taub_union, pvalue_taub_union,
        return self.l1.len(), self.l2.len(),cut_l1.len(),cut_l2.len(),len(intersection),len(union),rel_overlap, \
            rel_overlap_i, cosine, cosine_i, taub_i, pvalue_i, taub_u, pvalue_u
        
        
    #############################
    # compute_rel_overlap_from_set(set, l1, l2): computes the rel_overalp
    #  of the two given lists with the lemmas given in the set
    #   If the a lemma in the set is not in lx, we add 0 sim,
    # if a lemma in lx is not in the set, we ignore it
    #############################
    def compute_rel_overlap_from_set(self,lemmas, l1, l2):
        
        # For overlap I need to keep the order in l1 and l2 (already sorted)
        log.debug("  computing rel_ovelap for a given set of lemmas "+str(lemmas))
        
        # create the two lists of lemmas ordered by similarity, but
        # just if the lemma is in "lemmas" set
        lem1=[]
        lem2=[]
            
        for s in l1.synonims: # walk over the pairs of lemma, similarity
            if s[0] in lemmas: # the lemma of this pair is in the set of pairs, add the lemma to the list
                lem1.append(s[0])
                
        for s in l2.synonims: # walk over the pairs of lemma, similarity
            if s[0] in lemmas: # the lemma of this pair is in the set of pairs, add the lemma to the list
                lem2.append(s[0])
                
        ## we also need to check for lemmas in the set that were not in the original
        # lists. In these case, add the lemma to the end.
        #### I SHOULD CHECK IT; FOR SURE THERE IS A BETTER WAY TO DO THAT 
        for l in lemmas:
            if l not in lem1:
                lem1.append(l)
            if l not in lem2:
                lem2.append(l)
        
        log.debug("         lem1 "+str(lem1))
        log.debug("         lem2 "+str(lem2))
        
        return self.compute_rel_overlap(lem1, lem2)

        
    #############################
    # compute_rel_overlap(list1,list2): compute rel overlap given two list of lemmas
    #  already sorted by similarity. 
    #############################
    def compute_rel_overlap(self,lemma_list1,lemma_list2):

        log.debug("\n \n  ====== compute overlap ======")
        
        log.debug("      get overlap ....")
        overlap_l=overlap.check_overlap(lemma_list1,lemma_list2) # the function compares the two lists of lemmas ordered by similarity
        # overlap_l is a list of overlaping elements for different sizes of windows
                        
        # what we want to compute is the "relative overlap", which gives us a single number for each verb.
        # Thus, we can compute the mean for all verbs and get a measure of global overlap...
        log.debug("      get relative overlap ....")
        rel_o=overlap.rel_overlap_score(overlap_l) 
        
        log.debug("           relative overlap is "+str(rel_o)+"\n")
        
        return rel_o

    #############################
    # compute_cosine_from_set(list1,list2): compute cosine     
    #  of the two given lists with the lemmas given in the set
    #   If the a lemma in the set is not in lx, we add 0 sim,
    # if a lemma in lx is not in the set, we ignore it
    #############################
    def compute_cosine_from_set(self,lemmas, l1, l2):
        
        # For cosine I need to keep the order in l1 and l2 (already sorted)
        log.debug("  computing cosine for a given set of lemmas "+str(lemmas))
        
        # create the two lists of lemmas ordered by similarity, but
        # just if the lemma is in "lemmas" set
        lem1=[]
        lem2=[]
            
        for s in l1.synonims: # walk over the pairs of lemma, similarity
            if s[0] in lemmas: # the lemma of this pair is in the set of pairs, add the lemma to the list
                lem1.append(s[0])
                
        for s in l2.synonims: # walk over the pairs of lemma, similarity
            if s[0] in lemmas: # the lemma of this pair is in the set of pairs, add the lemma to the list
                lem2.append(s[0])
                
        ## we also need to check for lemmas in the set that were not in the original
        # lists. In these case, add the lemma to the end.
        #### I SHOULD CHECK IT; FOR SURE THERE IS A BETTER WAY TO DO THAT 
        for l in lemmas:
            if l not in lem1:
                lem1.append(l)
            if l not in lem2:
                lem2.append(l)
        
        log.debug("         lem1 "+str(lem1))
        log.debug("         lem2 "+str(lem2))
        
        return self.compute_cosine(lem1, lem2)

    #############################
    # compute_cosine(list1,list2): compute cosine of rankings given 
    #   two list of lemmas already sorted by similarity. 
    #############################
    def compute_cosine(self,lemmas1, lemmas2):
        
        log.debug("\n \n  ====== compute cosine ======")
        
        log.debug("      get cosine ....")
        cos=overlap.get_cosine(lemmas1,lemmas2) # the function compares the two lists of lemmas ordered by similarity and computes a cosine similarity
        
        log.debug("           cosine is "+str(cos)+"\n")
        
        return cos
    
    #############################
    # compute_kendall_tau_from_set(set, l1, l2): computes the kendall_tau
    #  of the two given lists with the lemmas given in the set
    #   If the a lemma in the set is not in lx, we add 0 sim,
    # if a lemma in lx is not in the set, we ignore it
    #############################
    def compute_kendall_tau_from_set(self,lemmas, l1, l2): 
        
        log.debug("  computing kendall tau for a given set of lemmas "+str(lemmas))
        
        ## we need to give to the function the lists of similarity, with each element of the list
        # being the similarity of the same lemma. WARNIGN: the similarityies must be floats!
        
        # create the two lists of similarities ordeded by the lemma that will contain
        #  only the lemmas in lemmas set
        sim1=[]
        sim2=[]
            
        for l in lemmas:
            sim1.append(float(l1.get_sim_for_lemma(l))) # this adds 0 if the lemma is not in l1
            sim2.append(float(l2.get_sim_for_lemma(l)))
                
        log.debug("         sim1 "+str(sim1))
        log.debug("         sim2 "+str(sim2))
        
        return self.compute_kendall_tau(sim1,sim2)  


    #############################
    # compute_kendall_tau: compute the kendall tau fucntion 
    #############################
    def compute_kendall_tau(self,sim1,sim2):
        
        log.debug("\n\n   ====== compute kendall tau ======")
        log.debug("     sim1 list: "+str(sim1))
        log.debug("     sim2 list: "+str(sim2))
        # we compute kendall tau b with scipy.stats.kendalltau (alias ss.kendalltau)
        # this funtion takes two arrays (or lists) as input, containing in each postition
        # the ranking of a given item. We can use directly similarity for ranking (the function
        # performs the sorting). Note: if we use similarity, the sorting will be done
        # from less similar to more similar, but I don't think this changes the result (it shouldn't!)
        # Thus, what we give to the function are the values of similarity for each measure,
        # with the same order in terms of lemma. This is, in position i, both arrays must
        # have the similarity for the same lemma_i. This is ensured if the list has not been sorted
        # inplace. The function return tau-b and its p-value
        
        # WARNING: if the list have length 1 or 0, ss.kendalltau fails, so we check it here (lleig, pero buenu...)

        taub, taubpvalue=0,0 # default (e.g. length 0 or length 1 with 0 sim)
        
        # special case: if both lenghts are one, tau will be one if both similarities are different than zero, and zero if one is 0
        if len(sim1) ==1 and len(sim2) ==1: 
            if sim1[0]!=self.zero and sim2[0]!=self.zero:
                taub, taubpvalue=1,1
            # else:  taub, taubpvalue=0,0 no need to specify that, it is the default value
            
        ## note: I am not sure what happens with one list of size one and the other bigger, for
        ## the moment I assume it will work well in the next step...
            
        elif len(sim1) !=0 and len(sim2) !=0: # lists bigger than 1, compute kendall tau with ss
            log.debug("\n  calling kendalltau") 
           
            r=ss.kendalltau(sim1,sim2) ## we are giving the similarities in lemma order, each position in the list talking about the same lemma
            # note, the function should return two vaues (taub, pvalue) but in some cases
            #  it returns 1 (when both lsits are equal and a perfect tie e.g. [1,1,1,1,1]
            
            # thus, we need to check whether r is a tuple
            if isinstance(r, tuple):
                taub, taubpvalue=r
                # another special caseis when one of the vectors is a prefect tie (no variance in the vector)
                #  then the fucntion returns nan (and it seems the prefered behavior).
                #  I don't know what to do with that case!!
##                if (taub==nan):
##                    taub=0.5 #???
##                    taubpvalue=1.0 # !!??? I use 1 to show that there is something strange!!
            else:
                taub=1.0
                taubpvalue=1.0 # !!??? I use 1 to show that there is something strange!!
                log.warning(" --ss--   Kendalltau did not return two values! assuming pvalue=1.0...\n")
                log.warning(" --ss--           sim1 was: "+str(sim1))
                log.warning(" --ss--           sim2 was: "+str(sim2))
                log.warning(" --ss--              kendalltau returned: "+str(r))
                
        
        log.debug("           taub, p-value "+str(taub)+", "+str(taubpvalue)+"\n")
        return taub, taubpvalue


    #### functions for measure output ######
    # get_string_X: returns the measure X as a string
    #############################
    def get_string_taub(self):
        return str(self.taub)

    def get_string_taubpvalue(self):
        return str(self.taubpvalue)

    def get_string_reloverlap(self):
        return str(self.rel_overlap)
    
    def get_string_overlap(self): # mmm... overlap is a list! --> we won't probably use it, but just in case!
        str_overlap=""
        for i in range(0,len(self.overlap)):
            str_ovelap=str_overlap+str(i-1)+"\t"+str(self.overlap[i])+"\n"
        
        return str_overlap

    # return sorted similarities for manual inspection
    def get_string_sorted_sim(self):
        
        ssim="====================\n"+self.l1.lemma+"\n\tnon-zero similarity:\t" +str(self.no_null)+"\n\tjust-one-zero similarity (included in non-zero):\t" +str(self.rare_null)+"\n\tzero similarity:\t" +str(self.max_len-self.no_null)+"\n\ttotal possible synonims (common lemmas):\t" +str(self.max_len)+"\n====================\n"
        
        # sort vectors
        l1sorted=self.l1.sorted_by_sim()
        l2sorted=self.l2.sorted_by_sim()
        
        for i in range(0,len(l1sorted)):
            ssim=ssim+str(l1sorted[i][0])+"\t"+str(l1sorted[i][1])+"\t--\t"+str(l2sorted[i][0])+"\t"+str(l2sorted[i][1])+"\n"
            
        return ssim

    
###################################
### compare_similarities:  compare two dictioanaries that have similarities
###  computed in diferent ways (key of the dict is id of a verb, same ids 
###  in the two dicts are associated to same lemma)
###################################
##def compare_similarities(v1,v2, lemma, f,verbose=False):
##
##    # (here we can implement different measures, if we want)
##    # 1st measure: Kendall tau
##    
##    #  to compare v1 and v2, we will compare their ranking
##    #  so we need to sort both of them by the similarity (value of the 
##    #  dict), keeping the id (key of the dict). The measure calculates 
##    #  how different is the order of the ids in each sorted dict
##    sim_sorted_v1=sorted(v1, key=v1.__getitem__, reverse=True)
##    sim_sorted_v2=sorted(v2, key=v2.__getitem__, reverse=True)
##       
##    count=0
##    
##    if verbose==True:
##        # output the ordered list of verbs if their similarity is bigger than 0
##        f.write("\n==========\n"+lemma+"\n==========\n")
##        
##        for i in range(0,len(sim_sorted_v1)):
##            if v1[sim_sorted_v1[i]]!=0 and v2[sim_sorted_v2[i]]!=0:
##                f.write(str(sim_sorted_v1[i])+"\t"+str(v1[sim_sorted_v1[i]])+"\t--\t"+str(sim_sorted_v2[i])+"\t"+str(v2[sim_sorted_v2[i]])+"\n")
##                ##f.write(str(sim_sorted_v1[i])+"\t"+all_verbs[sim_sorted_v1[i]-1]+"\t--\t"+str(sim_sorted_v2[i])+"\t"+all_verbs[sim_sorted_v2[i]-1] +"\n")
##                count+=1
##        f.write("verbs with non-zero similarity:\t" +str(count)+"\n")
##    
##    log.debug("  calling kendalltau with \n  "+str(sim_sorted_v1)+"\n and  "+str(sim_sorted_v2) )
##    
##    if len(sim_sorted_v1)==0 or len(sim_sorted_v2)==0:
##        return (0, 0, 0), 0
##
##    return kendall(sim_sorted_v1,sim_sorted_v2), count
##    



    ## if we manage to install scipy we can call: 
    # return ss.kendalltau(sim_sorted_v1,sim_sorted_v2) ## implemented in scipy
    # return fastfastkendalltau.kendalltau(sim_sorted_v1,sim_sorted_v2) ## optimized implementation
    
#####################
# main
##
##if __name__ == "__main__":
##
##    #log.basicConfig(level=log.DEBUG) # si cal es pot afegir com a parametre
##    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre
##
##
##    ##--- get parameters ---
##    usage = "usage: %prog file1 file2\n"
##    parser = OptionParser(usage=usage, version="%prog 1.0")
##
##    parser.add_option("-s", "--suffix", dest="suf", default="_", help="Suffix that will be added to the output files to label them")
##    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
##
##    (options, args) = parser.parse_args()
##
##    suffix=options.suf
##    count_z=options.count_zeros
##
##    if len(args) != 2:
##        parser.error("incorrect number of arguments")
##        exit (1)
##        
##
##    #if options.vector_type!="frequency" and options.vector_type!="absolute" and options.vector_type!="binary":
##        #parser.error("\nincorrect value for vector_type. May be:\n\t'frequency': number of times each cue has been seen over total number of occurrences, \n\t'absolute': number of times each cue has been seen,\n\t'binary': one if the cue has been seen once or more, zero otherwise.")
##        #exit (1)
##    # ----
##    logging.info(" ======== compare_distances.py ========")
##    #logging.info( " reading file:"+ args[0])
##
##    ##############
##    # We need to read the two files, which contain the same thesaurus with
##    #  similarities computed with different metrics, and compare the metrics
##    #
##    # The file has, for each verb, the similarities with respect to all other
##    #  verbs. To avoid memory problems, we will read one verb at a time (and 
##    #  all its similarities) and make the comparison, then proceed to the 
##    #  next verb.
##    #
##    # At the samle time we read the file, we will build an index with all 
##    #  the verbs that will allow us to relate id and verb. Not sure if
##    #  we need it, though...
##    # Alternativelly, we could read an independent file containing the 
##    #  verb and id pairs first of all and use the sixe of it to create
##    #  arrays and so directly with the desired size. Would it be better???    
##    ##############
##
##    ## initialize variables
##    all_verbs=[]
##    lemma=""
##    # we will create a "vector" containing all similarities of a given verb
##    #  for the measure in each file. The vector is implemented as a
##    #  dictionary, key is index, content is similarity. I do it as a dictionary
##    #  because latter we will need to sort it by value and keep the index
##    v1={} # dict for file 1
##    v2={} # dict for file 2
##    
##    ### to output  verbose stuff
##    fout=open("sorted_sim."+suffix+".out",'w')
##
##    ## open both files to read them at the same time
##    with codecs.open(args[0],'r', encoding='utf8') as file1, \
##      codecs.open(args[1],'r', encoding='utf8') as file2: 
##     # with open(args[0],'r') as file1, open(args[1],'r') as file2: 
##        for l1, l2 in izip(file1, file2):
##            l1p = l1.strip().split()
##            l2p = l2.strip().split()
##            
##            ## the thesaurus are expected to have the same lemas, in the same
##            #   order and with same ids. Report it that does not happen
##            if l1p[0] != l2p[0] or l1p[1] != l2p[1] or l1p[2] != l2p[2] or l1p[3] != l2p[3]:
##                logging.error("   Missmatch in the lemmas or ids in the two files!")
##                logging.error("      line in file1: "+l1)
##                logging.error("      line in file2: "+l2)                
##                exit(1)
##                        
##            # if the l1p[0] is different than lemma it means that
##            #  we reached a new lemma, so we finished to read similarities
##            #  for the current verb. Now we can compare the vector we got
##            #  for each file
##            if l1p[0] != lemma:
##                if len(v1)!=0: # if v1 is empty, this means that we are just 
##                               # starting to read the file, no need to compare
##                    logging.debug("    finished reading similarities for this lemma (" + lemma +")")
##                    # compare v1 and v2
##                    tau=compare_similarities(v1,v2, lemma, fout, True)
##                    print lemma, tau
##                    # re-start lists
##                    v1={}
##                    v2={}
##            
##                # store new lemma (that may not be necessary if we do not need 
##                #  the vector index) --> a little stupid in that way, better to read the
##                # vector index!!
##                lemma=l1p[0]    
##                all_verbs.append(lemma)
##                logging.debug("   starting new lemma " + lemma)
##
##            # add current line info from each file to the corresponding list
##            # we just need to append the current similarity (order is the same in
##            #  both files, so it will be consistent)
##            v1[int(l1p[3])]=float(l1p[4])
##            v2[int(l2p[3])]=float(l2p[4])
##                
##    ### compare the similarities for last verb (once loop is finished)
##    tau=compare_similarities(v1,v2, lemma, fout, True)
##    
##    print lemma, tau


