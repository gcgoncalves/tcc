#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


#################################################
# voting_toefl.py
#
# given the positions of toelfl answers in two different files, perform
#  a voting to create a new solution
# we have different options:
#   for the voting:
#     1- take the highest position for each verb: -v best (default)
#     2- compute the average of the positions: -v average
#   instances to take into account:
#     1- instances that have some non -1 element: -i all (default)
#     2- only instances that have all non -1 in BOTH files: -i strict
#  how to compute the positions: relative or absloute
#     1- use the values as they come in the files: -p absolute (default)
#     2- normalize the positions with respect to their maximum value in order to
#         take into account the different number of neighbours in each thesaurus: -p relative
#
#  value to add to the hoghets postions to simulate last positions: -n value
#
#  In the case that instances with -1 are taken into account, for the average case we 
#   will consider -1 elements to be at the end of the list, this is, we will give to them
#   a position equal to the biggest one +50 (should check to what extend cnaging this
#   modifies the results...)
#
# ./voting_toefl.py file_1.csv file_2.csv 
###################################################

import logging
log = logging.getLogger(" MAIN(compute_average.py)  --\t")

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

import math

#from contextlib import closing

#import bottleneck as bn
import scipy.stats as ss
from numpy import array, vstack


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # options
    #   for the voting:
    #     1- take the highest position for each verb: -v best (default)
    #     2- compute the average of the positions: -v average
    parser.add_option("-v", "--voting", dest="vote", default="best", help="kind of voting (best or average)")
    #   instances to take into account:
    #     1- instances that have some non -1 element: -i all (default)
    #     2- only instances that have all non -1 in BOTH files: -i strict    
    parser.add_option("-i", "--instances", dest="inst", default="all", help="isntances to take into account (all or strict)")

    #  how to compute the positions: relative or absloute
    #     1- use the values as they come in the files: -p absolute (default)
    #     2- normalize the positions with respect to their maximum value in order to
    #         take into account the different number of neighbours in each thesaurus: -p relative
    parser.add_option("-p", "--position", dest="pos", default="absolute", help="how to compute positions (absloute or relative)")

    #  value to add to the hoghets postions to simulate last positions: -n value
    parser.add_option("-n", "--number", dest="num", default="50", help=" value to add to the hoghets postions to simulate last (-1) positions")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of logging desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    if options.vote!="best" and options.vote!="average":
        parser.error("\nincorrect value for voting. Must be best or average")
    
    if options.inst!="all" and options.inst!="strict":
        parser.error("\nincorrect value for instances. Must be all or strict")
    
    if options.pos!="absolute" and options.pos!="relative":
        parser.error("\nincorrect value for position. Must be absolute or relative")
     
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== voting_toefl.py ========")

    ## open both files to read them at the same time
    # in principle both files have the same elements, we will process 
    #  each line in both files at a time
    # format of the files:
    #    game    punt    -1      kiss    -1      halve   -1      gloss   -1
    #    set     mark    167     undo    -1      approve 210     hijack  -1
    #    sat     mark    167     undo    -1      approve 210     hijack  -1 target_not_found
    with codecs.open(args[0],'r', encoding='utf8') as file1, \
      codecs.open(args[1],'r', encoding='utf8') as file2: 
        for l1, l2 in zip(file1, file2):
            
            log.debug("  studying lines:\n"+l1+l2)
            
            l1p = l1.strip().split()
            l2p = l2.strip().split()
            
            t_not_found=""
            if len(l1p)==10 and len(l2p)==10: # none of the lemmas was found in the thesaurus, we will output it
                t_not_found="target_not_found"
            
            # check that the lines are equal, i.e., they have the same lemmas
            for i in [0,1,3,5,7]:
                if l1p[i]!=l2p[i]:
                    log.error(" the lines in both files are not equal!\n"+l1+l2 )
                    exit (1)
                
            # we will need the maximum position in each case to substitute -1,
            #   (and to compute the relaticve postiions if this is the option)
            # compute it now
            max1=-1
            max2=-1
            for j in [2,4,6,8]:
                max1=max(int(l1p[j]),max1)
                max2=max(int(l2p[j]),max2)            
            logging.debug("          max1 "+str(max1))
            logging.debug("          max2 "+str(max2))            
            
            # to subsitut -1 we will use the maxximum among both 
            max_a=max(max1,max2)
                        
            if max1==-1: max1=max_a
            if max2==-1: max2=max_a
                
            no_null=True # record if some elments are -1
            
            # compute the new position for each candidate
            new_pos=[-1,-1,-1,-1]

            ## if max_a is -1, it means that all elements in both files are -1, we don't need
            # to recompute, will just output all -1
            if max_a!=-1:
                for i in [2,4,6,8]:
                    logging.debug("     ... recomputing position for "+l1p[i-1])
                    logging.debug("          in file1 "+l1p[i])
                    logging.debug("          in file2 "+l2p[i])
                    
                    if l1p[i]=="-1" or l2p[i]=="-1": no_null=False  # some -1
                    
                    # warning, we have a problem with "-1". This should be in fact the last position... (and is seems rather the first)                    
                    # max is the higher value, we will substitute -1 by max+50   
                    ## but if both are -1, hust leave them as they are  
    ##                if l1p[i]=="-1" and l2p[i]!="-1": l1p[i]=max1+int(options.num)
    ##                elif l2p[i]=="-1" and l1p[i]!="-1": l2p[i]=max2+int(options.num)                
                    ## CHANGED A BIT: due to problems when doing the relatives. Now all -1 
                    # are substituted, even if they are -1 in both files
                    if l1p[i]=="-1": l1p[i]=max_a+int(options.num)
                    if l2p[i]=="-1": l2p[i]=max_a+int(options.num)
                    
                    logging.debug("                      in file1 "+str(l1p[i]))
                    logging.debug("                      in file2 "+str(l2p[i]))
                    
                    ## if options.pos=relative, recompute the values of positions to be so
                    if options.pos=="relative":
                        l1p[i]=float(l1p[i])/float(max1)
                        l2p[i]=float(l2p[i])/float(max2)
                        logging.debug("             relative in file1 "+str(l1p[i]))
                        logging.debug("             relative in file2 "+str(l2p[i]))

                        # note that the last position will have value 1 (max) and the -1
                        # positions will have (max+num)/max, higher than 1
                    else:
                        l1p[i]=int(l1p[i])
                        l2p[i]=int(l2p[i])
                    
                    # compute the new position
                    if options.vote=="best":
                        new_pos[i/2-1]=min(l1p[i],l2p[i])
                        logging.debug("            new postion "+str(new_pos[i/2-1]))
                    elif options.vote=="average":
                        
                        logging.debug("              new lp1 i 2 "+str(l1p[i])+"  "+str(l1p[i]))
                        
                        ## compute average
                        new_pos[i/2-1]=(float(l1p[i])+float(l2p[i]))/2.0
                        logging.debug("            new postion "+str(new_pos[i/2-1]))
                        
                        
            # all four are computed, output them
            # if strict option is given, skip instances with -1 in any of the files (no_null=True)
            if options.inst!="strict" or no_null:
                ## output the same line with the new postions
                sys.stdout.write(l1p[0]+"\t"+l1p[1]+"\t"+str(new_pos[0])+"\t"+l1p[3]+"\t"+str(new_pos[1])+"\t"+l1p[5]+"\t"+str(new_pos[2])+"\t"+l1p[7]+"\t"+str(new_pos[3])+"\t"+t_not_found+"\n")
 

     

