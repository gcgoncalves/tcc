#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compute_average_per_freqs.py: given the output csv file with the rel overlap
#  and tau for each verb, returns the mean an variance of all fields grouping them
#  by the frequency of verbs. The verb frequency is given in a separate file
#  and the ranges of frequency to group as a parameter (have default values)
# 
# ./compute_average_per_freqs.py file.csv verb_freq.csv k  (k is a label just for the output)
###########################################################

import logging
log = logging.getLogger(" MAIN(compute_average.py)  --\t")

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

import math

#from contextlib import closing

#import bottleneck as bn
import scipy.stats as ss
from numpy import array, vstack


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    parser.add_option("-f", "--flist", dest="flist", default="100,200,500,1000,10000", help="List of ordered frequency values. The sets will be from 1 to value1, from value1 to value2, and bigger than last value")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 3:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    ## get freq_list from options 
    #  the list is given as string "10,50,500", convert it to a python list
    freq_list=[]
    for f in options.flist.split(","):
        freq_list.append(int(f))
    
    # ------------ ###
    log.info(" ======== compute_average_per_verbs.py ========")
    log.info("     file: "+args[0])
    log.info("     verb freq: "+args[1])
    
    log.debug("           list of freq thesholds that will be studied: "+str(freq_list))
    
    ##  read frequency list and store it in dictionary
    verb_freq={}
    with open(args[1],'r') as freqf:
        for line in freqf:
            line=line.strip()
            if line=="": break
            lp=line.split()
            verb_freq[lp[0]]=int(lp[1])
            #log.debug("   adding verb freq: "+lp[0]+" "+str(verb_freq[lp[0]]))
        log.debug("    verb freqs added")
    
    ## read the csv file and compute average and stdev for selected fields
    ##  but separating them for frequency ranks
           
    # fields in file:
    #  ##verb	all_syn1	all_syn2	cut_syn1	cut_syn2	intersect	union	rel_ov	rel_ov_int	rel_ov_uni	taub_int	pvalue_int	taub_uni	pvalue_uni

    # fields for which we want the average:	cut_syn1 ($4)	cut_syn2 ($5)	intersect ($6)	union ($7)	rel_ov ($8)	rel_ov_int ($9)	rel_ov_uni ($10)	taub_int ($11)	taub_uni ($13)
    
    # we will also compute jaccard (intersection/union) and we will change the order of the fields a bit
    
    # read the file and stors the desired fields in a numpy array
    #  we will create an array for each freq range, storing them in a dictionary
    all_arrays={}
    # we will also count the instances in each freq range
    N={}

    log.debug("   going to read file "+args[0])

    with open(args[0],'r') as ifile:
        for line in ifile:
            line=line.strip()
            if line=="": break
            
            log.debug("   reading line: \n         "+line)
            
            # split fields:
            linep=line.split()
            if linep[0].find("##")!=0: # not a comment line
                
                # compute jaccard (interserction/union) float(linep[5]/ float(linep[6])
                jac=float(linep[5])/float(linep[6])                
                
                # add desired fields in the  desired output order (easier for output)
                #  (all_syn1)  (all_syn2)  (cut_syn1)  (cut_syn2)  overlap  union  jaccard  im  taub  im_overlap  taub_overlap  (im_uni)
                new_a=array([float(linep[3]), float(linep[4]), float(linep[5]), float(linep[6]), jac, float(linep[7]), float(linep[12]), float(linep[8]), float(linep[10]), float(linep[9])])
                log.debug("      adding array\n        "+str(new_a))
                
                ## see to which freq range the verb belongs
                freq=verb_freq[linep[0]] # get freq for the verb
                log.debug("    freq for verb "+linep[0]+" "+str(freq))
                
                bigger=True # the freq is bigger than the threhsold, we keep looking                
                for f in freq_list: # the list is ordered: e.g 100, 1000, 10000. 
                    if freq<=f: # we found the closest theshold, add the array (note that this only works if the list is sorted)
                        bigger=False # found!!
                        if f in all_arrays: # the array is already started, add the new line
                            all_arrays[f]=vstack((all_arrays[f],new_a))
                            # add 1 to the counts
                            N[f]+=1
                        else: # first instance with this freq, start the array
                            all_arrays[f]=new_a
                            # start also the counts
                            N[f]=1
                            
                        break
                    
                # if we ended the for and bigger is still True, add the element to the "bigger" range, meaning that 
                #  the frequency is bigger than the biggest threshold
                if bigger:
                    f="bigger"
                    if f in all_arrays: # the array is already started, add the new line
                        all_arrays[f]=vstack((all_arrays[f],new_a))
                        # add 1 to the counts
                        N[f]+=1
                    else: # first instance with this freq, start the array
                        all_arrays[f]=new_a
                        # start also the counts
                        N[f]=1
                
                # log
                log.debug("   added to freq: "+str(f))
                log.debug(" \n  --- new array --- \n"+str(all_arrays[f]))
                    
                # add one to the total counts
                    
                
    ## file read
    
    # once the arrays with all elemetns have been created, compute mean and variance
    # and output it
    ##        stdev=all_a.std(axis=0)
    ##        mean=all_a.mean(axis=0)
    
    # add "bigger" to the list of freqs to be output
    freq_list.append("bigger")
    
    for f in freq_list:  # compute stuff per freq range
        
        stdev=ss.nanstd(all_arrays[f])
        mean=ss.nanmean(all_arrays[f])

        for i in all_arrays[f]:
            log.debug("         "+str(i))

        # output all results in order, mean, st  and stdev/N for each element
        # (N is the number of verbs)
        # add the freq range too

        sys.stdout.write(args[2]+"\t"+str(f)+"\t"+str(N[f])+"\t")
        for i in range(0,len(stdev)):
            sys.stdout.write(str(mean[i])+"\t"+str(stdev[i])+"\t"+str(stdev[i]/math.sqrt(N[f]))+"\t")
            
        sys.stdout.write("\n")
