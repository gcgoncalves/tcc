
for f in `ls results_*.csv`; do

	echo "" 
	echo "file $f has average number of synonims:"
	base_name=`echo $f | awk '{gsub(".csv","",$1); print $1}'`

	cat $f | awk '{if(lema!=$1) {print lema , counts; lema=$1; counts=0} if ($5>0) counts+=1}END{print lema , counts}' > $base_name.syn_per_verb

	cat $base_name.syn_per_verb | awk 'NF==2' | awk '{tot+=$2}END{print tot/NR}' 


done


