#! /usr/bin/python3
# -*- coding: UTF-8 -*-

### functions to compute the overlap of two sequences ####

__author__="muntsa"
__date__ ="$06/05/2015$"

import logging

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


##################
# check_overlap: checks the overlap of the two sequences for N from 1 to
#  L, this is, augmenting the window of elements we take into account
def check_overlap(s1,s2):
        
    overlap=[]
    
    for i in range (1,len(s1)):
        
        logging.debug("     sequence1 of N="+str(i)+"   "+str(s1[0:i]))
        logging.debug("     sequence2 of N="+str(i)+"   "+str(s2[0:i]))

        logging.debug("        overlaping list: "+str(intersect(s1[0:i], s2[0:i])))
        logging.debug("          overlaping size: "+str(intersect_size(s1[0:i], s2[0:i])))
        
        overlap.append(intersect_size(s1[0:i], s2[0:i]))

    logging.debug("   whole overlap list:  "+str(overlap)+"\n")

    return overlap


#################
# rel_overlap(overlap): given the overlap array,
#  compute the relative_overlap, which is a similar
#  array but with each elemnt divided by the index of the 
#  array. This measures how many elements are comon with 
#  respect to those that could be common. If the sequences
#  are identical, all rel_overlap elements will be 1, if it is 
#  random, it will start in 0 and end in 1
def rel_overlap(overlap):
    
    rel_overlap=[]
    for i in range (0,len(overlap)):
        rel_overlap.append(float(overlap[i])/float(i+1))

    return rel_overlap

#################
# rel_overlap_score(overlap): given the overlap array,
#  compute a global measure that gives the relative_overlap
#  computing the mean of the re_overlap array
def rel_overlap_score(overlap):
    
    return array_average(rel_overlap(overlap))

#################
# array_average: compute the average of the elements in a array
def array_average(a):
    tot=0.0
    for x in a:
        tot+=float(x)
    
    if len(a)==0:
        return 0
    else:
        return tot/float(len(a))

#################
# intersect: compute the overlaping elements of two lists
def intersect(a, b):
    return list(set(a) & set(b))

#################
# intersection_size: compute the size of overlaping elements of two lists
def intersect_size(a, b):
    return len(set(a) & set(b))



