#! /usr/bin/python3
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compare_thesaurus.py: compare two thesaurus with different metrics 
#  (overlap, tau, etc)
# 
# usage:
#  compare_thesaurus.py thesaurus1 thesaurus2 -s name -z -v -p number --log level
###########################################################

import logging
from optparse import OptionParser
from copy import deepcopy
#ifrom itertools import izip
import codecs
import sys
from decimal import Decimal

# import file with overla pcomputing functions
from check_overlap import *
# and with tau computing
from compare_distances import *

## needed for one option of kendall tau, but I have problems with isnalling scipy
# import scipy.stats as ss

### I tried to use this fastkendalltau ( http://projects.scipy.org/scipy/ticket/999), 
# which is in theory faster, but it needs scipy to be
#  installed, and I had some problems, to be checked in the future
#from fastkendalltau import *

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    ##--- get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    parser.add_option("-s", "--suffix", dest="suf", default="_", help="Suffix that will be added to the output files to label them")
    parser.add_option("-p", "--prec", dest="prec", default="4", help="numbr of decimals to be taken into account for similarity measures (i.e. 0.112 and 0.113 are the same? then use p=2)")
    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",default=False, help="Output more information in proper files")
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
        
        
    # options.prec tells us how many decimals we will take into account
    #  if it says two, our precision is 0.01, if it says 3, 0.001, an so on
    pr = Decimal(10) ** (-1*int(options.prec))
    zero=Decimal('0.0').quantize(pr) # set the value of 0.0, for comparison
    # we will use Decimal(number).quantize(pr) and that will create a decimal number with the desired decimals
    
    # get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ----
    logging.info(" ======== compare_thesaurus.py ========")
    #logging.info( " reading file:"+ args[0])

    ##############
    # We need to read the two files, which contain the same thesaurus with
    #  similarities computed with different metrics, and compare the metrics
    #
    # The file has, for each verb, the similarities with respect to all other
    #  verbs. To avoid memory problems, we will read one verb at a time (and 
    #  all its similarities) and make the comparison, then proceed to the 
    #  next verb.
    #
    # At the samle time we read the file, we will build an index with all 
    #  the verbs that will allow us to relate id and verb. Not sure if
    #  we need it, though...
    # Alternativelly, we could read an independent file containing the 
    #  verb and id pairs first of all and use the sixe of it to create
    #  arrays and so directly with the desired size. Would it be better???    
    ##############

    ## initialize variables
    all_verbs=[]
    lemma=""
    # we will create a "vector" containing all similarities of a given verb
    #  for the measure in each file. The vector is implemented as a
    #  dictionary, key is index, content is similarity. I do it as a dictionary
    #  because latter we will need to sort it by value and keep the index
    v1={} # dict for file 1
    v2={} # dict for file 2
    no_null=0 ## to count how many verbs have no null similarity
    
    # all_overlaps will contain the overlap info for all verbs (cumulative)
    all_overlaps=[]

    ### open output files
    # write to file the relative overlap per verb
    f_rel_overlap=open(options.suf+".rel_overlap.csv",'w')
    
    # write tau per verb
    f_tau=open(options.suf+".tau.csv",'w')
    
    # to output  verbose stuff
    fsorted_sim=open(options.suf+".sorted_sim.out",'w')

    ## open both files to read them at the same time
    with codecs.open(args[0],'r', encoding='utf8') as file1, \
      codecs.open(args[1],'r', encoding='utf8') as file2: 
     # with open(args[0],'r') as file1, open(args[1],'r') as file2: 
        for l1, l2 in zip(file1, file2):
            l1p = l1.strip().split()
            l2p = l2.strip().split()
            
##            logging.debug("  reading lines: \n \t"+l1+"\t"+l2)
##            
##            logging.debug("    current lemma: "+l1p[0])
##            logging.debug("    stored lemma: "+lemma)

            ## the thesaurus are expected to have the same lemas, in the same
            #   order and with same ids. Report it that does not happen
            if l1p[0] != l2p[0] or l1p[1] != l2p[1] or l1p[2] != l2p[2] or l1p[3] != l2p[3]:
                logging.error("   Missmatch in the lemmas or ids in the two files!")
                logging.error("      line in file1: "+l1)
                logging.error("      line in file2: "+l2)
                exit(1)
                        
            # if the l1p[0] is different than lemma it means that
            #  we reached a new lemma, so we finished to read similarities
            #  for the current verb. Now we can compare the vector we got
            #  for each file
            if l1p[0] != lemma:
                   
                if lemma!="": # if lemma is empty, this means that we are just starting to read the file, no need to compare
                #if len(v1)!=0: 
                    logging.debug("")
                    logging.debug("    finished reading similarities for this lemma (" + lemma +")")
                   
                    # check overlap of both vectors (in terms of number of elements taken into account)
                    overlap=check_overlap(sorted(v1, key=v1.__getitem__, reverse=True),sorted(v2, key=v2.__getitem__, reverse=True)) # the function compares the two ordered lists of similarity
                    
                    # add this overlap to all_overlaps
                    #--> WARNING: so far, it just makes sense if we counted all similarities (0), 
                    # otherwise, the length of the arrays varies and the total overlap makes no sense (needs to
                    # be designed how to do that)
                    if options.count_zeros:
                        if len(all_overlaps)==0: # first verb, initialize all_overlaps list
                            all_overlaps=deepcopy(overlap)
                        else:
                            for i in range(0,len(overlap)):
                                all_overlaps[i]+=overlap[i]
                            
                    #compute also the "relative overlap", which gives us a single number for each verb.
                    # Then, we can compute the mean for all verbs and get a measure of global overlap...
                    rel_overlap=rel_overlap_score(overlap)
                    f_rel_overlap.write(lemma+"\t"+str(rel_overlap)+"\t"+str(no_null)+"\n")
                    
                    # compare also similarities with tau
                    tau=compare_similarities(v1,v2, lemma, fsorted_sim, options.verbose)
                    f_tau.write(lemma+"\t"+str(tau[0][2])+"\t"+str(no_null)+"\n")
                    
                    # re-start lists
                    v1={}
                    v2={}
                    no_null=0
                    
                lemma=l1p[0] # store new lemma

            
##                # store new lemma (that may not be necessary if we do not need 
##                #  the vector index) --> a little stupid in that way, better to read the
##                # vector index!!
##                lemma=l1p[0]
##                all_verbs.append(lemma)
##                logging.debug("   starting new lemma " + lemma)

            ##
            # add current line info from each file to the corresponding list
            # we just need to append the current similarity (order is the same in
            #  both files, so it will be consistent)
            
            # if --zero option is given, we store all similarities, if not
            #  we store just non-zero similarities (to keep the size equal
            #  we ignore only the cases were both similarities are 0)
            
            # store similarities as Decimal numbers, with the desired precision
            s1=Decimal(l1p[4]).quantize(pr)
            s2=Decimal(l2p[4]).quantize(pr)
            if options.count_zeros or s1!=zero or s2!=zero:
                v1[int(l1p[3])]=s1
                v2[int(l2p[3])]=s2
                
            # in any case, count how many non-zero similarities we have
            if s1!=zero or s2!=zero:
                no_null+=1
                
    ## comparisions for last verb
    # check overlap 
    overlap=check_overlap(sorted(v1, key=v1.__getitem__),sorted(v2, key=v2.__getitem__)) # the function compares the two ordered lists of similarity
    if len(all_overlaps)==0: # first verb, initialize all_overlaps list
        all_overlaps=deepcopy(overlap)
    else:
        for i in range(0,len(overlap)):
            all_overlaps[i]+=overlap[i]

    #compute also the "relative overlap", which gives us a single number for each verb.
    # Then, we can compute the mean for all verbs and get a measure of global overlap...
    rel_overlap=rel_overlap_score(overlap)
    f_rel_overlap.write(lemma+"\t"+str(rel_overlap)+"\t"+str(no_null)+"\n")  
    
    # compare also similarities with tau
    tau=compare_similarities(v1,v2, lemma, fsorted_sim, options.verbose)
    f_tau.write(lemma+"\t"+str(tau[0][2])+"\t"+str(no_null)+"\n")        

    # print average overlap --> WARNING: so far, it just makes sense if we counted all similarities (0), 
    # otherwise, the length of the arrays varies and the total overlap makes no sense (needs to
    # be designed how to do that)
    if options.count_zeros:
        M=Decimal(len(all_overlaps))
        for i in range(0,len(all_overlaps)):
            sys.stdout.write(str(i+1)+"\t"+str(Decimal(all_overlaps[i])/M)+"\t"+str(no_null)+"\n")

    f_rel_overlap.close()

