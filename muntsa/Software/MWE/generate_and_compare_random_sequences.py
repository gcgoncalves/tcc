#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# Generate_and_compare_random_sequences.py: generates two random sequences
#  of length L (given as a parameter) and computes the overlaping between them
#  for sub-sequences of length N, with N from 1 to L.
# The process will be reapeated M times (parameter also) and the mean of all 
#  iterations will be returned.
# 
# usage:
#  generate_and_compare_random_sequences.py L M
###########################################################

import logging
from optparse import OptionParser
#from copy import deepcopy
import codecs
import random

import overlap
import scipy.stats as ss

#from set import set

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# import file with overlapcomputing functions
import overlap


######################
# generate_random_seq: generates a random sequence of integers
#  from 1 to L, of length L
def generate_random_seq(L):
    
    s=[]
    for i in range(1,L+1): 
        s.append(i)

    random.shuffle(s)
    
    logging.debug("  created random list:  "+str(s))
    return s

######################
# generate_part_random_seq: generates sequence of integers
#  from 1 to L, of length L, with n elements  ordered and 
#   L-n ordered with random sequence. If begin=True, the ordered
#  elements are at the beginning, else, they are at the end
def generate_part_random_seq(n,L, begin=True):
    
    so=[] # ordered
    sr=generate_random_seq(L-n) # random
    
    for i in range(1,n): 
        so.append(i)

    # concatenate so and sr. The order depends on begin value
    if begin:
        so.extend(sr)
        logging.debug("  created part random list (ordered at the beginning):  "+str(so))
        return so
    
    else:
        sr.extend(so)
        logging.debug("  created part random list (ordered at the end):  "+str(sr))
        return sr
    

######################
# generate two random sequences, compare them and add results to file
def add_random_seq(file,l,label):
    
    # generate two random sequences
    s1=generate_random_seq(l)
    s2=generate_random_seq(l)
    
    # compare them with rel_overlap and tau

    # overlap list
    overl=overlap.check_overlap(s1, s2) 
    #compute also the "relative overlap", which gives us a single number 
    rel_overlap=overlap.rel_overlap_score(overl)
    
    # tau
    taub, pvalue=ss.kendalltau(s1,s2,initial_lexsort=True)

    file.write(label+"\t"+str(l)+"\t"+str(rel_overlap)+"\t"+str(taub)+"\t"+str(pvalue)+"\t0\n")

######################
# generate two sequences ordered at the beginning (length n), 
# compare them and add results to file
def add_part_ordered_seq(file,l,n,label,begin=True):
        
    # generate two sequences ordered at the beginning or end according to "begin"
    s1=generate_part_random_seq(n,l,begin)
    s2=generate_part_random_seq(n,l,begin)
    
    # compare them with rel_overlap and tau

    # overlap list
    overl=overlap.check_overlap(s1, s2) 
    #compute also the "relative overlap", which gives us a single number 
    rel_overlap=overlap.rel_overlap_score(overl)
    
    # tau
    taub, pvalue=ss.kendalltau(s1,s2,initial_lexsort=True)

    file.write(label+"\t"+str(l)+"\t"+str(rel_overlap)+"\t"+str(taub)+"\t"+str(pvalue)+"\t"+str(n)+"\n") # add also info about how many elements were equally sorted


#########################
# main: we will create simulated thesaurus with different  characteristics
#  the parameters, L and M are the maximum number of elements in a sequence
# and M the number of repetitions (i.e. generated verbs)
if __name__ == "__main__":

    #logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre
    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre


    ##--- get parameters ---
    usage = "usage: %prog length repetitions\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    ###parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
    ###parser.add_option("-s", "--silent", action="store_false", dest="verbose")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    L=int(args[0])
    M=int(args[1])
    
    # we will create three kind of sequences and compare them (among same kinds)
    #  one sequence for random sequences,
    #  one with sequences ordered at the beginning (with diferent sizes of ordered
    #  elements) and one wirh sequences ordered at the end. 
    # In all cases, each "verb" will contain N elements witn N between L/2 and L
    # M states the number of verbs in the thesaurus
    
    #open output files: we will output rel_:overlap and tau for the generated sequences
    with open("random_sequences.csv",'w') as rf, open("first_ordered_sequences.csv",'w') as ff, \
      open("last_ordered_sequences.csv",'w') as lf: 
    
        # M iterations: M simulated verbs
        for i in range(0,M):
            
            logging.debug("-----")
            logging.debug("starting iteration "+str(i))
            
            # label for this instance:
            label="s_"+str(i)            
            
            # set the l for this instance, i.e. length of the sequence, between L/2 and L
            l=random.randint(L/2,L) #Inclusive
            
            # set the length of the partially ordered between l/4 and 3l/4
            #n=random.randint(l/4,3*l/4)
            n=random.randint(3*l/4,l)
            
            # generate two random sequences, compare them and add results to file
            add_random_seq(rf,l,label)
            
            # ordered at the beginning
            add_part_ordered_seq(ff,l,n,label,begin=True)
            
            # ordered at the end
            add_part_ordered_seq(lf,l,n,label,begin=False)
        


#######   old:  firts cimpuatations of overlap for random sequences ##############
### main
##
##if __name__ == "__main__":
##
##    #logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre
##    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre
##
##
##    ##--- get parameters ---
##    usage = "usage: %prog length repetitions\n"
##    parser = OptionParser(usage=usage, version="%prog 1.0")
##
##    ###parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
##    ###parser.add_option("-s", "--silent", action="store_false", dest="verbose")
##
##    (options, args) = parser.parse_args()
##
##    if len(args) != 2:
##        parser.error("incorrect number of arguments")
##        exit (1)
##    
##    L=int(args[0])
##    M=int(args[1])
##    
##    # each iteration will give an array containing the overlaps from N=1 to N=L
##    #  we will store the summ of those arrays in order to give back the
##    #  average at the end
##    all_overlaps=[0] * L # initialize list of length L
##    
##    for i in range(0,M):
##        
##        logging.debug("-----")
##        logging.debug("starting iteration "+str(i))
##        
##        # create two random sequences of length L (with elements from 1 to L)
##        seq1=generate_random_seq(L)
##        seq2=generate_random_seq(L)
##        
##        overlap=check_overlap(seq1,seq2)
##        
##        for i in range(0,L):
##            all_overlaps[i]+=overlap[i]
##        
##
##    # output: print a table of N and average overlap 
##    for i in range(0,L):
##        print str(i+1)+"\t"+str(float(all_overlaps[i]/float(M)))
##    
##
