#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# read_shelf_thes.py: reads shelf file that contains a thesaurus and 
#   outputs it (just as example/test case)
# 
# usage:
#  read_shelf_thes.py shelf_file --log level
###########################################################

import logging
log = logging.getLogger(" thesaurus2shelf.py)  --\t")

from optparse import OptionParser

#ifrom itertools import izip
import codecs
import sys

from contextlib import closing
import shelve

#####################
# main: just an example to read the shelve file and output it
## 

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # suffix to label the output files
    #parser.add_option("-o", "--output", dest="out_file", default="", help="Suffix that will be added to the output files to label them")
##    # options.prec tells us how many decimals we will take into account. 
##    #  if it says two, our precision is 0.01, if it says 3, 0.001, an so on
##    parser.add_option("-p", "--prec", dest="prec", default="4", help="numbr of decimals to be taken into account for similarity measures (i.e. 0.112 and 0.113 are the same? then use p=2)")
##    # wether we take zeros into account in the synonims vector
##    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 

    ## mandatory argument is input file which contains the shelf object

    log.info(" ======== read_sheld_thes.py ========")

    ## open input file and output it (just ot check, now)

    with closing(shelve.open(args[0], 'c')) as shelf: # open input file
        for l in shelf:
            print l, shelf[l]
    

