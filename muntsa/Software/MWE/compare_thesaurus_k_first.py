#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compare_thesaurus_k_first.py: compare two thesaurus with different metrics (overlap, tau, etc) 
#  taking only  the first k neighbours, and considering as possible neighbours only those
#  verbs given in an input file. If the file is not given, all common vebrs will be considered
#
# we give as parameter a list of k values, instead of just one, in order to
#  do the computations for various k in one run. We will give it as a python list e.g. [50,100,200, -1]
#  (we set as default these three k values). k=-1 takes into account all non-zero elements of the list

# Important: the thesaurus must be stored as shelve files. Use thesaurus2shelf.py to 
#  convert csv thesaurus to shelf
# 
# usage:
#  compare_thesaurus_k_first.py thesaurus1.shelf thesaurus2.shelf -k k_list
#                               -n neighbour_verbs_file  -s name  -v -p number --log level
###########################################################

import logging
log = logging.getLogger(" MAIN(compare_thesaurus.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

# import classes for storing synonim lists and comapring them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # list of k values to be taken into account
    parser.add_option("-k", "--klist", dest="klist", default="50,100,200,-1", help="List of k values for which the statistic of the k-first elements will be computed.")
    # file containing the list of verbs that will be considered as posisble neighbours
    parser.add_option("-n", "--neighbours", dest="neigh", default="none", help="File containing the list of verbs that will be considered as posisble neighbours.")
    # suffix to label the output files
    parser.add_option("-s", "--suffix", dest="suf", default="_", help="Suffix that will be added to the output files to label them")
    # options.prec tells us how many decimals we will take into account. 
    #  if it says two, our precision is 0.01, if it says 3, 0.001, an so on
    parser.add_option("-p", "--prec", dest="prec", default="4", help="numbr of decimals to be taken into account for similarity measures (i.e. 0.112 and 0.113 are the same? then use p=2)")
    
    ##    # wether we take zeros into account in the synonims vector --> at the moment I do not take it into account
    ##    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
    # verbose outputs files with sorted similarity
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",default=False, help="Output more information in proper files")
    # loggind
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compare_thesaurus.py ========")
    log.info("    thes1: "+args[0])
    log.info("    thes2: "+args[1])
    log.info(" ===============================")
    
    
    
    ## get k_list from options file and praapre the output files 
    #  the list is given as string "10,50,-1", convert it to a python list
    k_list=options.klist.split(",")
    log.debug("  list of k that will be studied: "+str(k_list))

    # each k will have its own oputput file, open them
    fout_list=[]
    for k in k_list:
        
        log.debug("    preparing output file for k "+k)
        
        f=open(options.suf+".k"+k+".measures.csv",'w')
        
        # output first line with column key (we will write to file all the measures: number of synonims,  relative overlap, tau-b)
        f.write("## measures for k="+k+"\n")
        f.write("##verb\tall_syn1\tall_syn2\tcut_syn1\tcut_syn2\tintersect\tunion\trel_ov\trel_ov_int\tcosine\tcosine_int\ttaub_int\tpvalue_int\ttaub_uni\tpvalue_uni\n")
        
        fout_list.append(f)

    # open the file for sorted sim (only if verbose option is given) 
    if options.verbose:  ## not sure wether it is working at the moment....!!!
        fsorted_sim=open(options.suf+".sorted_sim.out",'w')

    ##############
    # both thesaurus are already stored as shelves, so they can be
    #  directly accessed as a dictionary. We only need to open the files
    #  and access them as if it where dicts
    # We open the files readonly to avoid potential problems. We only read the thesaurus here, we do not modify them
    with closing(shelve.open(args[0], 'r')) as thes1, closing(shelve.open(args[1], 'r')) as thes2: 
        
        all_lemmas1=st.get_all_keys(thes1) # returns a SET of keys, so lemmas
        all_lemmas2=st.get_all_keys(thes2)
        
        #log.debug("   lemmas in first thesarurus:\n  "+ str(all_lemmas1))
        #log.debug("   lemmas in second thesarurus:\n  "+ str(all_lemmas2))
        
        ## we will study only entries for the common lemmas, so get the intersection
        # of both sets
        common_lemmas=all_lemmas1&all_lemmas2
        
        log.debug("   common lemmas in both thesarurus:\n  "+ str(common_lemmas))
        
        ## if options.neigh is given, we will consider as neighbours for the lemma
        #  only the verbs in this file. Add them to another set. If the option is not 
        #  given we will consider all common_lemmas
        ## Note that here we are converting the matrix in a rectangular matrix 
        # while it was originally square. The idea is that we study a set of lemmas
        # (the common ones at the moment) in terms of different neighbuors. This
        #  is useful to look for concrete neighbours, such as the frequent verbs,
        #  or the unfrequent, etc.
        # the format of the file is 
        #   verb1   freq1
        #   verb2   freq2
        #  (freqs may be there or not...)
        if options.neigh!="none":
            neigh_candidates=set()
            with open(options.neigh,'r') as nfile:
                for line in nfile:
                    line=line.strip()
                    if line=="": break
                    
                    neigh_candidates.add(line.split()[0])
        else: # if no file is given, we will stufy all common lemmas, so we are considering a square matrix
            neigh_candidates=common_lemmas
        
        ###
        # for each lemma in common lemmas, perform the comparison of both similarity vectors
        #  Warning: since the lemmas taken into account may be less than the stored, we will
        #  build two vectors with the elements we want to take into account using SynonimList class
        for lemma in common_lemmas:

            log.debug("------------------------------------------------------------------------")
            log.debug("   creating synonim vector for lemma: "+lemma)
            log.debug("------------------------------------------------------------------------")
                        
            # initialize v1 and v2, vectors of synonims for the given lemma
            log.debug("     ..... v1")
            v1=SynonimList(lemma) # synonims in thesaurus1
            log.debug("     ..... v2")
            v2=SynonimList(lemma) # synonims in thesaurus2
            
            # thes1[lemma] contains a dictionary of lemma/similarity, we need
            #  to add to v1 the similarities for lemmas in common_lemmas
            #  we will pass the whole dictionary and the set of lemmas
            #  to add_synonim_from_dict and it will do the job.
            log.debug("     adding synonim list")
            log.debug("       ..... v1 ")
            # we pass neigh_candidates to the function and it will add just as neighbors
            #  the verbs in the set that have similarity bigger than 0
            v1.add_synonim_from_dict(thes1[lemma],neigh_candidates)
            log.debug("       ..... v2")
            v2.add_synonim_from_dict(thes2[lemma],neigh_candidates)
            
            ## once we have both vectors created, with the same lemmas,
            #  we can compare the synonims we got for each file.
            log.debug("")
            log.debug("      synonim vectors build for lemma (" + lemma +")")
            
            
            ## compare similarities: create an insatnce of CompareSynLists class
            log.debug("         comparing similarities")
            compare=CompareSynLists(v1,v2, options.prec, common_lemmas) # create object that will take care of everything
            # note, this object will be the same for all k, in that way we do not need to store or sort the lists everytime
            
            ## now, for each k make the comparisons
            for i in range(0,len(k_list)):
                # we compare the synonims with k_list[i] and store the measures in fout_list[i]
                
                log.debug("---------------------............-----------------------")
                log.debug("      studying k first elements. k= "+k_list[i])
                log.debug("---------------------............-----------------------")
            
                ## compare_lists(k) return all the measures for the k first elements: 
                #      orginal lengths (1 and 2), cut lengths (1 and 2), intersection length, union length, rel_overlap, 
                #         rel_ovelap_intersection, rel_ovelap_union, taub_intersection, pvalue_taub_intersection, taub_union, pvalue_taub_union,
                len1, len2, cl1, cl2, inters, union, rel_overlap, rel_overlap_i, cosine, cosine_i, taub_i, pvalue_i, taub_u, pvalue_u = compare.compare_lists(int(k_list[i]))
                
                ## write the measures into the output file
                fout_list[i].write(lemma+"\t"+str(len1)+"\t"+ str(len2)+"\t"+ str(cl1)+"\t"+ str(cl2)+"\t"+ str(inters)+"\t"+ str(union)+"\t"+ str(rel_overlap)+"\t"+ str(rel_overlap_i)+"\t"+ str(cosine)+"\t"+ str(cosine_i)+"\t"+ str(taub_i)+"\t"+ str(pvalue_i)+"\t"+ str(taub_u)+"\t"+ str(pvalue_u)+"\n")
                       
            # if --verbose option is given, we will output also the sorted similarities in a file
            if options.verbose:                        
                fsorted_sim.write(compare.get_string_sorted_sim()+"\n")

    ## DONE! ##
    
    ##  close output files!!
    
    for f in fout_list:
        f.close()
    
    if options.verbose:
        fsorted_sim.close()



