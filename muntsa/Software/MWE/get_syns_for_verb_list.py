#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# get_syns_for_verb_list.py: given a  thesaurus and a list of verbs (in a file), create a file
#  for each verb with a sorted list of their neighbours 

# usage:
#   get_syns_for_verb_list.py thesaurus.shelf  verb_list  suffix --log level
###########################################################

import logging
log = logging.getLogger(" MAIN(compare_thesaurus.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

# import classes for storing synonim lists and comapring them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 3:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compare_thesaurus.py ========")
    log.info("    thes: "+args[0])
    log.info("    verb_list: "+args[1])
    log.info("    output file base name: "+args[2])
    log.info(" ===============================")
    
    ####
    # read the verb list, which are the verbs we will study
    #  one verb per line
    verb_list=[]
    with open(args[1], 'r') as vfile:
        for line in vfile:
            line=line.strip()
            if line=="": break
            
            log.debug("   reading line: "+line)
            
            verb_list.append(line)
            

    ##############
    # thesaurus is already stored as shelves, so they can be
    #  directly accessed as a dictionary. We only need to open the files
    #  and access them as if it where dicts
    # We open the files readonly to avoid potential problems. We only read the thesaurus here, we do not modify them
    #
    # open also TOEFL_file to read taget verbs
    with closing(shelve.open(args[0], 'r')) as thes: 
    
        for lemma in verb_list:
                
            if lemma in thes:  
                # open the output file for this verb  
                with open( args[2]+".neigh_for_"+lemma+".csv", 'w') as ofile:
                    # read the synonim list
                                
                    log.debug("------------------------------------------------------------------------")
                    log.debug("   creating synonim vector for lemma: "+lemma)
                    log.debug("------------------------------------------------------------------------")
                        
                    # initialize vector of synonims for the given lemma
                    vs=SynonimList(lemma) # synonims in thesaurus1
                     
                    # thes[lemma] contains a dictionary of lemma/similarity, we need
                    #  to add to vs the similarities for all lemmas 
                    #  we will pass the whole dictionary and the set of lemmas
                    #  to add_synonim_from_dict and it will do the job.
                    log.debug("     adding synonim list (all neighbours for this verb!)")
                    vs.add_all_synonims_from_dict(thes[lemma])
                    
                    # we sort the similarities
                    vs.sort_by_sim()
                       
                    # now we have vs with sorted sims, output allneighbors and their position 
                    ofile.write("#getting neighbors for "+lemma+"\n")
                    ofile.write("#neigh\tposition\tsimilarity\n")
                    pos=1
                    for v in vs.synonims: # contains pairs of (lemma, similarity), output them
                        ofile.write(v[0]+"\t"+str(pos)+"\t"+str(v[1])+"\n")
                        pos+=1
                
