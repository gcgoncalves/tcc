#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# thesaurus2shelf.py: reads thesaurus csv file and stores it as a
#  shelf dictionary. Creates a shelf file to be read aftewards
# 
# usage:
#  thesaurus2shelf.py thesaurus_file.csv -o  shelf_name  --log level
###########################################################

import logging
log = logging.getLogger(" thesaurus2shelf.py  --\t")

from optparse import OptionParser
import os

#ifrom itertools import izip
import codecs
import sys

from contextlib import closing
import shelve

import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus
 

#####################
# main
## 

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # suffix to label the output files
    parser.add_option("-o", "--output", dest="out_file", default="", help="Suffix that will be added to the output files to label them")
##    # options.prec tells us how many decimals we will take into account. 
##    #  if it says two, our precision is 0.01, if it says 3, 0.001, an so on
##    parser.add_option("-p", "--prec", dest="prec", default="4", help="numbr of decimals to be taken into account for similarity measures (i.e. 0.112 and 0.113 are the same? then use p=2)")
##    # wether we take zeros into account in the synonims vector
##    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 

    # prepare files to read/write
    ## mandatory argument is input file, if no output name is given, use the same name
    #  as the input file adding .shelf
    if options.out_file=="":
        output_file=args[0]+".shelf"
    else:
        output_file=options.out_file+".shelf"

    log.info(" ======== thesaurus2shelf.py ========")

    ## read input file and store it in shelf file ###
    #
    # The input file has, for each verb, the similarities with respect to all other
    #  verbs. 
    # file format:
    #    abandon     1     abandon     1     0.0000000000
    #    abandon     1     abate     2     0.0164906340
    #    abandon     1     abbreviate     3     0.0000324093
    #    ...
    #    abort    11      abandon         1       0.0516603557
    #    abort    11      abate   2       0.0000000000
    #    abort    11      abbreviate      3       0.0000000000
    #    ...
    # we will store it in a dict of dicts: sim[lemma1][lemma2] contains
    #  the similarity between these two lemmas
    #### Note: the dictionary is simetric, if speed problems
    # arise, we can store just the half of it (and even not store the zeros!)

    ## careful! If a .shelf file already exists with the same name, that module
    #  may change its contents but won¡t delete everything. I recomend to
    #  either remove the existing file or to create a different one.
    if os.path.exists(output_file):
        log.error("  The output file already exists!! It is better that you remove it or use another name!")
        exit(1)
        
    with closing(shelve.open(output_file, 'c',writeback=True)) as shelf: # open output file
        with open(args[0],'r') as ifile: #open input file
            for line in ifile:
                line=line.strip()
                if line=="": break
                
                log.debug("   reading line: "+line)
                
               # add current line info from to the shelf dictionary                       
                st.add_line2dict(shelf,line)
    

