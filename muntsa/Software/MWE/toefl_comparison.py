#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# toefl_comparison.py:

# usage:
#  toefl_comparison.py thesaurus.shelf toefl-ref  --log level
###########################################################

import logging
log = logging.getLogger(" MAIN(compare_thesaurus.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

# import classes for storing synonim lists and comapring them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compare_thesaurus.py ========")
    log.info("    thes: "+args[0])
    log.info("    toefl file: "+args[1])
    log.info(" ===============================")
    
    lemma=""
    ##############
    # thesaurus is already stored as shelves, so they can be
    #  directly accessed as a dictionary. We only need to open the files
    #  and access them as if it where dicts
    # We open the files readonly to avoid potential problems. We only read the thesaurus here, we do not modify them
    #
    # open also TOEFL_file to read taget verbs
    with closing(shelve.open(args[0], 'r')) as thes, open(args[1], 'r') as toefl: 
       
        # read each line in toefl and look for neighbours of that verb in shelve.
        #  format:
        #    people inhabit lower near spill
        #    people dwell blush blanket conclude
        #    time clock install thwart cheat
        #    bank swear shrink pick lamb
        for line in toefl:
            line=line.strip()
            if line=="": break
            
            log.debug("   reading line: "+line)
            
            linep=line.split() # separate line fields
            
            # if it is a new lemma (1rst field), get the neighbours for the taget lemma --> create synonim_list
            if lemma!=linep[0]:
                    
                lemma=linep[0]
                
                # if the lemma is not in thesaurus, we can't do anyting, will ouptut 
                #  that line with all -1 just to keep the same number of instances in all elements
                if lemma in thes:                    
                    log.debug("------------------------------------------------------------------------")
                    log.debug("   creating synonim vector for lemma: "+lemma)
                    log.debug("------------------------------------------------------------------------")
                        
                    # initialize vector of synonims for the given lemma
                    vs=SynonimList(lemma) # synonims in thesaurus1
                     
                    # thes[lemma] contains a dictionary of lemma/similarity, we need
                    #  to add to vs the similarities for all lemmas 
                    #  we will pass the whole dictionary and the set of lemmas
                    #  to add_synonim_from_dict and it will do the job.
                    log.debug("     adding synonim list (all neighbours for this verb!)")
                    vs.add_all_synonims_from_dict(thes[lemma])
                    
                    # we need to sort the similarities
                    vs.sort_by_sim()
                    
                
            # now we have vs with sorted sims, get the position for the four remaining fields
            #  and output them together with the lemmas
            # (only if the lemma is in thesaurus) 
            if lemma in thes: 
                sys.stdout.write(lemma+"\t"+linep[1]+"\t"+str(vs.get_lemma_position(linep[1]))+"\t"+linep[2]+"\t"+str(vs.get_lemma_position(linep[2]))+"\t"+linep[3]+"\t"+str(vs.get_lemma_position(linep[3]))+"\t"+linep[4]+"\t"+str(vs.get_lemma_position(linep[4]))+"\n")
            
            # if not, just ouput all -1, and a last field meaning that hte lemma was not found (different than the noeghbors bot found!)
            else:
                sys.stdout.write(lemma+"\t"+linep[1]+"\t-1\t"+linep[2]+"\t-1\t"+linep[3]+"\t-1\t"+linep[4]+"\t-1\ttarget_not_found\n")
 
