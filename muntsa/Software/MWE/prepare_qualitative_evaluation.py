#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# prepare_qualitative_evaluation.py: output info of two thesaurus to be qualitativley studied
#
#   parameters: thesaurus1 thesaurus2 freq_list [-l list_of_lemmas]
#
#    freq_list: list of verb frequencies to be output together with similarities
#    -v list_of_lemmas:  only the lemmas in this file will be output
#
# Important: the thesaurus must be stored as shelve files. Use thesaurus2shelf.py to 
#  convert csv thesaurus to shelf
# 
# usage:
#  prepare_qualitative_evaluation.py thesaurus1.shelf thesaurus2.shelf  freq_list  -l verb_list --log level
###########################################################

import logging
log = logging.getLogger(" MAIN(compare_thesaurus.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

# import classes for storing synonim lists and comapring them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')

##############
# print
# if unique=True outputs only neighbours that
#  are in v but not in the reference
def print_info(v,vref,fout,lemma,unique=False):
    fout.write("\n--------\t----\t----\t----\n")
    fout.write(lemma+"\tneighbours:\t"+str(v.len())+ "\t"+str(freq[lemma])+"\n\n")
    for neig, sim in v.synonims:
        both=""
        if neig not in vref.get_lemma_list(): both="*" ## just a mark for the lemmas that are only in one neighbour list
        # print similarity and frequency if sim>0
        if float(sim)>0.0:
            if not unique or both!="": # if unique=True outputs only neighbours that are in v but not in the reference
                fout.write(both+"\t"+neig+"\t"+str(sim)+ "\t"+str(freq[neig])+"\n")
 

### assumim que vés el que té més veins!
def print_together(v,vref,fout,lemma):
    fout.write("\n--------\t----\t----\t----\t----\t----\n")
    fout.write(lemma+"\tneighbours:\t"+str(v.len())+ "\t"+str(freq[lemma])+"\n\n")
    i=1
    for neig, sim in v.synonims:
        both=""
        if neig not in vref.get_lemma_list(): both="*" ## just a mark for the lemmas that are only in one neighbour list
        # print similarity and frequency if sim>0
        if float(sim)>0.0:
            if both=="": # find in both, output info from both
                fout.write(both+"\t"+neig+"\t"+str(freq[neig])+"\t"+str(sim)+"\t"+str(i)+"\t"+str(vref.get_sim_for_lemma(neig))+"\t"+str(vref.get_lemma_position(neig))+"\n")                   
            else:
                fout.write(both+"\t"+neig+"\t"+str(freq[neig])+"\t"+str(sim)+"\t"+str(i)+"\t--\t--\n")
            i=i+1


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # list of k values to be taken into account
    parser.add_option("-v", "--vlist", dest="vlist", default="", help="List of verbs to be studied")
    # suffix to label the output files

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 3:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compare_thesaurus.py ========")
    log.info("    thes1: "+args[0])
    log.info("    thes2: "+args[1])
    log.info("    verb_freq: "+args[2])
    log.info(" ===============================")
        
    
    
    ## get verb_list 
    verb_list=options.vlist
    if verb_list!="":
        log.info("    verbs that will be studied are in "+verb_list)

    ### read frequencies
    freq={}
    with open(args[2],'r') as ffile:
        # ffile has a verb and its frequenc in each line:
        #   know    27189
        #   leave   30277
        for line in ffile:
            line=line.strip()
            if line=="": break
            linep=line.split()
            freq[linep[0]]=linep[1]

    ##############
    # both thesaurus are already stored as shelves, so they can be
    #  directly accessed as a dictionary. We only need to open the files
    #  and access them as if it where dicts
    # We open the files readonly to avoid potential problems. We only read the thesaurus here, we do not modify them
    with closing(shelve.open(args[0], 'r')) as thes1, closing(shelve.open(args[1], 'r')) as thes2, \
       open("neighbours_1.csv", 'w') as fout1, open("neighbours_2.csv", 'w') as fout2, open("neighbours_2.uniq.csv", 'w') as fout3 , \
       open("neighbours_join.csv", 'w') as fout4:
    
        ## put in files info about origin
        fout1.write("## Neighbours found in\n##\t\t"+args[0]+"\n##\twith reference to\n##\t\t"+args[1]+"\n")        
        fout1.write("\nverb\tneighbour\tsim\tfreq\n")

        fout2.write("## Neighbours found in\n##\t\t"+args[1]+"\n##\twith reference to\n##\t\t"+args[0]+"\n")   
        fout2.write("\nverb\tneighbour\tsim\tfreq\n")
        
        fout3.write("## UNIQUE neighbours found in\n##\t\t"+args[1]+"\n##\twith reference to\n##\t\t"+args[0]+"\n")   
        fout3.write("\nverb\tneighbour\tsim\tfreq\n")
        
        fout4.write("## Neighbours found in\n##\t\t"+args[1]+"\n##\twith reference to\n##\t\t"+args[0]+"\n")   
        fout4.write("\nverb\tneighbour\tfreq\tsim_in_1\tposition_in_1\tsim_in_2\tposition_in_2\n")
        
        all_lemmas1=st.get_all_keys(thes1) # returns a SET of keys, so lemmas
        all_lemmas2=st.get_all_keys(thes2)
        
        # get the intersection of both lemma sets
        common_lemmas=all_lemmas1&all_lemmas2
        
        log.debug("   common lemmas in both thesarurus:\n  "+ str(common_lemmas))
        
        if verb_list=="":  # all common lemmas will be output
            studied_lemmas=common_lemmas
        else: # study only the lemmas in the given file
            studied_lemmas=set()
            with open(verb_list,'r') as vfile:
                # vfile has a verb in each line
                for line in vfile:
                    line=line.strip()
                    if line=="": break
                    studied_lemmas.add(line)
            # from that list of lemmas, take only those that are common
            ##studied_lemmas=studied_lemmas&common_lemmas
        
        ###
        # for each lemma in studied_lemmas, output its neighbours and their freq
        for lemma in  studied_lemmas:

            log.debug("------------------------------------------------------------------------")
            log.debug("   creating synonim vector for lemma: "+lemma)
            log.debug("------------------------------------------------------------------------")
                        
            # initialize v1 and v2, vectors of synonims for the given lemma
            log.debug("     ..... v1")
            v1=SynonimList(lemma) # synonims in thesaurus1
            log.debug("     ..... v2")
            v2=SynonimList(lemma) # synonims in thesaurus2
            
            if lemma not in thes1:
                logging.warning("   This is strange, lemma "+ lemma+" is not in the first thesaurus!")
            
            if lemma not in thes2:
                logging.warning("   This is strange, lemma "+ lemma+" is not in the second thesaurus!")
            
            # thes1[lemma] contains a dictionary of lemma/similarity, we need
            #  to add to v1 the similarities for lemmas in common_lemmas
            #  we will pass the whole dictionary and the set of lemmas
            #  to add_synonim_from_dict and it will do the job.
            log.debug("     adding synonim list")
            log.debug("       ..... v1 ")
            v1.add_synonim_from_dict(thes1[lemma],common_lemmas)
            log.debug("       ..... v2")
            v2.add_synonim_from_dict(thes2[lemma],common_lemmas)
            
            ## once we have both vectors created, with the same lemmas,
            #  output the synonims we got for each file.
            log.debug("")
            log.debug("      synonim vectors build for lemma (" + lemma +")")
            
            # sort by similarity:
            v1.sort_by_sim()
            v2.sort_by_sim()
            
            ###
            # output:
            # thes1
            print_info(v1,v2,fout1,lemma)
            print_info(v2,v1,fout2,lemma)
            print_info(v2,v1,fout3,lemma, unique=True)
            print_together(v2,v1,fout4,lemma)


##            
##
##            
##            ## compare similarities: create an insatnce of CompareSynLists class
##            log.debug("         comparing similarities")
##            compare=CompareSynLists(v1,v2, options.prec, common_lemmas) # create object that will take care of everything
##            # note, this object will be the same for all k, in that way we do not need to store or sort the lists everytime
##            
##            ## now, for each k make the comparisons
##            for i in range(0,len(k_list)):
##                # we compare the synonims with k_list[i] and store the measures in fout_list[i]
##                
##                log.debug("---------------------............-----------------------")
##                log.debug("      studying k first elements. k= "+k_list[i])
##                log.debug("---------------------............-----------------------")
##            
##                ## compare_lists(k) return all the measures for the k first elements: 
##                #      orginal lengths (1 and 2), cut lengths (1 and 2), intersection length, union length, rel_overlap, 
##                #         rel_ovelap_intersection, rel_ovelap_union, taub_intersection, pvalue_taub_intersection, taub_union, pvalue_taub_union,
##                len1, len2, cl1, cl2, inters, union, rel_overlap, rel_overlap_i, rel_overlap_u, taub_i, pvalue_i, taub_u, pvalue_u = compare.compare_lists(int(k_list[i]))
##                
##                ## write the measures into the output file
##                fout_list[i].write(lemma+"\t"+str(len1)+"\t"+ str(len2)+"\t"+ str(cl1)+"\t"+ str(cl2)+"\t"+ str(inters)+"\t"+ str(union)+"\t"+ str(rel_overlap)+"\t"+ str(rel_overlap_i)+"\t"+ str(rel_overlap_u)+"\t"+ str(taub_i)+"\t"+ str(pvalue_i)+"\t"+ str(taub_u)+"\t"+ str(pvalue_u)+"\n")
##                       
##            # if --verbose option is given, we will output also the sorted similarities in a file
##            if options.verbose:                        
##                fsorted_sim.write(compare.get_string_sorted_sim()+"\n")
##
##    ## DONE! ##
##    
##    ##  close output files!!
##    
##    for f in fout_list:
##        f.close()
##    
##    if options.verbose:
##        fsorted_sim.close()
##
##
##
