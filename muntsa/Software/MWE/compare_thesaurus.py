#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compare_thesaurus.py: compare two thesaurus with different metrics 
#  (overlap, tau, etc)
#
# Important: the thesaurus must be stored as shelve files. Use thesaurus2shelf.py to 
#  convert csv thesaurus to shelf
# 
# usage:
#  compare_thesaurus.py thesaurus1.shelf thesaurus2.shelf -s name -z -v -p number --log level
###########################################################

import logging
log = logging.getLogger(" MAIN(compare_thesaurus.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
from copy import deepcopy
#ifrom itertools import izip
import codecs
import sys

# import classes for storing synonim lists and comapring them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

### set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # suffix to label the output files
    parser.add_option("-s", "--suffix", dest="suf", default="_", help="Suffix that will be added to the output files to label them")
    # options.prec tells us how many decimals we will take into account. 
    #  if it says two, our precision is 0.01, if it says 3, 0.001, an so on
    parser.add_option("-p", "--prec", dest="prec", default="4", help="numbr of decimals to be taken into account for similarity measures (i.e. 0.112 and 0.113 are the same? then use p=2)")
    # wether we take zeros into account in the synonims vector
    parser.add_option("-z", "--zeros", action="store_true", dest="count_zeros",default=False, help="Wether to take the zero similarities into account when performing the comparisons. Default: False (only non-zero similarities are considered)")
    # verbose outputs files with sorted similarity
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",default=False, help="Output more information in proper files")
    # loggind
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compare_thesaurus.py ========")

    ### prepare structures and output files
    # all_overlaps will contain the overlap info for all verbs (cumulative) --> per anra no ho faig
    #all_overlaps=[]

    ## open output files
    # write to file the relative overlap per verb
    f_rel_overlap=open(options.suf+".rel_overlap.csv",'w')
    
    # write tau per verb
    # f_taua=open(options.suf+".tau-a.csv",'w')# not computing tau a any more...
    f_taub=open(options.suf+".tau-b.csv",'w')

    # to output  verbose stuff
    if options.verbose:
        fsorted_sim=open(options.suf+".sorted_sim.out",'w')

    ##############
    # both thesaurus are already stored as shelves, so they can be
    #  directly accessed as a dictionary. We only need to open the files
    #  and access them as if it where dicts
    # We open the files readonly to avoid potential problems. We only read the thesaurus here, we do not modify them
    with closing(shelve.open(args[0], 'r')) as thes1, closing(shelve.open(args[1], 'r')) as thes2: 
    
        all_lemmas1=st.get_all_keys(thes1) # returns a SET of keys, so lemmas
        all_lemmas2=st.get_all_keys(thes2)
        
        #log.debug("   lemmas in first thesarurus:\n  "+ str(all_lemmas1))
        #log.debug("   lemmas in second thesarurus:\n  "+ str(all_lemmas2))
        
        ## we will study only entries for the common lemmas, so get the intersection
        # of both sets
        common_lemmas=all_lemmas1&all_lemmas2
        
        log.debug("   common lemmas in both thesarurus:\n  "+ str(common_lemmas))

        ###
        # for each lemma in common lemmas, perform the comparison of both similarity vectors
        #  Warning: since the lemmas taken into account may be less than the stored, we will
        #  build two vectors with the elements we want to take into account using SynonimList class
        for lemma in common_lemmas:

            log.debug("------------------------------------------------------------------------")
            log.debug("   creating synonim vector for lemma: "+lemma)
            log.debug("------------------------------------------------------------------------")
                        
            # initialize v1 and v2, vectors of synonims for the given lemma
            log.debug("     ..... v1")
            v1=SynonimList(lemma) # synonims in thesaurus1
            log.debug("     ..... v2")
            v2=SynonimList(lemma) # synonims in thesaurus2
            
            # thes1[lemma] contains a dictionary of lemma/similarity, we need
            #  to add to v1 the similarities for lemmas in common_lemmas
            #  we will pass the whole dictionary and the set of lemmas
            #  to add_synonim_from_dict and it will do the job.
            log.debug("     adding synonim list")
            log.debug("       ..... v1 ")
            v1.add_synonim_from_dict(thes1[lemma],common_lemmas)
            log.debug("       ..... v2")
            v2.add_synonim_from_dict(thes2[lemma],common_lemmas)

            ## once we have both vectors created, with the same lemmas and size,
            #  we can compare the synonims we got for each file.
            log.debug("")
            log.debug("      synonim vectors build for lemma (" + lemma +")")
            
            ## compare similarities
            log.debug("         compating similarities")
            # create object that will take care of everything. equal_lengths=True states that both synonim
            #  list will have the same length, adding 0s if necessary.
            compare=CompareSynLists(v1,v2, options.prec, common_lemmas, options.count_zeros,equal_lengths=True) 
            compare.compare_permutation_lists()
            
            # write the different measures into the corresponding files
            f_rel_overlap.write(lemma+"\t"+compare.get_string_reloverlap()+"\t"+str(compare.no_null)+"\n")
            #f_taua.write(lemma+"\t"+compare.get_string_taua()+"\t"+str(compare.no_null)+"\n")
            f_taub.write(lemma+"\t"+compare.get_string_taub()+"\t"+compare.get_string_taubpvalue()+"\t"+str(compare.no_null)+"\n")
            
            # if --verbose option is given, we will output also the sorted similarities in a file
            if options.verbose:                        
                fsorted_sim.write(compare.get_string_sorted_sim()+"\n")

    ## DONE! ##
    
    ##  close output files!!
    f_rel_overlap.close()
    #f_taua.close()
    f_taub.close()
    if options.verbose:
        fsorted_sim.close()

    ## TO DO (maybe...): at the end, print average overlap... s'ha de repassar si ho vull fer realment
        ##    # print average overlap --> WARNING: so far, it just makes sense if we counted all similarities (0), 
        ##    # otherwise, the length of the arrays varies and the total overlap makes no sense (needs to
        ##    # be designed how to do that)
        ##    if options.count_zeros:
        ##        M=Decimal(len(all_overlaps))
        ##        for i in range(0,len(all_overlaps)):
        ##            sys.stdout.write(str(i+1)+"\t"+str(Decimal(all_overlaps[i])/M)+"\t"+str(no_null)+"\n")
        
        ## before, we should have stored the total overlap or so...::
            ### això no va aquí, però com que tampoc té sentit si no tenim en compte els zeros, per ara ho ignoro        
            ##        # add this overlap to all_overlaps
            ##        #--> WARNING: so far, it just makes sense if we counted all similarities (0), 
            ##        # otherwise, the length of the arrays varies and the total overlap makes no sense (needs to
            ##        # be designed how to do that)
            ##        if options.count_zeros:
            ##            if len(all_overlaps)==0: # first verb, initialize all_overlaps list
            ##                all_overlaps=deepcopy(overlap)
            ##            else:
            ##                for i in range(0,len(overlap)):
            ##                    all_overlaps[i]+=overlap[i]


