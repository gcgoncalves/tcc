#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# get_sims_from_wordnet.py: given a thesaurus, compute similarities 
#  among them using wordnet synsets (to be used for comparison with 
#   corpus measures). We will read an existing thesaurus and output
#   a similar file with the similarities computed with WN 
# 
# usage:
#  get_sims_from_wordnet.py thesaurus.csv
###########################################################

import logging
log = logging.getLogger(" MAIN(get_sims_from_wordnet.py)  --\t")

from contextlib import closing
import shelve
import shelve_thesaurus as st #contains epecific methods to use shelf with MWE thesaurus

from optparse import OptionParser
#from copy import deepcopy
#ifrom itertools import izip
#import codecs
import sys
import itertools

# import classes for storing synonim lists and comparing them
from synonim_list import SynonimList
from compare_syn_lists import CompareSynLists

# import nltk classes for accessing wordnet
from nltk.corpus import wordnet as wn

####################
# get_sim: given the name of the desired similarity and the
#  current stored similarity, copmputes the sim for the two
#  synsets and return it if it is bigger than the current sim.
def get_sim(p,name,s):
    
    if name=="path":
        new_s=p[0].path_similarity(p[1])
        
    elif name=="lch":
        new_s=p[0].lch_similarity(p[1])
    
    elif name=="wup":
        new_s=p[0].wup_similarity(p[1])
        
    log.debug("    " +name+" sim for pair: "+str(p)+" "+str(new_s))
        
    if new_s>s: 
        s=new_s
        log.debug("           best sim so far")
    
    return s


####################
# get_wn_sim(lemma1,lemma2,pos): given two lemmas and a PoS
#  use wn to get the similarity between lemmas
def get_wn_sim(lemma1,lemma2,pos, not_found):
    
    log.debug("  comparing "+lemma1+" with "+lemma2)    
    
    if lemma1 == lemma2:  # if the lemmas are equal, return 0 (for our convinience...)
        return 0.0, 0.0, 0.0, not_found
    
    # get all synsets for each lemma
    syn1=wn.synsets(lemma1, pos=pos)
    syn2=wn.synsets(lemma2, pos=pos)
    log.debug("     synsets for "+lemma1+" "+str(syn1))    
    log.debug("     synsets for "+lemma2+" "+str(syn2))
    
    # if the lemma is not in wordnet the synset list is empty and the similarity
    #  will be zero. We will add tyo the not_found set the verbs that are not in WN
    #  to be output at the end
    if len(syn1)==0: not_found.add(lemma1)
    if len(syn2)==0: not_found.add(lemma2)
    
    # compute similarities between all synsets and return as
    #  similarity the biggest one
    ## We will compute the three similarities available in NLTK: 
    #  path_similaruty, lch_similarity and wup_similarity
    s_path=None
    s_lch=None
    s_wup=None
    for p in list(itertools.product(syn1,syn2)): # p is a tupple (s1,s2), the synstes we need to compute the similarity for
   #     s_path=get_sim(p,'path',s_path)
        s_lch=get_sim(p,'lch',s_lch)
        s_wup=get_sim(p,'wup',s_wup)
    
    log.debug("   best sims for these lemmas: ")
   # log.debug("              path_ "+str(s_path))
    log.debug("              lch_ "+str(s_lch))
    log.debug("              wup_ "+str(s_wup))
    
    if s_path==None: s_path=0.0
    if s_lch==None: s_lch=0.0
    if s_wup==None: s_wup=0.0
    return s_path, s_lch, s_wup, not_found


####################
#  get_wn_sim(line): parse the line, compute the
#  similarities between lemmas and output it
#def output_wn_sim(line, f_path, f_lch, f_wup,not_found):
def output_wn_sim(line, f_lch, f_wup,not_found):
    # split the line into fields
    linep = line.split()
    
    #  linep[0] is the first lemma,  linep[2] the second,  
    #    linep[1] and linep[3] are the ids of the lemma, we will 
    #    output them again.
    # warning: our mwe verbs are joined with "+" but in WN are with "_", so we need to
    # do a replace
    s_path, s_lch, s_wup, not_found=get_wn_sim(linep[0].replace("+","_"), linep[2].replace("+","_"),'v', not_found)
    
    #f_path.write(linep[0]+"\t"+linep[1]+"\t"+linep[2]+"\t"+linep[3]+"\t"+str(s_path)+"\n")
    f_lch.write(linep[0]+"\t"+linep[1]+"\t"+linep[2]+"\t"+linep[3]+"\t"+str(s_lch)+"\n")
    f_wup.write(linep[0]+"\t"+linep[1]+"\t"+linep[2]+"\t"+linep[3]+"\t"+str(s_wup)+"\n")
    
    return not_found

#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog thesaurus.csv\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== get_sims_from_wordnet.py ========")

    ###
    # we will read the list of pair (args[0]) and compute similarity between 
    #  them. I use list of pairs because in this way it will be easier to
    #  follow the order of existing thesaurus
    
    # we are thus reading a thesaurus file like it:
    #    abandon     1     abandon     1     0.0000000000
    #    abandon     1     abate     2     0.0164906340
    #    abandon     1     abbreviate     3     0.0000324093
    #    ...
    #    abort    11      abandon         1       0.0516603557
    #    abort    11      abate   2       0.0000000000
    #    abort    11      abbreviate      3       0.0000000000
    #    ...
    
    # not_found will contain the lemmas that are not in WN. Their
    #  similarity will be zero, but we will output them separatelly to
    #  study the amount of not found lemmas
    not_found=set()
    
    #with open("wn_sim_path.csv", 'w') as f_path, open("wn_sim_lch.csv", 'w') as f_lch, open("wn_sim_wup.csv", 'w') as f_wup:
    with open("wn_sim_lch.csv", 'w') as f_lch, open("wn_sim_wup.csv", 'w') as f_wup:
        with open(args[0],'r') as ifile: #open input file
            for line in ifile:
                line=line.strip()
                if line=="": break
                
                log.debug("\n")
                log.debug("---------------------------")
                log.debug("   reading line: "+line)
                log.debug("---------------------------")
                
               # compute similarities of current line with WN                       
                not_found=output_wn_sim(line, f_lch, f_wup,not_found)
		# not_found=output_wn_sim(line,f_path, f_lch, f_wup,not_found)
    
    # output the not_found set
    with open("not_in_wn.out",'w') as nf: #open input file
        for l in sorted(not_found):
            nf.write(l+"\n")

''' 
Example of wn usage: 
    dog = wn.synset('dog.n.01')
    cat = wn.synset('cat.n.01')

    sing = wn.synset('sing.v.01')
    dance = wn.synset('dance.v.01')
    
    print "dog:", dog 
    print "cat:", cat
    print "dog vs cat", dog.path_similarity(cat)
    print "cat vs dog", cat.path_similarity(dog)
    print "dog vs dog", dog.path_similarity(dog)
    print "cat vs cat", cat.path_similarity(cat)
    
    print "....................."
    print "sing:", sing, sing.root_hypernyms()
    print "dance:", dance, dance.root_hypernyms()
    print "sing vs dance", sing.path_similarity(dance)
    print "dance vs sing", dance.path_similarity(sing)
    print "sing vs sing", sing.lch_similarity(sing)
    print "dance vs dance", dance.path_similarity(dance)

    #print wn.synsets('sing', pos='v')
    #print wn.synsets('catch_up', pos='v')
'''
