#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compute_average.py: given the output csv file with the rel overltap
#  and tau for each verb, returns the mean an variance of all fields
# 
# ./compute_average.py file.csv k : k is a label just for the output
###########################################################

import logging
log = logging.getLogger(" MAIN(compute_average.py)  --\t")

from optparse import OptionParser
#ifrom itertools import izip
import codecs
import sys

import math

#from contextlib import closing

#import bottleneck as bn
import scipy.stats as ss
from numpy import array, vstack


#####################
# main

if __name__ == "__main__":

    ##  get parameters ---
    usage = "usage: %prog file\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    # logging
    parser.add_option("-l", "--log", dest="log", default="info", help="Level of loggin desired (into, debug, warning, critical or error)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    ### get logging
    if options.log!="info" and options.log!="debug" and options.log!="error" and options.log!="warning" and options.log!="critical":
        parser.error("\nincorrect value for logging. Must be info, debug, warning, critical or error")
        exit (1)
        
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

    #logging.basicConfig(level=logging.DEBUG) # valor fixat
    logging.basicConfig(level=LEVELS[options.log]) 
    
    # ------------ ###
    log.info(" ======== compute_average.py "+ args[1] +" ========")

    # compute average and stdev for selected fields in file
    # fields in file:
    #  ##verb	all_syn1	all_syn2	cut_syn1	cut_syn2	intersect	union	rel_ov	rel_ov_int	rel_ov_uni	taub_int	pvalue_int	taub_uni	pvalue_uni

    # fields for which we want the average:	cut_syn1 ($4)	cut_syn2 ($5)	intersect ($6)	union ($7)	rel_ov ($8)	rel_ov_int ($9)	rel_ov_uni ($10)	taub_int ($11)	taub_uni ($13)
    
    # we will also compute jaccard (intersection/union) and we will change the order of the fields a bit
    
    # read the file and stords the desired fields in a numpy array
    first=True
    N=0
    with open(args[0],'r') as ifile:
        for line in ifile:
            line=line.strip()
            if line=="": break
            
            log.debug("   reading line: "+line)
            
            # split fields:
            linep=line.split()
            if linep[0].find("##")!=0: # not a comment line
                
                # compute jaccard (interserction/union) float(linep[5]/ float(linep[6])
                jac=float(linep[5])/float(linep[6])
                
                # add one to the total counts
                N+=1
            
                # add desired fields in the  desired output order (easier for output)
                #  (all_syn1)  (all_syn2)  (cut_syn1)  (cut_syn2)  overlap  union  jaccard  im  taub  im_overlap  taub_overlap  (im_uni)
                new_a=array([float(linep[3]), float(linep[4]), float(linep[5]), float(linep[6]), jac, float(linep[7]), float(linep[12]), float(linep[8]), float(linep[10]), float(linep[9])])
                log.debug("      adding array "+str(new_a))
                
                if first:
                    all_a=new_a
                    first=False
                else:
                    all_a=vstack((all_a,new_a))
                    
                log.debug(" \n  --- new array --- \n"+str(all_a))
                
        # once the array with all elemetns have been created, compute mean and variance
        ##        stdev=all_a.std(axis=0)
        ##        mean=all_a.mean(axis=0)
        stdev=ss.nanstd(all_a)
        mean=ss.nanmean(all_a)

        for i in all_a:
            log.debug("         "+str(i))

        # output all results in order, mean, st  and stdev/N for each element
        # (N is the number of verbs)
    
        sys.stdout.write(args[1]+"\t"+str(N)+"\t")
        for i in range(0,len(stdev)):
            sys.stdout.write(str(mean[i])+"\t"+str(stdev[i])+"\t"+str(stdev[i]/math.sqrt(N))+"\t")
            
        sys.stdout.write("\n")

##    nums = array(((0.01, 0.01, 0.02, 0.04, 0.03),
##              (0.00, 0.02, 0.02, 0.03, 0.02),
##              (0.01, 0.02, 0.02, 0.03, 0.02),
##              (0.01, 0.00, 0.01, 0.05, 0.03)))
##
##print nums.std(axis=1)
### [ 0.0116619   0.00979796  0.00632456  0.01788854]
##
##print nums.mean(axis=1)
### [ 0.022  0.018  0.02   0.02 ]