BEGIN{
 newname=name # iI do a copy to output labels without "_", latex does not like them
 gsub("_","-",newname)
 print "\\begin{table*}[htb]";
 print "\\centering"
 print "\\begin{tabular}{|l|ccc|cc|cc|}"
 print "\\hline"
 printf "cut  & \\multicolumn{3}{|c|}{size} & \\multicolumn{2}{|c|}{total} & \\multicolumn{2}{|c|}{overlap} \\\\ \n"
 printf " & overlap & union & jaccard & $im$ & $\\tau_b$ & $im$  & $\\tau_b$ \\\\ \n"
 print "\\hline"
}


{
if ($1=="all")
  print "\\hline"

if (NR>2)
	printf "%s & %.1f & %.1f & %.2f & %.2f & %.2f & %.2f & %.2f \\\\ \n", $1,$6,$8,$10,$12,$14,$16,$18


}
END{
 print "\\hline"
 print "\\end{tabular}"
 print "\\caption{Results for",newname, "}"
 printf "\\label{%s}\n",name
 print "\\end{table*}"
}
