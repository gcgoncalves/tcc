#! /usr/bin/python3
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# compare_distances.py
# 
# usage:
#  compare_distances.py thesaurus1 thesaurus2
###########################################################

import logging
from optparse import OptionParser
#from copy import deepcopy
from itertools import izip
import codecs
from kendall import *

## needed for one option of kendall tau, but I have problems with isnalling scipy
# import scipy.stats as ss

### I tried to use this fastkendalltau ( http://projects.scipy.org/scipy/ticket/999), 
# which is in theory faster, but it needs scipy to be
#  installed, and I had some problems, to be checked in the future
#from fastkendalltau import *

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


#################################
# compare_similarities:  compare two dictioanaries that have similarities
#  computed in diferent ways (key of the dict is id of a verb, same ids 
#  in the two dicts are associated to same lemma)
#################################
def compare_similarities(v1,v2, lemma, f,verbose=False):

    # (here we can implement different measures, if we want)
    # 1st measure: Kendall tau
    #  to compare v1 and v2, we will compare their ranking
    #  so we need to sort both of them by the similarity (value of the 
    #  dict), keeping the id (key of the dict). The measure calculate 
    #  how different is the order of the ids in each sorted dict
    sim_sorted_v1=sorted(v1, key=v1.__getitem__)
    sim_sorted_v2=sorted(v2, key=v2.__getitem__)
       
    if verbose==True:
        # output the ordered list of verbs if their similarity is bigger than 0
        f.write("\n==========\n"+lemma+"\n==========\n")
        count=0
        for i in range(0,len(sim_sorted_v1)):
            if v1[sim_sorted_v1[i]]!=0 and v2[sim_sorted_v2[i]]!=0:
                f.write(str(sim_sorted_v1[i])+"\t"+str(v1[sim_sorted_v1[i]])+"\t--\t"+str(sim_sorted_v2[i])+"\t"+str(v2[sim_sorted_v2[i]])+"\n")
                ##f.write(str(sim_sorted_v1[i])+"\t"+all_verbs[sim_sorted_v1[i]-1]+"\t--\t"+str(sim_sorted_v2[i])+"\t"+all_verbs[sim_sorted_v2[i]-1] +"\n")
                count+=1
        f.write("verbs with non-zero similarity:\t" +str(count)+"\n")
    
    logging.debug("  calling kendalltau...")
    
    return kendall(sim_sorted_v1,sim_sorted_v2), count
    
    ## if we manage to install scipy we can call: 
    # return ss.kendalltau(sim_sorted_v1,sim_sorted_v2) ## implemented in scipy
    # return fastfastkendalltau.kendalltau(sim_sorted_v1,sim_sorted_v2) ## optimized implementation
    
#####################
# main

if __name__ == "__main__":

    #logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre
    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre


    ##--- get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    ###parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
    ###parser.add_option("-s", "--silent", action="store_false", dest="verbose")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
        

    #if options.vector_type!="frequency" and options.vector_type!="absolute" and options.vector_type!="binary":
        #parser.error("\nincorrect value for vector_type. May be:\n\t'frequency': number of times each cue has been seen over total number of occurrences, \n\t'absolute': number of times each cue has been seen,\n\t'binary': one if the cue has been seen once or more, zero otherwise.")
        #exit (1)
    # ----
    logging.info(" ======== compare_distances.py ========")
    #logging.info( " reading file:"+ args[0])

    ##############
    # We need to read the two files, which contain the same thesaurus with
    #  similarities computed with different metrics, and compare the metrics
    #
    # The file has, for each verb, the similarities with respect to all other
    #  verbs. To avoid memory problems, we will read one verb at a time (and 
    #  all its similarities) and make the comparison, then proceed to the 
    #  next verb.
    #
    # At the samle time we read the file, we will build an index with all 
    #  the verbs that will allow us to relate id and verb. Not sure if
    #  we need it, though...
    # Alternativelly, we could read an independent file containing the 
    #  verb and id pairs first of all and use the sixe of it to create
    #  arrays and so directly with the desired size. Would it be better???    
    ##############

    ## initialize variables
    all_verbs=[]
    lemma=""
    # we will create a "vector" containing all similarities of a given verb
    #  for the measure in each file. The vector is implemented as a
    #  dictionary, key is index, content is similarity. I do it as a dictionary
    #  because latter we will need to sort it by value and keep the index
    v1={} # dict for file 1
    v2={} # dict for file 2
    
    ### to output  verbose stuff
    fout=open("sorted_sim.out",'w')

    ## open both files to read them at the same time
    with codecs.open(args[0],'r', encoding='utf8') as file1, codecs.open(args[1],'r', encoding='utf8') as file2: 
     # with open(args[0],'r') as file1, open(args[1],'r') as file2: 
        for l1, l2 in izip(file1, file2):
            l1p = l1.strip().split()
            l2p = l2.strip().split()
            
            ## the thesaurus are expected to have the same lemas, in the same
            #   order and with same ids. Report it that does not happen
            if l1p[0] != l2p[0] or l1p[1] != l2p[1] or l1p[2] != l2p[2] or l1p[3] != l2p[3]:
                logging.error("   Missmatch in the lemmas or ids in the two files!")
                logging.error("      line in file1: "+l1)
                logging.error("      line in file2: "+l2)                
                exit(1)
                        
            # if the l1p[0] is different than lemma it means that
            #  we reached a new lemma, so we finished to read similarities
            #  for the current verb. Now we can compare the vector we got
            #  for each file
            if l1p[0] != lemma:
                if len(v1)!=0: # if v1 is empty, this means that we are just 
                               # starting to read the file, no need to compare
                    logging.debug("    finished reading similarities for this lemma (" + lemma +")")
                    # compare v1 and v2
                    tau=compare_similarities(v1,v2, lemma, fout, True)
                    print lemma, tau
                    # re-start lists
                    v1={}
                    v2={}
            
                # store new lemma (that may not be necessary if we do not need 
                #  the vector index) --> a little stupid in that way, better to read the
                # vector index!!
                lemma=l1p[0]    
                all_verbs.append(lemma)
                logging.debug("   starting new lemma " + lemma)

            # add current line info from each file to the corresponding list
            # we just need to append the current similarity (order is the same in
            #  both files, so it will be consistent)
            v1[int(l1p[3])]=float(l1p[4])
            v2[int(l2p[3])]=float(l2p[4])
                
    ### compare the similarities for last verb (once loop is finished)
    tau=compare_similarities(v1,v2, lemma, fout, True)
    
    print lemma, tau


