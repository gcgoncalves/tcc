#! /usr/bin/python3
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


##############################################
# compare_thes_overlap.py: compare two thesaurus in temrs of their overlap
# 
# usage:
#  compare_thes_overlap.py thesaurus1 thesaurus2
##############################################

import logging
from optparse import OptionParser
from copy import deepcopy

import codecs
import sys

# import file with overlapcomputing functions
from check_overlap import *

## needed for one option of kendall tau, but I have problems with isnalling scipy
# import scipy.stats as ss

### I tried to use this fastkendalltau ( http://projects.scipy.org/scipy/ticket/999), 
# which is in theory faster, but it needs scipy to be
#  installed, and I had some problems, to be checked in the future
#from fastkendalltau import *

# set stdout to utf-8
##import sys
##reload(sys)
##sys.setdefaultencoding('utf-8')


#####################
# main

if __name__ == "__main__":

    #logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre
    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre


    ##--- get parameters ---
    usage = "usage: %prog file1 file2\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    ###parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
    ###parser.add_option("-s", "--silent", action="store_false", dest="verbose")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
        

    #if options.vector_type!="frequency" and options.vector_type!="absolute" and options.vector_type!="binary":
        #parser.error("\nincorrect value for vector_type. May be:\n\t'frequency': number of times each cue has been seen over total number of occurrences, \n\t'absolute': number of times each cue has been seen,\n\t'binary': one if the cue has been seen once or more, zero otherwise.")
        #exit (1)
    # ----
    logging.info(" ======== compare_thes_overlap.py ========")
    #logging.info( " reading file:"+ args[0])

    ##############
    # We need to read the two files, which contain the same thesaurus with
    #  similarities computed with different metrics, and compare the metrics
    #
    # The file has, for each verb, the similarities with respect to all other
    #  verbs. To avoid memory problems, we will read one verb at a time (and 
    #  all its similarities) and make the comparison, then proceed to the 
    #  next verb.
    #
    # At the samle time we read the file, we will build an index with all 
    #  the verbs that will allow us to relate id and verb. Not sure if
    #  we need it, though...
    # Alternativelly, we could read an independent file containing the 
    #  verb and id pairs first of all and use the sixe of it to create
    #  arrays and so directly with the desired size. Would it be better???    
    ##############

    ## initialize variables
    all_verbs=[]
    lemma=""
    # we will create a "vector" containing all similarities of a given verb
    #  for the measure in each file. The vector is implemented as a
    #  dictionary, key is index, content is similarity. I do it as a dictionary
    #  because latter we will need to sort it by value and keep the index
    v1={} # dict for file 1
    v2={} # dict for file 2
    
    # all_overlaps will contain the overlap info for all verbs (cumulative)
    all_overlaps=[]


    ## open both files to read them at the same time
    with codecs.open(args[0],'r', encoding='utf8') as file1, codecs.open(args[1],'r', encoding='utf8') as file2: 
     # with open(args[0],'r') as file1, open(args[1],'r') as file2: 
        for l1, l2 in zip(file1, file2):
            l1p = l1.strip().split()
            l2p = l2.strip().split()
            
            ## the thesaurus are expected to have the same lemas, in the same
            #   order and with same ids. Report it that does not happen
            if l1p[0] != l2p[0] or l1p[1] != l2p[1] or l1p[2] != l2p[2] or l1p[3] != l2p[3]:
                logging.error("   Missmatch in the lemmas or ids in the two files!")
                logging.error("      line in file1: "+l1)
                logging.error("      line in file2: "+l2)                
                exit(1)
                        
            # if the l1p[0] is different than lemma it means that
            #  we reached a new lemma, so we finished to read similarities
            #  for the current verb. Now we can compare the vector we got
            #  for each file
            if l1p[0] != lemma:
                if len(v1)!=0: # if v1 is empty, this means that we are just 
                               # starting to read the file, no need to compare
                    logging.debug("    finished reading similarities for this lemma (" + lemma +")")
                   
                    # check overlap of both vectors (in terms of number of elements taken into account)
                    overlap=check_overlap(sorted(v1, key=v1.__getitem__),sorted(v2, key=v2.__getitem__)) # the function compares the two ordered lists of similarity
                    if len(all_overlaps)==0: # first verb, initialize all_overlaps list
                        all_overlaps=deepcopy(overlap)
                    else:
                        for i in range(0,len(overlap)):
                            all_overlaps[i]+=overlap[i]
                    
                    # re-start lists
                    v1={}
                    v2={}
            
                # store new lemma (that may not be necessary if we do not need 
                #  the vector index) --> a little stupid in that way, better to read the
                # vector index!!
                lemma=l1p[0]
                all_verbs.append(lemma)
                logging.debug("   starting new lemma " + lemma)

            # add current line info from each file to the corresponding list
            # we just need to append the current similarity (order is the same in
            #  both files, so it will be consistent)
            v1[int(l1p[3])]=float(l1p[4])
            v2[int(l2p[3])]=float(l2p[4])
                
    # check overlap for last verb
    overlap=check_overlap(sorted(v1, key=v1.__getitem__),sorted(v2, key=v2.__getitem__)) # the function compares the two ordered lists of similarity
    if len(all_overlaps)==0: # first verb, initialize all_overlaps list
        all_overlaps=deepcopy(overlap)
    else:
        for i in range(0,len(overlap)):
            all_overlaps[i]+=overlap[i]
                
    
    # print average overlap
    M=float(len(all_overlaps))
    for i in range(0,len(all_overlaps)):
        sys.stdout.write(str(i+1)+"\t"+str(float(all_overlaps[i]/M))+"\n")



