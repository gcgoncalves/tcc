#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"

import logging

# set stdout to utf-8
#import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')

##################
# check_overlap: checks the overlap of the two sequences for N from 1 to
#  L, this is, augmenting the window of elements we take into account
def check_overlap(s1,s2):
        
    overlap=[]
    
    for i in range (1,len(s1)):
        
        logging.debug("     sequence1 of N="+str(i)+"   "+str(s1[0:i]))
        logging.debug("     sequence2 of N="+str(i)+"   "+str(s2[0:i]))

        logging.debug("        overlaping list: "+str(intersect(s1[0:i], s2[0:i])))
        logging.debug("          overlaping size: "+str(intersect_size(s1[0:i], s2[0:i])))
        
        overlap.append(intersect_size(s1[0:i], s2[0:i]))

    logging.debug("    overlap list:  "+str(overlap))

    return overlap

#################
# intersect: compute the overlaping elements of two lists
def intersect(a, b):
    return list(set(a) & set(b))

#################
# intersection_size: compute the size of overlaping elements of two lists
def intersect_size(a, b):
    return len(set(a) & set(b))
