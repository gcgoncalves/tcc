#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"


###########################################################
# Generate_and_compare_random_sequences.py: generates two random sequences
#  of length L (given as a parameter) and computes the overlaping between them
#  for sub-sequences of length N, with N from 1 to L.
# The process will be reapeated M times (parameter also) and the mean of all 
#  iterations will be returned.
# 
# usage:
#  generate_and_compare_random_sequences.py L M
###########################################################

import logging
from optparse import OptionParser
#from copy import deepcopy
import codecs
import random
#from set import set

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# import file with overlapcomputing functions
from check_overlap import *


######################
# generate_random_seq: generates a random sequence of integers
#  from 1 to L, of length L
def generate_random_seq(L):
    
    s=[]
    for i in range(1,L+1): 
        s.append(i)

    random.shuffle(s)
    
    logging.debug("  created random list:  "+str(s))
    return s

#####################
# main

if __name__ == "__main__":

    #logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre
    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre


    ##--- get parameters ---
    usage = "usage: %prog length repetitions\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    ###parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
    ###parser.add_option("-s", "--silent", action="store_false", dest="verbose")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
        exit (1)
    
    L=int(args[0])
    M=int(args[1])
    
    # each iteration will give an array containing the overlaps from N=1 to N=L
    #  we will store the summ of those arrays in order to give back the
    #  average at the end
    all_overlaps=[0] * L # initialize list of length L
    
    for i in range(0,M):
        
        logging.debug("-----")
        logging.debug("starting iteration "+str(i))
        
        # create two random sequences of length L (with elements from 1 to L)
        seq1=generate_random_seq(L)
        seq2=generate_random_seq(L)
        
        overlap=check_overlap(seq1,seq2)
        
        for i in range(0,L):
            all_overlaps[i]+=overlap[i]
        

    # output: print a table of N and average overlap 
    for i in range(0,L):
        print str(i+1)+"\t"+str(float(all_overlaps[i]/float(M)))
    

