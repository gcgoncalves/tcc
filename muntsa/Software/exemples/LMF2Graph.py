#! /usr/bin/python
# -*- coding: UTF-8 -*-

###########################################################
# LMF2Graph.py
# Given the LMF xml of a lexical resource, 
#  store this information as a graph to perform unification.
# Each lexical entry will be a feature structure identified by
#  its lemma
#
#   WARNING: this version of the program only works for LMF
#    files without references. This is, LMF files containing only
#    <LexicalEntry> elements. I.e. It works for morphological
#    dictionaries, noun classification, etc but not for SCF lexica,
#    for example. SCF lexica define first the frames associated
#    to each verb and then the frames itself. The link between
#    the two is made using the IDs. To convert such a lexicon
#    to feature structures, it is necessary to follow this links
#    and explicitely put the information of each frame in each
#    verb. This part is not implemented in this program. TO DO!!
#
# usage:
#  ./LMF2Graph.py LMF-dictionary.xml
###########################################################

import logging
import codecs
from optparse import OptionParser
from lxml import etree
from nltk.featstruct import FeatStruct
from copy import deepcopy

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#########################
# scape html
html_escape_table = {
    "&": "&amp;",
   '"': "&quot;",
   "'": "&apos;",
   ">": "&gt;",
   "<": "&lt;",
   }

#########################
# html_escape: convert characters into xml adequate ones, to allow reafing with xml parser
#  afterwards
def html_escape(text):
    return  "".join(html_escape_table.get(c,c) for c in text)


##################################
# replace_ugly_things: # susbtitute characters that are not accepted as
#    FeatStruct keys by "_"
def replace_ugly_things(strng): # space, "-", ",", etc are not accepted as FeatStruct keys
    return strng.replace(" ", "_").replace("-", "_").replace(",", ";").replace("'", "_").replace("&","_and_")
    #return strng.replace(" ", "_").replace(",", ";").replace("'", "_").replace("&","_and_")



#########################
# get_lemma: checks if there is lemma in this lexical entry and returns it. If there
#  is no lemma, exit the program
# WARNING: Lemma node should exist to find the lemma, since this is a LMF standard.
#  But the contents of the node may encode the lemma string in
#  a different form than the one predicted here (e.g. the lemma itself
#  may not be named "writtenForm". In this case we will need to modify this function
#  or to look for a more general way to find the lemma.
def get_lemma(lex_en):

    lemma_node=lex_en.find("Lemma")

    if lemma_node is not None:
        lemma=""
        features=lemma_node.findall("feat")
        for f_node in features:
            #print etree.tostring(f_node, encoding="UTF-8")
            if f_node.get("att")=="writtenForm":
                lemma=f_node.get("val")

        if lemma=="": # if no lemma found, stop
            logging.error("Entry "+ lex_en.get("id")+" has no lemma!")
            exit  (1)

    else: # if no lemma found, stop
        logging.error("Entry "+ lex_en.get("id")+" has no lemma!")
        exit  (1)

    return lemma


####################################
# get_child_names: return a list with the different element names under a node
def get_child_names(lex_en):

    tag_list=[]

    for child in lex_en:
        if child.tag not in tag_list:
            tag_list.append(child.tag)

    return tag_list

##################### main #####################

#sys.getdefaultencoding()
logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre

##--- get parameters ---
usage = "usage: %prog LMF-dictionary.xml\n"
parser = OptionParser(usage=usage, version="%prog 1.0")

parser.add_option("-o", "--output_type", dest="output", default="nltk", help="type of desired output. May be 'nltk' for a machine readable output or 'hum' for a human readable output.")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("incorrect number of arguments")
    exit (1)

if options.output!="nltk" and options.output!="hum" and options.output!="human":
    parser.error("\nincorrect value for output. May be 'nltk' for a machine readable output or 'hum' for a human readable output.")
    exit (1)

# ----
logging.info(" ======== LMF2Graph.py ========")
logging.info( " reading file:"+ args[0])
logging.info( " output: "+ options.output)


############
# read LMF file and convert it to feature structures: write each lexical entry
#  into a new file with each contents as an NLTK feature structure

####
# LMF format:
#<Lexicon>
#	<feat att="language" val="es"/>
#	<feat att="numberOfLexicalEntries" val="38604"/>
#
#	<LexicalEntry  id="id9-s">
#                        <feat att="partOfSpeech" val="adj"/>
#                        <Lemma>
#                        <feat att="writtenForm" val="abusivo"/>
#                        </Lemma>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusivas"/>
#                                <feat att="gender" val="f"/>
#                                <feat att="number" val="pl"/>
#                        </WordForm>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusiva"/>
#                                <feat att="gender" val="f"/>
#                                <feat att="number" val="sg"/>
#                        </WordForm>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusivos"/>
#                                <feat att="gender" val="m"/>
#                                <feat att="number" val="pl"/>
#                        </WordForm>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusivo"/>
#                                <feat att="gender" val="m"/>
#                                <feat att="number" val="sg"/>
#                        </WordForm>
#	</LexicalEntry>
#	<LexicalEntry> ...
#####

#####
# we need to transform each LexicalEntry into a feature structure.
# read and parse the xml file with the LMF dictionary --> etree (lxml)
fd =  codecs.open(args[0],'r', encoding='utf-8')
xml_tree = etree.parse(fd)

# get a list of lexical entries
lexical_entries = xml_tree.find("Lexicon").findall("LexicalEntry")

#print lexical_entries
logging.debug("-----------------\n"+str(lexical_entries))

# for each lexical entry, transform its contents into a feat structure and
# write it to the standard output
    #dom_file=codecs.open( args[0].replace('\.xml','\.feat_struct.txt'),'w',encoding='utf-8')

for lex_en in lexical_entries:
    
    # we will indentify each lexical entry by its Lemma. This is the only element
    #  we will look byt its name
    lemma=get_lemma(lex_en)

    logging.debug("-----------------\n"+lemma)

    ###########
    # apanyu: if lemma matches "siglo X" or "siglos X", ignore it, because they are over-generated
    if lemma.find("siglo ")==0 or lemma.find("siglos ")==0 or lemma.find("Siglo ")==0 or lemma.find("Siglos ")==0:
        #sys.stderr(lemma+"\n")
        continue

    # output the id and lemma of the lexical entry
    # Output should be of the form:
    # <LexicalEntry lemma="lemma1" id="id1-s">
    # fs1.1
    # fs1.2
    # fs1.3
    # </LexicalEntry>
    #
    # <LexicalEntry lemma="lemma2" id="id2-s">
    # fs2.1
    # fs2.2
    # </LexicalEntry>

    # start a new lexical entry
    id=lex_en.get("id")
    if id:
        print "\n<LexicalEntry lemma=\""+html_escape(lemma)+"\" id=\""+id+"\">"
    else:
        print "\n<LexicalEntry lemma=\""+html_escape(lemma)+"\">"

    # each element in "LexicalEntry" that is not the Lemma will be converted into
    # feature structure. We will find two kind of children: some "frozen" elements
    #  (Lemma, Sense, WordForm, etc) or "feat" elements <feat att="number" val="sg"/>.
    # To convert LMF elements into feature structures we use the following rules:
    #   1- The “feat” elements in LMF have always de same form: <feat att=”attribute_name” val=”value”/>
    #     Those elements never have children, so they can be safely converted into
    #     feature/value pairs of the kind [attribute_name=value].
    #   2- The elements with specific names (WordForm, Stem, Sense) can have
    #      children and/or attributes. For these elements, we will create a
    #      feature structure with key equal to the element name
    #      (WordForm, Sense, etc). The value for this feature will be another
    #      feature structure containing the information enveded in this element.
    #      This information can come from two sources:
    #        a.	The element has some “<feat>” elements as children. Such elements
    #           are converted into feature-value pairs as in (1). The whole
    #           feature structure assigned to the studied element will be the
    #           list of child features.
    #        b. If the element has attributes of the main element, we will create
    #           feature/value pairs for each element. For example:
    #
    #  Important issue: In LMF each entry can contain a list of elements with
    #  the same key. For example, each lemma may contain a list of WordForms, Senses, etc.
    #  In such cases, we will split every entry into different feature structure:
    #  one for each element in list. If there are different WordForms and Sense,
    #  we will create all combinations of them.
    #  Thus, each lexical entry is represented by a list of feature structures.

    # create the list of feature structures for this lexical entry
    fs_list=[]

    # create one empty feat structure to start the list
    feat_struct=FeatStruct()

    fs_list.append(feat_struct)

    #for each son in lex_en that is not the lemma, add it to each feat structure in fs_list

    # first we need to know which different element names we have (they may be repeated)
    children_names=get_child_names(lex_en)

    logging.debug("-----------------\n"+str(children_names))


    # remove lemma from children_names, because we do not want to encode it in the FS
    children_names.remove("Lemma")

    # we iter by different tag names because different tags will create new FS combinations
    #  but equal tags won't.
    for tag in children_names:
        logging.debug("-----------------\n"+tag)

        # create a new fs_list to add splitted elements
        new_fs_list=[]

        # get all elements wiht this tag (we do this by tag instead of looping
        #  over all elements becasuse we need to treat differently the elements
        #  with repeated tags with respect to other elements
        children=lex_en.findall(tag)

        for child in children: # for each element with this tag, create fs
            
            # "feat" elements will be directly converted into a simple
            # feat structure: "att" is the attribute and "val" is the value:
            #  e.g. partOfSpeech=adj
            # in this case we convert all of them with no splitting process, because
            #  all feat element will be unique once translated into att=val
            if tag=="feat" and child.get("val")!="":  # if the value is empty, just ignore this feat, this shouldn't be in the lexicon
                # for each element in list, add this feature structure
                for fs in fs_list:
                    #fs[replace_ugly_things(child.get("att"))]=replace_ugly_things(child.get("val"))
                    # add "feat_" at the beginning of the key of feat struct in order to
                    #  be able to rebuild the LMf onece unification is performed
                    fs["feat_"+replace_ugly_things(child.get("att"))]=child.get("val")   ## I try to replace only att, not values
                    logging.debug("  fs feat  "+fs.__repr__())


            ## Ohter nodes: complex structure containing its own feat/value. In this
            #  case we need to split feature strcutures when repeated elements are found
            else:
                ######## eiiii!! que no he implementat el cas: **********
                #  <Sense id="id-s1916" synset="ss02787772"/> --> Sense=[attr_id=id-s1916,attr_synset=ss02787772]
                ################ TO DO!!!

                # create the feat structure for what is contained inside this node
                tmp_struct=FeatStruct()
                # loop over all possible features that this node contains and add them to a feat structure.
                for tmp_child in child:
                    if tmp_child.get("val")!="":  # if the value is empty, just ignore this feat, this shouldn't be in the lexicon
                        #tmp_struct[replace_ugly_things(tmp_child.get("att"))]=replace_ugly_things(tmp_child.get("val"))
                        tmp_struct["feat_"+replace_ugly_things(tmp_child.get("att"))]=tmp_child.get("val")
                        logging.debug("  tmp struc "+tmp_struct.__repr__())
                    ##print "---", tmp_child.get("val").replace(" ", "_").replace("-", "_").decode('utf8')

                # add the children to each feat_struct in the list
                for fs in fs_list:
                    if  child.tag in fs: # we already have one FS with this key, create a new one
                        fs1=deepcopy(fs)
                        fs1[child.tag]=tmp_struct
                        fs1.append(fs1)
                        logging.debug("  fs1    "+fs1.__repr__())
                    else:
                        fs[child.tag]=tmp_struct


        # once we have treated all element with a tag, add element in new_fs_list to fs_list
        fs_list=fs_list+new_fs_list


    # Print fs depending on input parameter
    if options.output=="nltk":
        for fs in fs_list:
            print fs.__repr__()
    else:
        for fs in fs_list:
            print "----------------\n",fs

        
    # close this LexicalEntry before moving to the next one
    print "</LexicalEntry>"
