# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/05/2015$"

import logging
import codecs
import random
import os
import time
import subprocess


### Warning: it is necessary to have the directory containing WekaFileManager defined
# in .bashrc:
#   export PYTHONPATH=${PYTHONPATH}:/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/WekaFileManager/src/
# or uncomment the following lines
#   import sys
#   sys.path.append("/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/WekaFileManager/src/")
####

from  weka_file_creator import  WekaFileCreator

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

###########################################################
# SignaturesCreator class: stores information about the corpus and
#   performs queries to get concordances and build the weka file
############################################################
class SignaturesCreator:

    #############################
    # constructor, store parameters and create suds client to
    #  perform cqp queries
    #####################
    def __init__(self,corpus_id,ER_file,sem_class,weka_file_name,lemmas_not_found_file_name):
        
        self.corpus=corpus_id
        self.sem_class=sem_class # just to put it in weka file
        
        # Uncomment this if you want to use the WS for cqp_query (cqp_query_ws_* functions)
        #        # create Suds client to acces cqp_query WS
        #        wsdl_url='http://ws03.iula.upf.edu/soaplab2-axis/services/corpus_workbench.cqp_query?wsdl'
        #        self.cqp_query_client=Client(wsdl_url)

        #        print "--- WS -----"
        #        print  self.cqp_query_client
        #        print "-----------------------"
        #
        #        print "-----------------------"
        #        print self.cqp_query_client.service.getInputSpec()
        #        print "-----------------------"


        # create weka_file_creator instance to store concordances into a weka file
        #  that will be used to classify

        # create a weka file using the cues in ER_File
        self.weka_file_cr=WekaFileCreator(weka_file_name,ER_file, self.sem_class) # opens and initializes a weka file

        # open a file to output the not found lemmas
        self.not_found_file=open(lemmas_not_found_file_name,'w')


    ########################
    # destructor
    ########################
    def __del__(self):
        logging.info(" SignaturesCreator:  closing lemmas_not_found.txt")
        self.not_found_file.close


 ############################
    # get_signatures_of_given_nouns: create signatures for nouns given in the nouns_file
    ############################
    def get_signatures_of_given_nouns(self,nouns_file,threshold,v_type,verbose):

        # read lema list and make the query for each element in list
        f = codecs.open(nouns_file,'r', encoding='utf8')
        while (1):
            line = f.readline()
            if not line: break

            lemma=line.rstrip()

            if lemma!="":
                self.get_signatures_of_lemma(lemma,"?",threshold,v_type,verbose)

        f.close()
        
        # now we have all signatures in weka.arff


    ############################
    # get_signatures_of_gs_nouns: create signatures for nouns given in the nouns_file
    ############################
    def get_signatures_of_gs_nouns(self,gs_file,threshold,v_type,verbose):

        # read lema list and make the query for each element in list
        f = codecs.open(gs_file,'r', encoding='utf8')
        while (1):
            line = f.readline()
            if not line: break

            fields=line.rstrip().split()

            if len(fields)>1:
                lemma=fields[0]
                gs=fields[1]
                
                if lemma!="":
                    self.get_signatures_of_lemma(lemma,gs,threshold,v_type,verbose)

        f.close()
        
        # now we have all signatures in weka.arff
        
    ############################
    # get_signatures_of_all_nouns:create signatures for all nouns in the corpus
    ############################
    def get_signatures_of_all_nouns(self,threshold,v_type,verbose):

        ## make a query to get all nouns

        # we need to create a random name for the file where we will write the
        #  output of the query in order to be able to make parallel queries
        name=str(random.randint(1, 1000))+"_all_nouns.txt"

        logging.info(" getting signatures of all_nouns... "+ name+"...")

        self.get_all_nouns(name)

        # output is in all_nouns.txt, read it and create weka

        # read lema list and make the query for each element in list
        # format of file:
        #  3260	ley  [#301479-#304738]
        #  2885	huelga  [#262262-#265146]
        #     oju! the first space is a tabular, the second a regular space! :-/
        ## we will take those that their number of occurrences is over the given threshold
        
        f = codecs.open(name,'r', encoding='utf8')

        while (1):

            lemma=""
            line = f.readline()
            if not line: break

            line=line.rstrip()

            #logging.info(" studyig line"+  line+"...")

            line_fields=line.split() # this directly splits by tab or space!

            #logging.info("    freq is "+ str(line_fields[0])+", trehshold: "+str(threshold)+"  is bigger? "+str(int(line_fields[0])>=threshold))

            if int(line_fields[0])>=threshold:
                lemma=line_fields[1]
                self.get_signatures_of_lemma(lemma,"?",threshold,v_type,verbose)
            else: # lemma under the threshold, add this lemma to lemmas_not_found.txt
                self.not_found_file.write(line_fields[1]+"\n")
                #logging.info("   "+line_fields[1])
                #logging.info("    lemma is ["+lemma+"]")

        f.close()

        ## delete the random file
        os.remove(name)

    ############################
    # get_signatures_of_lemma: create signatures for a concrete lemma
    ############################
    def get_signatures_of_lemma(self,lemma,gs,threshold,v_type,verbose):

        # create a random name for the file where we will write the output of the query
        #  in order to be able to make parallel queries
        name=str(random.randint(1, 1000))+"_cqp_output.txt"

        self.get_concordances(lemma,name)

        # now in cqp_output.txt we have the concordances. Apply RE to this file
        # to add the corresponding weka line to the weka file
        if verbose:
            found=self.weka_file_cr.add_signature(name,lemma,gs,threshold,v_type)
        else:
            found=self.weka_file_cr.add_signature_silent(name,lemma,gs,threshold,v_type)

        if not found: # there were no concordances, add this lemma to lemmas_not_found.txt
            self.not_found_file.write(lemma+"\n")

        ## delete the random file
        os.remove(name)

    ##############################
    # get_concordances: query corpus to get all
    #  sentences where the given noun occurs with the desired format
    ##############################
    def get_concordances(self,lemma,output_file):

        logging.info(" SignaturesCreator:   getting concordances for \'"+  lemma+"\'...")

        # query to ask for this lemma with pos=NC (FreeLing es), pos=NN (FreeLing en) or pos=N5 (iula) ## note; the checking with Iula may be removed if create_signatures is not longer used with Iula annotated corpus
        query="set AutoShow off; set Context s; set PrintOptions nonum; \
           show -cpos; set LeftKWICDelim '##'; set RightKWICDelim '##'; \
           show -word +lemma +pos; \
           [(pos=\"NC.+\" | pos=\"NN*\" | pos=\"N5.+\") & (lemma=\""+lemma+"\") ]; cat;"

        self.cqp_query(query, output_file)

    ##############################
    # get_all_nouns: query corpus to get all nouns
    ##############################
    def get_all_nouns(self,output_file):

        logging.info(" SignaturesCreator:   getting all nouns")

        # query to ask for all lemmas with pos=NC (or N5 if we are using Iula tagset)
        query="set AutoShow off; set PrintOptions nonum; \
           show -cpos; set LeftKWICDelim '##'; set RightKWICDelim '##'; \
           show -word +lemma +pos; \
           [(pos=\"NC.+\" | pos=\"NN*\" | pos=\"N5.+\")]; count by lemma;"

        # get all_nouns
        self.cqp_query(query, output_file) # output in out_file

        ## if using the wS:
        # get all_nouns may take a long time, so do this query with polling
        ##self.cqp_query_ws_polling(query, output_file) # output in out_file


    ##############################
    # cqp_query: perform the given query to indexed corpus given by id to get all
    #  sentences where the given noun occurs with the desired format
    ##############################
    def cqp_query(self,query, output_file):

        ## we will use the cqp_query script in this machine (system call)
        #  >  cqp_query.sh -D <corpusid> -f <query file>

        # input for cqp is "query" and corpusid, which is self.corpus
        # but we need the query to be written in a file
        q_file=open(output_file+".query","w")
        q_file.write(query)
        q_file.close()

        command="/usr/ws/soaplab-scripts/cqp_query.sh -D "+ self.corpus +" -f "+output_file+".query > "+output_file

        logging.info("-------> "+command)
       
        subprocess.call(command, shell=True)
        # now in output_file we have the concordances. If it is empty, it means that
        # there were no concordances
        
        ## delete the query file
        os.remove(output_file+".query")


    #########################
    # What follows are the old cqp functions that acces the webservice.
    # To use them, you need to create the
    #  client in init function (uncomment proper lines).



    ##############################
    # cqp_query: perform the given query to indexed corpus given by id to get all
    #  sentences where the given noun occurs with the desired format
    ##############################
    def cqp_query_ws(self,query, output_file):

        ## access the cqp WS (it is in the same machine, but I prefer to acces it as WS
        # in that way I can input the query as direct data

        # create input for cqp WS
        input = self.cqp_query_client.factory.create('ns3:Map')

        #logging.info("   query: "+query)

        input.item = {'key':'query_direct_data','value': query}, {'key':'corpusId','value': self.corpus}

        #        print ".............."
        #        print input
        #        print ".............."

        # run directly "runAndWaitfor
        result=self.cqp_query_client.service.runAndWaitFor(input) ## this only works for short queries, we should instead do a polling!

        #        print "====== results: ====="
        #        print result
        #        print "====================="


        ### oju: em torna una url dins d'un Map maligne!! i si no hi ha cap concordança al corpus, no hi ha url!
        # per estar segurs que no tenim les concordances del lemma anterior, esborrem el fitxer
        f = open( output_file, 'w')
        f.close
        # ara mirem el map
        for item in result.item:
            if item.key=="output_url":           # get remote file
                rfile=item.value
                urllib.urlretrieve(rfile, output_file)  # Copy the network object in "rfile" (url) to the local file "output_file". Returns a tuple (filename, headers)

        # now in output_file we have the concordances. If it is empty, it means that
        # there were no concordances

    ##############################
    # cqp_query: perform the given query to indexed corpus given by id to get all
    #  sentences where the given noun occurs with the desired format
    ##############################
    def cqp_query_ws_polling(self,query, output_file):

        ## access the cqp WS (it is in the same machine, but I prefer to acces it as WS
        # in that way I can input the query as direct data

        # create input for cqp WS
        input = self.cqp_query_client.factory.create('ns3:Map')

        #logging.info("   query: "+query)

        input.item = {'key':'query_direct_data','value': query}, {'key':'corpusId','value': self.corpus}

        #        print ".............."
        #        print input
        #        print ".............."

        ## call and run WS with polling
        jobid = self.cqp_query_client.service.createAndRun(input)
        #logging.info(str(jobid))
        sec=float(2)
        while (self.cqp_query_client.service.getStatus(jobid) == 'RUNNING'):
            #logging.info("Still Running, waiting "+str(sec)+"....")
            time.sleep(sec)
            if (sec<60): # while the delay is less than 30 seconds, keep increasing it. After, ask every minute
                sec=sec*float(1.5) # increment the time that we will wait before asking again
            #pass
        file_request =['report','output']
        result = self.cqp_query_client.service.getResults(jobid, file_request)

        #        print "====== results: ====="
        #        print result
        #        print "====================="


        ### oju: em torna una url dins d'un Map maligne!! i si no hi ha cap concordança al corpus, no hi ha url!
        # per estar segurs que no tenim les concordances del lemma anterior, esborrem el fitxer
        f = open( output_file, 'w')
        f.close
        # ara mirem el map
        for item in result.item:
            if item.key=="output_url":           # get remote file
                rfile=item.value
                urllib.urlretrieve(rfile, output_file)  # Copy the network object in "rfile" (url) to the local file "output_file". Returns a tuple (filename, headers)

        # now in output_file we have the concordances. If it is empty, it means that
        # there were no concordances
