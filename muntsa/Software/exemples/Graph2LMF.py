#! /usr/bin/python
# -*- coding: UTF-8 -*-

###########################################################
# Graph2LMF.py
# Given a file with feature structures representing a lexical resource
#  (that was originally in LMF) encode this information as
#   LMF again. Note that the file with FS is expected to already
#   come from an LMF lexicon (converted using LMF2Graph) so
#   it should be only a formal conversion. We do not need
#   to know any semantics of the LMF (if you need that, you
#   should explore lmf_creator class).
#
#   WARNING: this version of the program (parallel to LMF2Graph)
#    only works for LMF files without references.  I.e. It works for morphological
#    dictionaries, noun classification, etc but not for SCF lexica,
#    for example. SCF lexica define first the frames associated
#    to each verb and then the frames itself. The link between
#    the two is made using the IDs. To convert such a lexicon
#    to feature structures, it is necessary to follow this links
#    and explicitely put the information of each frame in each
#    verb. This part is not implemented in this program and I am no
#    sure that it will be trivial to do that in a general way (only
#    formal changes). It seems to me that we will need to define rules 
#    to know which features whould be inside LexicalEntries and which 
#    should not. In fact, we already have a conversos from featStruct 
#    to LMF for subcats. Maybe we can use two different programs? How 
#    do I select them??? (thinking of a generel webservice) ---> TO DO
#    *** this can be done more or less heuristically, the thing is
#        to find a general way to do that. And we should explore
#        the consequences of combined lexica (lexica with semantinc
#        and SCF info, for example!!)
# 
# Each lexical entry is a feature structure identified by
#  its lemma:
#    <LexicalEntry lemma="boda" id="id_6" unified_fs="1" non_unified_from_1="0" non_unified_from_2="0">
#    [Sense=[corpusID='lab', eventive='-0.384', hum='-0.384'], partOfSpeech='noun']  1,2
#    [Sense=[corpusID='env', eventive='-0.384', hum='-0.384'], partOfSpeech='noun']  1,2
#    </LexicalEntry>
#
#    <LexicalEntry lemma="boicot" id="id_7" unified_fs="1" non_unified_from_1="0" non_unified_from_2="0">
#    [Sense=[corpusID='lab', eventive='-0.384', hum='-0.384'], partOfSpeech='noun']  1,2
#    </LexicalEntry>
#
#  <LexicalEntry lemma="contador" id="id360-s">
#    [WordForm=[gender='f', number='pl', writtenForm='contadoras'], partOfSpeech='adj']
#    [WordForm=[gender='f', number='sg', writtenForm='contadora'], partOfSpeech='adj']
#    [WordForm=[gender='m', number='pl', writtenForm='contadores'], partOfSpeech='adj']
#    [WordForm=[gender='m', number='sg', writtenForm='contador'], partOfSpeech='adj']
#  </LexicalEntry>
#
# or
#  <LexicalEntry lemma="calor" id="le_345">
#    [Sense=[corpusID='lab', eventive='unknown'], partOfSpeech='noun']
#    [Sense=[corpusID='env', eventive='yes'], partOfSpeech='noun']
#  </LexicalEntry>
#
# **** OJUUUUUUUUU!!! -- to do!! ****
#    but what happens if we have both senses and wordforms???: How will the feat structus be?
#    would this program, and LMF2Graph.py work properly? We need to check it!!!
#
# usage:
#  ./Graph2LMF.py FS_dictionary.txt
###########################################################

import logging
import codecs
from optparse import OptionParser
from lxml import etree
from nltk.featstruct import FeatStruct

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

### Warning: it is necessary to have the directory containing WekaFileManager and LMFCreator defined
# in .bashrc:
#   export PYTHONPATH=${PYTHONPATH}:/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/WekaFileManager/src/:/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/LMFCreator/src/
# or uncomment following the lines
#  import sys
#   sys.path.append("/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/WekaFileManager/src/")
#   sys.path.append("/home/muntsa/PANACEA/lexical-adquisition/software/pythonModules/LMFCreator/src/")
####
from lmf_creator import LMFCreator

#########################
# scape html
html_escape_table = {
    "&": "&amp;",
   '"': "&quot;",
   "'": "&apos;",
   ">": "&gt;",
   "<": "&lt;",
   }

#########################
# html_escape: convert characters into xml adequate ones, to allow reafing with xml parser
#  afterwards
def html_escape(text):
    return  "".join(html_escape_table.get(c,c) for c in text)


##################################
# replace_ugly_things: # susbtitute characters that are not accepted as
#    FeatStruct keys by "_"
def replace_ugly_things(strng): # space, "-", ",", etc are not accepted as FeatStruct keys
    return strng.replace(" ", "_").replace("-", "_").replace(",", ";").replace("'", "_").replace("&","_and_")
    #return strng.replace(" ", "_").replace(",", ";").replace("'", "_").replace("&","_and_")


##################### main #####################

#sys.getdefaultencoding()
logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre
#logging.basicConfig(level=logging.DEBUG) # si cal es pot afegir com a parametre

##--- get parameters ---
usage = "usage: %prog FS_dictionary.txt\n"
parser = OptionParser(usage=usage, version="%prog 1.0")

parser.add_option("-o", "--output_type", dest="output", default="nltk", help="type of desired output. May be 'nltk' for a machine readable output or 'hum' for a human readable output.")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("incorrect number of arguments")
    exit (1)

if options.output!="nltk" and options.output!="hum" and options.output!="human":
    parser.error("\nincorrect value for output. May be 'nltk' for a machine readable output or 'hum' for a human readable output.")
    exit (1)

# ----
logging.info(" ======== LMF2Graph.py ========")
logging.info( " reading file:"+ args[0])
logging.info( " output: "+ options.output)


############
# read file with feat structs and convert it to feature structures: write each lexical entry
#  into a new file with each contents as an NLTK feature structure
#  <LexicalEntry lemma="contador" id="id360-s">
#    [WordForm=[feat_gender='f', feat_number='pl', feat_writtenForm='contadoras'], feat_partOfSpeech='adj']
#    [WordForm=[feat_gender='f', feat_number='sg', feat_writtenForm='contadora'], feat_partOfSpeech='adj']
#    [WordForm=[feat_gender='m', feat_number='pl', feat_writtenForm='contadores'], feat_partOfSpeech='adj']
#    [WordForm=[feat_gender='m', feat_number='sg', feat_writtenForm='contador'], feat_partOfSpeech='adj']
#  </LexicalEntry>
#
# or
#  <LexicalEntry lemma="calor" id="le_345">
#    [Sense=[feat_corpusID='lab', feat_eventive='unknown'], feat_partOfSpeech='noun']
#    [Sense=[feat_corpusID='env', feat_eventive='yes'], feat_partOfSpeech='noun']
#  </LexicalEntry>
#
#  OJUUUUUUUUU!!! -- to do!!
# but what happens if we have both senses and wordforms???: How will the feat structs be?
#  would this program, and LMF2Graph.py work properly? We need to check it!!!
############

##
# create an LMF creator that contains all methods needed to encode the
# information into LMF format
lmf_creator=LMFCreator()

f = codecs.open(args[0],'r', encoding='utf8')

# read lines
lemma=""
id=1
while (1):
    line = f.readline()
    if not line: break

    line=line.rstrip()

    if line != "":
        if line.find("<LexicalEntry")==0: # read lemma
            #logging.debug(str(line))
            xml_element = etree.fromstring(line+"</LexicalEntry>") # here we simulate an XMl just to get the lemma easily
            # get lemma
            lemma=xml_element.get("lemma")
            #print lemma
            ## create new lexical entry in final xml
            le=lmf_creator.create_lexical_entry(id,lemma)

        elif  line.find("</LexicalEntry")==0: # end of this lemma FS
            # start again
            lemma=""
            id=id+1

            ## Oju aquí, està fent bé lo de la le???
            lmf_creator.add_le(le)
            
            #le.clear()

        else: # read fs associated to this tag
            # fs lines may contain some information after a tab: [word=..-] 1, so split the line
            fs= FeatStruct(line.split("\t")[0])
            ## add this fs as xml in lmf object
            ## --add pos i tot lo altre!!!
            logging.debug("       introducing  "+fs.__repr__())
            
            le=lmf_creator.add_content_from_fs(le,fs)
           

f.close()

lmf_creator.print_lmf()


####
# LMF format (target):
#<Lexicon>
#	<feat att="language" val="es"/>
#	<feat att="numberOfLexicalEntries" val="38604"/>
#
#	<LexicalEntry  id="id9-s">
#                        <feat att="partOfSpeech" val="adj"/>
#                        <Lemma>
#                        <feat att="writtenForm" val="abusivo"/>
#                        </Lemma>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusivas"/>
#                                <feat att="gender" val="f"/>
#                                <feat att="number" val="pl"/>
#                        </WordForm>
#                        <WordForm>
#                                <feat att="writtenForm" val="abusiva"/>
#                                <feat att="gender" val="f"/>
#                                <feat att="number" val="sg"/>
#                        </WordForm>
#	</LexicalEntry>
#	<LexicalEntry> ...
#####
