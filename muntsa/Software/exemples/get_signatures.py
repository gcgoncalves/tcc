#! /usr/bin/python
# -*- coding: UTF-8 -*-

__author__="muntsa"
__date__ ="$06/03/2012 14:58:09$"


###########################################################
# get_signatures.py
# Given the id of an indexed corpus, a list of nouns (optional)
#  and a ER file create a weka file with signatures for all nouns 
#
# usage:
#  ./get_signatures.py corpusID ER_file class_name (-l noun_list.txt -o weka_file.arff -t min_occ --silent)
###########################################################

import logging
from optparse import OptionParser
#from copy import deepcopy

# class that creates weka file
from signatures_creator import SignaturesCreator

# set stdout to utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == "__main__":
    #sys.getdefaultencoding()
    logging.basicConfig(level=logging.INFO) # si cal es pot afegir com a parametre

    ##--- get parameters ---
    usage = "usage: %prog corpusID ER_file class_name [-l lemma_list]\n"
    parser = OptionParser(usage=usage, version="%prog 1.0")

    parser.add_option("-l", "--noun_list", dest="lemma_list", default="", help="File with the list of lemmas to be classified. If none is given, all nouns will be classified")
    parser.add_option("-o", "--output", dest="weka_file", default="weka.arff", help="Name for the weka file (without extension)")
    parser.add_option("-n", "--not_found", dest="lemmas_not_found", default="lemmas_not_found.txt", help="Name for the file with not found lemmas")
    parser.add_option("-t", "--threshold", dest="thr", default="none", help="Minimum number of appearances in the corpus to be considered for classification. Default: 5 if all nouns of the corpus are considered and 1 otherwise")
    parser.add_option("-g", "--gold_st", dest="gs", default="", help="Gold standard file with one noun per line and whether it belongs or not to the class (1/0) separated by tab. If this is given, the lemma list file is ignored")
    parser.add_option("-v", "--vector_type", dest="vector_type", default="frequency", help="type of elements of the vector. May be 'frequency' (number of times each cue has been seen over total number of occurrences), 'absolute' (number of times each cue has been seen) or 'binary' (one if the cue has been seen once or more, zero otherwise.")

    parser.set_defaults(verbose=True)
    parser.add_option("-s", "--silent", action="store_false", dest="verbose")

    (options, args) = parser.parse_args()

    if len(args) != 3:
        parser.error("incorrect number of arguments")
        exit (1)

    if options.vector_type!="frequency" and options.vector_type!="absolute" and options.vector_type!="binary":
        parser.error("\nincorrect value for vector_type. May be:\n\t'frequency': number of times each cue has been seen over total number of occurrences, \n\t'absolute': number of times each cue has been seen,\n\t'binary': one if the cue has been seen once or more, zero otherwise.")
        exit (1)

    # ----
    logging.info(" ======== get_signatures.py ========")
    #logging.info( " reading file:"+ args[0])

    corpus_id=args[0]
    ER_file=args[1]
    sem_class=args[2]

    nouns_file=options.lemma_list # if it is "", no file given, all nouns will be processed
    gs_file=options.gs  # if it is "", no file given, we will add "?" to the weka file
    weka_file_name=options.weka_file
    lemmas_not_found_file_name=options.lemmas_not_found
    v_type=options.vector_type

    if options.thr=="none":
        if nouns_file=="": # all nouns will be considered, set minimum occurrences to 5
            thr=5
        else:
            thr=1
    else: # threshold given by the user
        thr=int(options.thr)

    signatures_creator=SignaturesCreator(corpus_id,ER_file,sem_class,weka_file_name,lemmas_not_found_file_name)

    ###
    # depending on whether we have a file with noun list or not, call different
    # classification functions
    if gs_file!="": # get signatures for nouns in gs_file
        logging.info(" get_signatures.py: getting signatures for given nouns appearing more than "+ str(thr)+" times (with gold standard info)...")
        signatures_creator.get_signatures_of_gs_nouns(gs_file,thr,v_type,options.verbose)
    else:
        if nouns_file=="":  # create signatures for all nouns in corpus
            logging.info(" get_signatures.py: no noun file given, all nouns in corpus appearing more than "+ str(thr)+" times will be added to weka file")
            signatures_creator.get_signatures_of_all_nouns(thr,v_type,options.verbose)
        else:
            logging.info(" get_signatures.py: getting signatures for given nouns appearing more than "+ str(thr)+" times (without gold standard info)...")
            signatures_creator.get_signatures_of_given_nouns(nouns_file,thr,v_type,options.verbose)
        
