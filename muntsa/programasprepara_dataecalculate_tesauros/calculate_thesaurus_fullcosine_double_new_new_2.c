//-----------------------------------------------------------------------------------
//  calculate_thesaurus_fullcosine.c   20 dezembro 2013  Marco Idiart
//  It calculates the thesaurus using cosine formula.
//  ATTENTION
//  In order for it to work we have to use normalized pmi, npmi
//  
//  Input files:(indicated by the user)
//     files generate by prepare_data_thesaurus usually called
//     verb_index.csv
//     results_index_npmi.csv
//
//
//  Output files:(also indicated by the user)
//     typically called     results_thesaurus*.csv
//
//-----------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "nrutil.h"
#include "nrutil.c"



void similarity_fullcosine( double ***, int, int, int, double ** );
void similarity_fullcosine_selections( double ***, int, int, int, int *, int, double ** );

int main() //int argc, char *argv[])
{
    int i,j,k,v1,v2,count=0,verb_index_old;
    int number_of_verb,number_of_relations,number_of_noun, index, verb_index, rels_index, noun_index, n_selec ;
    int rel_count, verb_rel_count, noun_rel_count, verb_noun_rel_count, *selections;
    double verb_noun_rel_count_norm, entropy_norm, pmi_norm, npmi_norm, epmi_norm, lpmi_norm;
    double p_mle, ***data, **assoc;
    float alpha,beta,gamma, delta;
    char verb[50],noun[50], relation[50], results_all_norm_file[50],thesaurus_file[50], selections_file[50];
    char verbslist[10000][50];
    FILE *fp;
    
    
 //  if(argc < 3)
 //	   {
 //    printf ("the correct use is \n");
 //    printf ("  ./calculate_thesaurus_lin  results_index*.csv(input file)  results_thesaurus_lin.csv(output file)\n");
 //    return -1;
 //	   }
    
    
    // Read parameter_input_file
    fp = fopen("calculate_thesaurus_input_file","r");
    fscanf(fp,"%s", results_all_norm_file );
    fscanf(fp,"%s", selections_file );
    fscanf(fp,"%s", thesaurus_file );           
    fscanf(fp,"%f %f %f",&alpha,&beta,&delta);
    gamma = sqrt( 1- alpha*alpha -beta*beta - delta*delta );
    fclose(fp);
    
    printf("Read input file!\n");
    
    fp = fopen( results_all_norm_file, "r");
    fscanf(fp,"%d\t %d\t %d\n",&number_of_verb,&number_of_relations,&number_of_noun);
    
    data = d3tensor(1,number_of_verb,1,number_of_relations,1,number_of_noun);
    for(i=1;i<=number_of_verb;i++)
        for(j=1;j<=number_of_relations;j++) 
            for(k=1;k<=number_of_noun;k++) data[i][j][k] = 0.0;
    
    
    verb_index_old = 0;
    //while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %lf",verb, &verb_index, relation, &rels_index, noun, &noun_index, &p_mle) != EOF )
    while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\n", verb, &verb_index, relation, &rels_index, noun, &noun_index, &rel_count, &verb_rel_count, &noun_rel_count, &verb_noun_rel_count_norm, &entropy_norm, &pmi_norm, &npmi_norm, &epmi_norm, &lpmi_norm ) != EOF )
    {
    count++;
    if( count%1000 == 0) printf(".");    
    data[verb_index][rels_index][noun_index] = alpha*verb_noun_rel_count_norm + beta*npmi_norm + delta*epmi_norm + gamma*lpmi_norm;
    if( verb_index != verb_index_old )
      {
      strcpy(verbslist[verb_index],verb); 
      verb_index_old = verb_index;    
      }    
    }    
    printf("\n   End of data reading from 'results_index.csv'\n");
    printf("   Number of Verbs=%d  Number of Relations=%d  Number of Nouns=%d\n",number_of_verb,number_of_relations,number_of_noun); 
    fclose(fp);
    
    
    //  Read the file with the selected target verbs 
    selections = ivector(1,number_of_verb);
    n_selec = 0;
    fp = fopen(selections_file,"r");
    while( fscanf(fp,"%d %s",&verb_index, verb) != EOF  )
         {
         n_selec++;
         selections[n_selec] = verb_index;    
         }
    fclose(fp);
    //  n_selec has the number of selected target verbs
    printf("   Read selection file.\n");
    
    assoc = dmatrix(1,number_of_verb,1,number_of_verb);
    
    //similarity_fullcosine( data,number_of_verb,number_of_relations,number_of_noun, assoc );
    similarity_fullcosine_selections( data,number_of_verb,number_of_relations,number_of_noun, selections, n_selec, assoc );
    
     
    fp = fopen(thesaurus_file,"w");
    for(v1=1;v1<=number_of_verb;v1++)
        for(v2=1;v2<=number_of_verb;v2++)
        { 
        if(assoc[v1][v2] != 0 ) // only prints non-zero similarity
          fprintf(fp, "%s\t %d\t %s\t %d\t %.10lf\n",verbslist[v1],v1,verbslist[v2],v2,assoc[v1][v2]); 
        }
    close(fp);
    
    free_dmatrix(assoc,1,number_of_verb,1,number_of_verb);
    free_d3tensor(data,1,number_of_verb,1,number_of_relations,1,number_of_noun);

    printf("   You find the result in '%s'\n",thesaurus_file); 
    
}



void similarity_fullcosine( double ***data, int n_verb, int n_rels, int n_noun, double **assoc )
{
int v1,v2,i,j,k,l;
double *sum_npmi_square, npmi;

sum_npmi_square = dvector(1,n_verb);    
    
printf("   Starting the calculation of similarity using full cosine formula\n");   
    
for(v1=1;v1<=n_verb;v1++)
   {
   sum_npmi_square[v1] = 0.0;
   for(k=1;k<=n_rels;k++)
      for(l=1;l<=n_noun;l++) 
         {
         npmi = data[v1][k][l];    
         sum_npmi_square[v1] += npmi*npmi;
         }    
   }   
    
    
printf("   Starting the second stage.\n");     
    
for(v1=2;v1<=n_verb;v1++)
  for(v2=1;v2<v1;v2++)
     {    
     assoc[v1][v2] = 0.0;
     for(k=1;k<=n_rels;k++)
        for(l=1;l<=n_noun;l++)
           {
           assoc[v1][v2] += data[v1][k][l]*data[v2][k][l];    
           }    
    assoc[v1][v2] /= sqrt(sum_npmi_square[v1]*sum_npmi_square[v2]);
    assoc[v2][v1] = assoc[v1][v2];
    }

printf("   End of the similarity calculation.\n");   
free_dvector(sum_npmi_square,1,n_verb); 
}


void similarity_fullcosine_selections( double ***data, int n_verb, int n_rels, int n_noun, int *selections, int n_selec, double **sim )
{
    int v1,v2,i,j,k,l;
    double *sum_npmi_square, npmi;
    
    sum_npmi_square = dvector(1,n_verb);    
    
    printf("   Starting the calculation of similarity using full cosine formula\n");   
    
    for(v1=1;v1<=n_verb;v1++)
    {
        sum_npmi_square[v1] = 0.0;
        for(k=1;k<=n_rels;k++)
            for(l=1;l<=n_noun;l++) 
            {
                npmi = data[v1][k][l];    
                sum_npmi_square[v1] += npmi*npmi;
            }    
    }   
    
    
    printf("   Starting the second stage.\n");     
    
    for(i=2;i <= n_selec; i++)
        for(v2=1;v2 < n_verb;v2++)
           {    
           v1 = selections[i];
           sim[v1][v2] = 0.0;
           for(k=1;k<=n_rels;k++)
              for(l=1;l<=n_noun;l++)
                 {
                 sim[v1][v2] += data[v1][k][l]*data[v2][k][l];    
                 }    
           sim[v1][v2] /= sqrt(sum_npmi_square[v1]*sum_npmi_square[v2]);
           // sim[v2][v1] =  sim[v1][v2];
           }
    
    printf("   End of the similarity calculation.\n");   
    free_dvector(sum_npmi_square,1,n_verb);
}



