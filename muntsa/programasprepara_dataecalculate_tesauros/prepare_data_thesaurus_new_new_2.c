//-----------------------------------------------------------------------------------
//  prepare_data_thesaurus_new_new.c   15 march 2013  Marco Idiart
//  It calculates the pontwise mutual information pf the triples (verb,relation,noun)
//  taking from BNC.
//
//  Input files:
//     1 files containing the counts of the combinations
//     (v,r,n)
//
//
//  Output files:
//
//     rels_index.csv
//     verb_index.csv
//     noun_index.csv
//     results_plus.csv
//     results_index.csv
//
//-----------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "nrutil.h"
#include "nrutil.c"
#include "ran2.c"

/*------------------------------------------------------------------------------------
 

Example of the input file ( only illustrative ) 
 
 
 VN_rels.csv ------------------>>  (v,r,n)
 abandon	academia	2	dobj
 abandon	achievement	1	dobj
 abandon	action      5	dobj
 abandon	action      1	obj
 abandon	activity	2	dobj
 abandon	adherence	1	dobj
 abandon	advantage	2	dobj
 abandon	advice      1	dobj
 abandon	aerodrome	1	obj
 abandon	africa      1	obj
	
 ...
 
---------------------------------------------------------------------------------------*/ 

// This is a hashtable that I adapted from something I took from internet. More details
// are found inside the file 'hastable.c'


#include "hashtable.c"

/* --- What is inside  'hashtable.c'  -------------------------------------------------- 
 
 hash_table_t   *create_hash_table(int );
 unsigned int   hash(hash_table_t *, char *);
 list_t         *lookup_string(hash_table_t *, char *);
 int            add_string(hash_table_t *, char *);
 void           free_table(hash_table_t *);
 
--------------------------------------------------------------------------------------- */ 



int main(int argc, char *argv[])
{
    int number, count, count_min, count_noise, i,j,k,l, index, rel_count, verb_rel_count, noun_rel_count,verb_noun_rel_count, count_dobj ;
    int number_of_relations=0,number_of_noun_rels=0, number_of_verb_rels=0, number_of_verb=0, number_of_noun=0, number_old;
    int number_of_rels_old=0, number_of_noun_old=0, number_of_verb_old=0;
    int verb_index, noun_index, rels_index;
    char verb[50],noun[50], relation[50], tag[50], *noun_relation, *verb_relation, noisechar,vminchar;
    char results_plus_file[50], results_all_file[50],results_all_norm_file[50],results_index_file[50], results_index_npmi_file[50], input_file_noise[50], entropy_output_file[50];
    double pmi, npmi, lpmi, epmi,  prob_rn, entropy2;
    long seed; // = -341880;
    
        
    FILE *fp,*fp2,*fp3,*fp4,*fp5;
    hash_table_t *rels_HT, *noun_rels_HT, *verb_rels_HT, *verb_HT, *noun_HT;
    
/* ----------------------------------------------------------------------------------------
 Number of lines of the triplets relation
 
  901109        obj-final-triples.txt
 2247743    subjobj-final-triples.txt
 ------------------------------------------------------------------------------------------ */
    
    if(argc < 1)
	{
        printf ("the correct use is \n");
        printf ("  ./prepare_data_thesauros verb_noun_rels_file \n");
        return -1;
	}

    printf("Enter the minimal number of counts allowed for analysis ="); 
    scanf("%d", &count_min);
    printf("Enter the noise level in number of counts (integer number)="); 
    scanf("%d", &count_noise);
    if( count_noise != 0 )
      {
      printf("Enter a large negative integer to use as seed =");
      scanf("%ld",&seed);
      printf("Seed = %ld\n",seed);
      }
    
// The Output files
 
   sprintf(results_plus_file,      "results_plus_min%d_nois%d.csv" ,     count_min,count_noise);
   sprintf(results_all_file,       "results_all_min%d_nois%d.csv" ,      count_min,count_noise); 
   sprintf(results_all_norm_file,       "results_all_norm_min%d_nois%d.csv" ,      count_min,count_noise);  
   sprintf(results_index_file,     "results_index_min%d_nois%d.csv",     count_min,count_noise); 
   sprintf(results_index_npmi_file,"results_index_npmi_min%d_nois%d.csv",count_min,count_noise);  
   sprintf(input_file_noise,       "inputfile_min%d_nois%d.csv",         count_min,count_noise); 
    
//------------------------------------------------------------------------------------------
// BLOCK 1    
// Reading the data and tests to check if everything is ok and putting in a Hash table  
// I use the hash table to count from the triples, the number of verbs, nouns, relations
// end combinations of noun+relations and verb+relations.    
//------------------------------------------------------------------------------------------     

    number_of_verb_old = 0;
    number_of_noun_old = 0;
    number_of_rels_old = 0;
    
    rels_HT      = create_hash_table(30);
    verb_rels_HT = create_hash_table(100000);  
    verb_HT      = create_hash_table(10000);
    noun_rels_HT = create_hash_table(300000);
    noun_HT      = create_hash_table(30000);
    
    fp  = fopen( argv[1], "r");  
    fp2 = fopen( "verb_index.csv", "w");   //  Save a a list of  (verb numerical index,         verb) 
    fp3 = fopen( "noun_index.csv", "w");   //  Save a a list of  (noun numerical index,         noun) 
    fp4 = fopen( "rels_index.csv", "w");   //  Save a a list of  (relation numerical index, relation) 
    fp5 = fopen( input_file_noise, "w");
     
    while( fscanf(fp,"%s\t %s\t %d\t %s", verb, noun, &verb_noun_rel_count, relation ) != EOF )
        { 
        // Here the noise is added    
        if ( count_noise != 0 )
           { 
           verb_noun_rel_count += floor(2*count_noise*ran2(&seed)+1)-count_noise;
           }
        
        // Only triples with counts larger than count_min are considered    
        if( verb_noun_rel_count >= count_min )
          {
           // add the verbs names to the hash table and print the list in a file   
           add_string_add(verb_HT, verb,     verb_noun_rel_count, &number_of_verb);  
           if( number_of_verb != number_of_verb_old )      fprintf(fp2,"%d\t %s\n",number_of_verb,verb);    
           number_of_verb_old  = number_of_verb;    
            
           // add the relations names to the hash table and print the list in a file 
           add_string_add(rels_HT, relation, verb_noun_rel_count, &number_of_relations); 
           if( number_of_relations != number_of_rels_old ) fprintf(fp4,"%d\t %s\n",number_of_relations,relation);   
           number_of_rels_old = number_of_relations;    
           
           // add the noun names to the hash table and print the list in a file   
           add_string_add(noun_HT, noun,     verb_noun_rel_count, &number_of_noun);
           if( number_of_noun != number_of_noun_old )      fprintf(fp3,"%d\t %s\n",number_of_noun,noun);
           number_of_noun_old  = number_of_noun;    
        
           // add verb+relation to a hash table   
           verb_relation = concatenar(verb,"_",relation);
           add_string_add(verb_rels_HT, verb_relation, verb_noun_rel_count, &number_of_verb_rels);  
        
           // add noun+relation to a hash table   
           noun_relation = concatenar(noun,"_",relation);
           add_string_add(noun_rels_HT, noun_relation, verb_noun_rel_count, &number_of_noun_rels);
                    
           // generate a file that is filtered for minimal counts and has some noise. This
           // file is read in the next block    
           fprintf(fp5,"%s\t %s\t %d\t %s\n", verb, noun, verb_noun_rel_count, relation );  
           }
         
         // this is to show the progress of the reading   
         count++; if(count%10000==0) printf(".");   
         }    
    fclose(fp5);
    fclose(fp4);
    fclose(fp3);
    fclose(fp2);
    fclose(fp);
 
//-----------------------------------------------------------------------------------------------------------
//  BLOCK 2    
//  In this block the data is written in separated files and also pmi, npmi and entropies are calculated
//-----------------------------------------------------------------------------------------------------------    
    
    count=0;
    double **Entropy_rn;
    Entropy_rn    = dmatrix(1,number_of_relations,1,number_of_noun);
    int   **Counts_rn;
    Counts_rn     = imatrix(1,number_of_relations,1,number_of_noun);
    int   **Counts_vr;
    Counts_vr     = imatrix(1,number_of_verb,1,number_of_relations);
    
    
    char noun_list[number_of_noun+1][30],rels_list[number_of_relations+1][10],verb_list[number_of_verb+1][30];
    
    int number_of_verbs_for_relation, number_of_nouns_for_relation;
    
    
    //---------------------------------------------------------------------------------------------------
    //
    // Initialization Entropy = -1.0 to      differentiate between inexistent relation and zero entropy 
    //
    //---------------------------------------------------------------------------------------------------
    for(j=1;j<=number_of_relations;j++) 
       { 
       for(k=1;k<=number_of_noun;k++)
          {
          Counts_rn[j][k]  =     0;     // Counts_rn   stores how many counts for a relation "j" and a noun "k"
          Entropy_rn[j][k] = -1.0;      // Entropy_rn  stores the entropy     for a relation "j" and a noun "k"
          }
       for(i=1;i<=number_of_verb;i++)
          {
          Counts_vr[i][j]  = 0;         // Counts_vr   stores how many counts for a verb "i" and a relation "j"
          }
       }    
    
    fp  = fopen( input_file_noise  , "r");  // this is the file filtered (min & noise) that is read again
    fp2 = fopen( results_plus_file , "w");
    fp3 = fopen( results_index_file, "w");
    fp4 = fopen( results_index_npmi_file, "w");
    //----------------------------------------------------------------------------------------------------
    // Writes in the begining of the file the maximal values of the indexes of  verb, relation, and noun
    // this is essential for the calculate program to set the correct number of reading loops.
    //-----------------------------------------------------------------------------------------------------
    fprintf(fp4,"%d\t %d\t %d\n",number_of_verb,number_of_relations,number_of_noun);
    fprintf(fp3,"%d\t %d\t %d\n",number_of_verb,number_of_relations,number_of_noun);
    fprintf(fp2,"%d\t %d\t %d\n",number_of_verb,number_of_relations,number_of_noun); 
    
    while( fscanf(fp,"%s\t %s\t %d\t %s", verb, noun, &verb_noun_rel_count, relation ) != EOF )
      {
      
      if( verb_noun_rel_count >= count_min )
        {
        // --------------------------------------------------------------------------------------
        //  Retrieving all the counts related to (v,r,n) using hash tables 
        //  These are (*,r,*) (v,r,*) (*,r,n)                                     
        // --------------------------------------------------------------------------------------   
        rel_count      = find_counts(rels_HT,relation);              //  (*,r,*)
        verb_relation  = concatenar (verb,"_",relation);             
        verb_rel_count = find_counts(verb_rels_HT,verb_relation);    //  (v,r,*)
        noun_relation  = concatenar (noun,"_",relation);             
        noun_rel_count = find_counts(noun_rels_HT,noun_relation);    //  (*,r,n) 
        
        verb_index = return_index(verb_HT, verb );
        noun_index = return_index(noun_HT, noun );
        rels_index = return_index(rels_HT, relation );
            
        //----------------------------------------------------------------------------------   
        // Create dictionaries of nouns, relations and verbs.  
        // The dictionary is a char vector that returns  a string for each index    
        //----------------------------------------------------------------------------------    
        strcpy( noun_list[noun_index],     noun);
        strcpy( rels_list[rels_index], relation); 
        strcpy( verb_list[verb_index],     verb);    
            
        Counts_rn[rels_index][noun_index] = noun_rel_count; 
        Counts_vr[verb_index][rels_index] = verb_rel_count;    
            
        //------------------------------------------------------------------------------------------------------  
        //    
        // Calculation of pmi =   log( P(v,n|r)/(P(v|r)P(n|r)) )   
        //
        //------------------------------------------------------------------------------------------------------    
        pmi = log(1.0*verb_noun_rel_count)+log(1.0*rel_count)-log(1.0*verb_rel_count)-log(1.0*noun_rel_count );
        //------------------------------------------------------------------------------------------------------
        //    
        // Calculation of npmi =  log( P(v,n|r)/(P(v|r)P(n|r)) )/(- log P(v,n|r) )   
        //    
        //------------------------------------------------------------------------------------------------------    
        npmi = pmi/(log(1.0*rel_count)-log(1.0*verb_noun_rel_count)); 
        //------------------------------------------------------------------------------------------------------
        //  Entropy of a choice of noun+relation considering possible association with verbs
        //    
        //  E(n,r) = - sum_v P(v|r,n) log P(v|r,n)   onde P(v|r,n) = (v,r,n)/(*,r,n)
        //-------------------------------------------------------------------------------------------------------
        prob_rn = 1.0*verb_noun_rel_count/noun_rel_count;  
           
        if( Entropy_rn[rels_index][noun_index] < 0 )
          {
          Entropy_rn[rels_index][noun_index] = 0;
          }    
        Entropy_rn[rels_index][noun_index] +=  - prob_rn*log(prob_rn);    
            
        //-------------------------------------------------------------------------------------------------------------    
        // Here the data is written in files in terms of numeric indexes so it is easy to manipulate in the future 
        //-------------------------------------------------------------------------------------------------------------
        // results_plus.csv --- has many more information than what is needed
        fprintf(fp2,"%s\t %d\t %s\t %d\t %s\t %d\t",verb, verb_index, relation, rels_index, noun, noun_index);            
        fprintf(fp2,"%d\t %d\t %d\t %d\t", rel_count, verb_rel_count, noun_rel_count, verb_noun_rel_count );
        fprintf(fp2,"%.10lf\t %.10lf\n", pmi, npmi);  
            
        // results_index.csv
        fprintf(fp3,"%s\t %d\t %s\t %d\t %s\t %d\t %.10lf\n",verb, verb_index, relation, rels_index, noun, noun_index, pmi);
        
        // results_index_npmi.csv
        fprintf(fp4,"%s\t %d\t %s\t %d\t %s\t %d\t %.10lf\n",verb, verb_index, relation, rels_index, noun, noun_index, npmi);    
        }
        
      // this is to show the progress of the reading   
      count++; if(count%10000==0) printf(".");     
      } 
    fclose(fp4);
    fclose(fp3);
    fclose(fp2);
    fclose(fp);
    
    
    //-------------------------------------------------------------------------------------------------------------    
    // Print the entropy
    //-------------------------------------------------------------------------------------------------------------
    for(j=1;j<=number_of_relations;j++)
       {
       sprintf(entropy_output_file, "entropy_%s_n.csv", rels_list[j]); printf("\n open entropy for rel=%s\n", rels_list[j] ); 
       fp  = fopen(entropy_output_file, "w");    
       number_of_verbs_for_relation = 0;   
       number_of_nouns_for_relation = 0;    
       for(i=1;i<=number_of_verb;i++)
          {
          if (Counts_vr[i][j] > 0 ) number_of_verbs_for_relation++;  // how many different verbs the relation accepts  
          }    
       // total number of nouns is printed in the beginning of the file.    
       fprintf(fp,"%d\n",number_of_noun);     
       for(k=1;k<=number_of_noun;k++) 
          {  // the entropies are printed
          fprintf(fp,"%s\t%d\t%s\t%d\t",rels_list[j],j,noun_list[k],k);
          // the correct way to normalize the entropy is with the total number of verbs for a given relation    
          fprintf(fp,"%lf\t",Entropy_rn[j][k]);
          // Counts_rn   stores how many counts (tokens) for a relation "j" and a noun "k"     
          fprintf(fp,"%d \n",Counts_rn[j][k]);
          }
       fclose(fp);    
       }    
   
 
//-----------------------------------------------------------------------------------------------------------
//  Freeing all the hash tables    
//-----------------------------------------------------------------------------------------------------------
    free_table(rels_HT);
    free_table(verb_HT);
    free_table(noun_HT);
    free_table(verb_rels_HT);
    free_table(noun_rels_HT);
    
    // Generate results_all_file
    
    fp  = fopen( results_plus_file , "r");
    fp2 = fopen( results_all_file  , "w");
    fscanf (fp ,"%d\t %d\t %d",&number_of_verb,&number_of_relations,&number_of_noun);
    fprintf(fp2,"%d\t %d\t %d\n",number_of_verb,number_of_relations,number_of_noun);
    while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %d\t %lf\t %lf\n", verb, &verb_index, relation, &rels_index, noun, &noun_index,&rel_count, &verb_rel_count, &noun_rel_count, &verb_noun_rel_count, &pmi, &npmi ) != EOF )
    {
        
    epmi = pmi/(1+Entropy_rn[rels_index][noun_index]);
    lpmi = pmi*verb_noun_rel_count;    
    fprintf(fp2,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %d\t %.10lf\t %.10lf\t %.10lf\t %.10lf\t %.10lf\n", verb, verb_index, relation, rels_index, noun, noun_index, rel_count, verb_rel_count, noun_rel_count, verb_noun_rel_count, Entropy_rn[rels_index][noun_index], pmi, npmi, epmi, lpmi );
        
    }   
    fclose(fp2);
    fclose(fp);    
    
    
    
    double *norm_pmi, *norm_npmi, *norm_lpmi, *norm_epmi, *norm_entropy, *norm_triplets;
    norm_pmi      = dvector(1,number_of_verb);
    norm_npmi     = dvector(1,number_of_verb);
    norm_lpmi     = dvector(1,number_of_verb);
    norm_epmi     = dvector(1,number_of_verb);
    norm_entropy  = dvector(1,number_of_verb);
    norm_triplets = dvector(1,number_of_verb);
    
    for(i=1;i<=number_of_verb;i++)
       {
       norm_pmi[i] = norm_npmi[i] = norm_lpmi[i] = norm_epmi[i] = norm_entropy[i] = norm_triplets[i] = 0;    
       }
    
    // Read again results_all_file and calculate the normalization factors
    
    fp2 = fopen( results_all_file  , "r");
    fscanf (fp2 ,"%d\t %d\t %d",&number_of_verb,&number_of_relations,&number_of_noun);
    while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %d\t %lf\t %lf\t %lf\t %lf\t %lf\n", verb, &verb_index, relation, &rels_index, noun, &noun_index, &rel_count, &verb_rel_count, &noun_rel_count, &verb_noun_rel_count, &entropy2, &pmi, &npmi, &epmi, &lpmi ) != EOF )
    {
    norm_pmi[verb_index]      += pmi*pmi;
    norm_npmi[verb_index]     += npmi*npmi;
    norm_lpmi[verb_index]     += lpmi*lpmi;
    norm_epmi[verb_index]     += epmi*epmi;
    norm_entropy[verb_index]  += entropy2*entropy2;
    norm_triplets[verb_index] += verb_noun_rel_count*verb_noun_rel_count;   
    }   
    fclose(fp2);
    
    // Read once again :-(  to print the normalized values
    
    fp2 = fopen( results_all_file       , "r");
    fp3 = fopen( results_all_norm_file  , "w");
    fscanf (fp2 ,"%d\t %d\t %d",&number_of_verb,&number_of_relations,&number_of_noun);
    fprintf(fp3,"%d\t %d\t %d\n",number_of_verb,number_of_relations,number_of_noun);
    while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %d\t %lf\t %lf\t %lf\t %lf\t %lf\n", verb, &verb_index, relation, &rels_index, noun, &noun_index, &rel_count, &verb_rel_count, &noun_rel_count, &verb_noun_rel_count, &entropy2, &pmi, &npmi, &epmi, &lpmi ) != EOF )
    {
    fprintf(fp3,"%s\t %d\t %s\t %d\t %s\t %d\t %d\t %d\t %d\t %.10lf\t %.10lf\t %.10lf\t %.10lf\t %.10lf\t %.10lf\n", verb, verb_index, relation, rels_index, noun, noun_index, rel_count, verb_rel_count, noun_rel_count, verb_noun_rel_count/sqrt(norm_triplets[verb_index]), Entropy_rn[rels_index][noun_index]/sqrt(norm_entropy[verb_index]), pmi/sqrt(norm_pmi[verb_index]), npmi/sqrt(norm_npmi[verb_index]), epmi/sqrt(norm_epmi[verb_index]), lpmi/sqrt(norm_lpmi[verb_index]) );    
    }  
    fclose(fp3);        
    fclose(fp2);
    
    

    printf("\nFound %d different verbs, in %d relation, with %d different nouns.\n",number_of_verb, number_of_relations,number_of_noun );
    printf( "You find the results of this processing in 'results_plus.csv' and 'results_index.csv'.\n" );
    printf( "To complete the thesaurus calculation run 'calculate_thesaurus_<measure>'.\n" );
    
/*    
//-----------------------------------------------------------------------------------------------------------
//  BLOCK 3    
//  Here the nouns that do not add significant information for the similarity calculation of the verbs
//  are deleted from the data
//  Still thinking how to do this.    
//-----------------------------------------------------------------------------------------------------------    
    
    
    fp  = fopen( results_index_file, "r");
    fp2 = fopen( results_index_pmi_filtered, "w");
    fscanf(fp,"%d\t %d\t %d\n",&number_of_verb,&number_of_relations,&number_of_noun);
    
    data = d3tensor(1,number_of_verb,1,number_of_relations,1,number_of_noun);
    for(i=1;i<=number_of_verb;i++)
        for(j=1;j<=number_of_relations;j++) 
            for(k=1;k<=number_of_noun;k++) data[i][j][k] = 0.0;
    
    
    while( fscanf(fp,"%s\t %d\t %s\t %d\t %s\t %d\t %lf",verb, &verb_index, relation, &rels_index, noun, &noun_index, &p_mle) != EOF )
    {
        count++;
        if( count%10000 == 0) printf(".");    
        
        
        
        data[verb_index][rels_index][noun_index] = p_mle;    
    }    
    printf("\n   End of data reading from 'results_index.csv'\n");
    printf("   Number of Verbs=%d  Number of Relations=%d  Number of Nouns=%d\n",number_of_verb,number_of_relations,number_of_noun); 
    fclose(fp);
    

 */   
    
    
    return 0;
}
