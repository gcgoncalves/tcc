#! /bin/bash

no=0 # por agora, sem noise

DATA_DIR=/home/muntsa/MWE/data
BIN_DIR=/home/muntsa/MWE/Software

# for now I remove "-v" which oputs the sorted sim (we already have them for a lot of cases), should go much faster
# but if we want to use it, just change this variable
verbose=""
#verbose="-v"

for zero in "-z"; do # com o sem zeros

  echo ""
  echo "=========== zeros?  $zero ==============="

  for prec in 4 6; do # provamos diferentes presiçoes (decimales to take into account)

    echo ""
    echo "=========== prec $prec ==============="

    for i in 5 3; do # diferentes minimos

	# fullcosine with others
	f1=$DATA_DIR/marco-thesaurus/results_thesaurus_fullcosine_min$i"_nois"$no".csv"

	for s in npmi_lin lin; do

		echo ""
		echo "-------------------------"

		f2=$DATA_DIR/marco-thesaurus/results_thesaurus_$s"_min"$i"_nois0.csv" 
		echo comparing $f1 and $f2 with prec $prec and zeros? $zero
		date
		echo "  unsing suffix " fullcosine_vs_$s.min$i.nois$no.prec$prec$zero
		$BIN_DIR/compare_thesaurus.py $f1 $f2 -s fullcosine_vs_$s.min$i.nois$no.prec$prec$zero -p $prec $zero $verbose >& fullcosine_vs_$s.min$i.nois$no.prec$prec$zero
	done

	# npmi with lin (fullcosine already done)
	f1=$DATA_DIR/marco-thesaurus/results_thesaurus_npmi_lin_min$i"_nois"$no".csv"

	for s in lin; do

		f2=$DATA_DIR/marco-thesaurus/results_thesaurus_$s"_min"$i"_nois"$no".csv"

		echo ""
		echo "-------------------------"
		echo comparing $f1 and $f2  with prec $prec and zeros? $zero
		date
		echo "  unsing suffix " npmi_lin_vs_$s.min$i.nois$no.prec$prec$zero
		$BIN_DIR/compare_thesaurus.py $f1 $f2 -s npmi_lin_vs_$s.min$i.nois$no.prec$prec$zero -p $prec $zero $verbose >& npmi_lin_vs_$s.min$i.nois$no.prec$prec$zero
	done

      # mv the results into the corresponding directory
      mv *min$i*.* results/min$i     

    done
  done
done


### si acabem algun dia, fem els de min 1

for zero in "" "-z"; do # com o sem zeros

  echo ""
  echo "=========== zeros?  $zero ==============="

  for prec in 4 6; do # provamos diferentes presiçoes (decimales to take into account)

    echo ""
    echo "=========== prec $prec ==============="

    for i in 1; do # diferentes minimos

	# fullcosine with others
	f1=$DATA_DIR/marco-thesaurus/results_thesaurus_fullcosine_min$i"_nois"$no".csv"

	for s in npmi_lin lin; do

		echo ""
		echo "-------------------------"

		f2=$DATA_DIR/marco-thesaurus/results_thesaurus_$s"_min"$i"_nois0.csv" 
		echo comparing $f1 and $f2  with prec $prec and zeros? $zero
		date
		echo "  unsing suffix " fullcosine_vs_$s.min$i.nois$no.prec$prec$zero
		$BIN_DIR/compare_thesaurus.py $f1 $f2 -s fullcosine_vs_$s.min$i.nois$no.prec$prec$zero -p $prec $zero $verbose >& fullcosine_vs_$s.min$i.nois$no.prec$prec$zero
	done

	# npmi with lin (fullcosine already done)
	f1=$DATA_DIR/marco-thesaurus/results_thesaurus_npmi_lin_min$i"_nois"$no".csv"

	for s in lin; do
		f2=$DATA_DIR/marco-thesaurus/results_thesaurus_$s"_min"$i"_nois"$no".csv"

		echo ""
		echo "-------------------------"
		echo comparing $f1 and $f2  with prec $prec and zeros? $zero
		date
		$BIN_DIR/compare_thesaurus.py $f1 $f2 -s npmi_lin_vs_$s.min$i.nois$no.prec$prec$zero -p $prec $zero $verbose >& npmi_lin_vs_$s.min$i.nois$no.prec$prec$zero
	done

        # mv the results into the corresponding directory
        mv *min$i*.* results/min$i     

    done
  done
done

