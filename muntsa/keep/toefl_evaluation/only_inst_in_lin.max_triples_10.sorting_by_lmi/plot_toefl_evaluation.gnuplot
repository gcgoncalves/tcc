## plot toefl results for keep_only_relevant_triples and filter_by _counts

## file format: 
## thesaurus       found_in_th     accuracy        cv      tokens_in_th_optimistic types_in_th_optimistic  accuracy_optimistic     cv_optimistic  total   first_not_found others_not_found        none-found      target_not_found
# cosine.max_triples_10.sorting_by_lmi    1       0       inf     1766    990     0.777463        0.0127311       6643    360     1405    4877    755
# cosine.max_triples_20.sorting_by_lmi    2       0.5     0.707107        3037    1290    0.731314        0.0109989       6643    671     2364 3606    755


# files to plot: toefl_results.$suf.sorting_by_$sort.csv, toefl_results.$suf.csv


#set terminal postscript color landscape enhanced dashed lw 1 "Times" 24
set terminal postscript landscape enhanced dashed lw 1 "Times" 20

set pointsize 1.2                             # larger point!
set xlabel '{/Times-Italic number of selected triples} ' offset 0,0.5
set xr [10:200] 
set xtics font "Times, 14"
set logscale x

set xtics nomirror 
set x2tics font "Times, 14"
set x2range[1:50]
set x2label '{/Times-Italic threshold} for triples ' offset 0,-0.5
set logscale x2

set ytics font "Times, 16"
  set yr [0.5:1] 

#set key bottom #reverse 
set key top

    ## plot accuracy (optimistic, field 7)
    set title "WBST task: accuracy"   
    set ylabel '{/Times-Italic accuracy}' offset 3
    set output  "toefl-accuracy-with-bars-selected-instances.eps"

    plot "toefl_results.lin.sorting_by_freq.csv" using 1:7:8 title 's_{lin}, sorting by freq' with  errorlines  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:7:8 title 's_{lin}, sorting by lmi' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:7:8 title 's_{cosine}, sorting by freq' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:7:8 title 's_{cosine}, sorting by lmi' with  errorlines  axes x1y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.lin.csv" using 1:7:8 title 's_{lin}, filtering triples by counts' with  errorlines  axes x2y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.cosine.csv" using 1:7:8 title 's_{cosine}, filtering triples by counts' with  errorlines  axes x2y1

    ## plot accuracy (optimistic, field 7), without error bars
    set title "WBST task: accuracy"   
    set ylabel '{/Times-Italic accuracy}' offset 3
    set output  "toefl-accuracy-selected-instances.eps"

    plot "toefl_results.lin.sorting_by_freq.csv" using 1:7 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:7 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:7 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:7 title 's_{cosine}, sorting by lmi' with  linespoints  axes x1y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.lin.csv" using 1:7 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.cosine.csv" using 1:7 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1



    ## plot tokens (optimistic, field 5)
    set title "WBST task: number of solved instances"   
    set ylabel '{/Times-Italic Solved instances}' offset 3
    set output  "toefl-tokens-selected-instances.eps"
     set yr [0:2000] 

    plot "toefl_results.lin.sorting_by_freq.csv" using 1:5 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:5 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:5 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:5 title 's_{cosine}, sorting by lmi' with  linespoints  axes x1y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.lin.csv" using 1:5 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../../filter_triples_by_counts/toefl_evaluation/only_inst_in_lin.max_triples_10.sorting_by_lmi/toefl_results.cosine.csv" using 1:5 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1






