BEGIN{OFS="\t"}
{

  if(match($0, "1000000")) { # some of the verbs were not found, we will output "--" in that line to keep track 
    if ($3==1000000 && $5==1000000 && $7==1000000 && $9==1000000)  print "--","none", $1, $2, $3, $4, $5, $6, $7, $8, $9
    else {
    	# anyway, see if the first one is better than others (optimistic count) (Only if they are not all -1, this is why we ar inside the if)
	# warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, for stupid problems..)
    	if ($3<=$5 && $3<=$7 && $3<=$9) { print "1","some", $1, $2, $3, $4, $5, $6, $7, $8, $9}
	else {print "0", "some",  $1, $2, $3, $4, $5, $6, $7, $8, $9}
    }
  }
  else { # if all were found in the thesaurus see if the first synonim is the best ranked (strict count)
    if ($3<=$5 && $3<=$7 && $3<=$9) print "1",  "all",  $1, $2, $3, $4, $5, $6, $7, $8, $9
    else print "0",  "all",  $1, $2, $3, $4, $5, $6, $7, $8, $9
  }
}

