#! /bin/bash

nois=0


## with generated thesaurus

for suf in fullcosine lin npmi_lin; do
  for min in 1 3 5 10 20 50; do
	fname=$suf"_min"$min

	echo 
	echo "processing $fname"
	# get the accuraccy for this case
	# warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, for stupid problems..)
	cat  toefl_positions.$fname.csv | awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' | awk -f which_ok.awk > toefl_positions.$fname.ok_ko.csv
  done
done


## with WN thesaurus
for suf in lch wup; do
  for min in 1 3 5; do
	fname=$suf".min"$min

	echo 
	echo "processing $fname"
	# get the accuraccy for this case
	cat toefl_positions.wn_$fname.csv |  awk '{gsub(-1,1000000,$3); gsub(-1,1000000,$5); gsub(-1,1000000,$7); gsub(-1,1000000,$9); print}' |  awk -f which_ok.awk > toefl_positions.wn_$fname.ok_ko.csv

  done
done


#### join outputs from lin and fullcos to be compared.

## ouptut the first line of the comparisons
printf "suffix\tcounts11\tc11/total\tcounts00\tc00/total\tagree\tagree/total\tc10\tc10/total\tc01\tc01/total\tdisagree\tdisagree/total\tnotfound1\tnotfound2\tallnotfound\n" > agreements.csv

for min in 1 3 5 10 20 50; do

    fname1="lin_min"$min
    fname2="fullcosine_min"$min

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > lin_vs_fullcosine.which_ok.min$min.csv
    cat lin_vs_fullcosine.which_ok.min$min.csv | awk -v suf=lin_vs_fullcosine.min$min -f count_agreements.awk >> agreements.csv

done

printf "\n" >>  agreements.csv

# join also different minimums
# (always with the next one)

for suf in lin fullcosine; do

    min1=1
    min2=3

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > $suf.min$min1"_vs_"min$min2.which_ok.csv
    cat $suf.min$min1"_vs_"min$min2.which_ok.csv | awk -v suf=$suf.min$min1"_vs_"min$min2 -f count_agreements.awk >> agreements.csv

    ## 
    min1=3
    min2=5

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > $suf.min$min1"_vs_"min$min2.which_ok.csv
    cat $suf.min$min1"_vs_"min$min2.which_ok.csv | awk -v suf=$suf.min$min1"_vs_"min$min2 -f count_agreements.awk >> agreements.csv

    ## 
    min1=5
    min2=10

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > $suf.min$min1"_vs_"min$min2.which_ok.csv
    cat $suf.min$min1"_vs_"min$min2.which_ok.csv | awk -v suf=$suf.min$min1"_vs_"min$min2 -f count_agreements.awk >> agreements.csv

    ## 
    min1=10
    min2=20

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > $suf.min$min1"_vs_"min$min2.which_ok.csv
    cat $suf.min$min1"_vs_"min$min2.which_ok.csv | awk -v suf=$suf.min$min1"_vs_"min$min2 -f count_agreements.awk >> agreements.csv

    ## 
    min1=20
    min2=50

    fname1=$suf"_min"$min1
    fname2=$suf"_min"$min2

    paste toefl_positions.$fname1.ok_ko.csv toefl_positions.$fname2.ok_ko.csv > $suf.min$min1"_vs_"min$min2.which_ok.csv
    cat $suf.min$min1"_vs_"min$min2.which_ok.csv | awk -v suf=$suf.min$min1"_vs_"min$min2 -f count_agreements.awk >> agreements.csv

done

