## plot toefl results for keep_only_relevant_triples and filter_by _counts

## file format: 
## thesaurus       found_in_th     accuracy        cv      tokens_in_th_optimistic types_in_th_optimistic  accuracy_optimistic     cv_optimistic  total   first_not_found others_not_found        none-found      target_not_found
# cosine.max_triples_10.sorting_by_lmi    1       0       inf     1766    990     0.777463        0.0127311       6643    360     1405    4877    755
# cosine.max_triples_20.sorting_by_lmi    2       0.5     0.707107        3037    1290    0.731314        0.0109989       6643    671     2364 3606    755


# files to plot: toefl_results.$suf.sorting_by_$sort.csv, toefl_results.$suf.csv


#set terminal postscript color landscape enhanced dashed lw 1 "Times" 24
set terminal postscript landscape enhanced dashed lw 1 "Times" 20

set pointsize 1.2                             # larger point!
set xlabel '{/Times-Italic p} ' offset 0,0.5
set xr [10:1000] 
set xtics font "Times, 16"
set logscale x


set key bottom

##############
# make a plot with P, R and F1 for each thesaurus
set yr[0:1]
do for [meas in "lin cosine"] {

  do for [sort in "lmi freq"] {

    set title "WBST task: P, R and F1\n{/*0.8Keeping {/Times-Italic p} most frequent triples per verb}"   
    set ylabel '{/Times-Italic P, R, F1}' offset 2
    set output  "toefl_".meas."_p-r-f1_keep-sorting_by_".sort.".eps"

    plot "toefl_results.".meas.".sorting_by_".sort.".csv" using 1:9 title 'Precision' with  linespoints, \
         "toefl_results.".meas.".sorting_by_".sort.".csv" using 1:11 title 'Recall' with  linespoints, \
         "toefl_results.".meas.".sorting_by_".sort.".csv" using 1:13 title 'F1' with  linespoints


  }
}



set xtics nomirror 
set x2tics font "Times, 16"
set x2range[1:50]
set x2label '{/Times-Italic threshold} for triples ' offset 0,-0.5
set logscale x2

set ytics font "Times, 16"
  set yr [0:1] 

#set key bottom #reverse 
set key bottom

    ## plot accuracy (optimistic, field 9)
    set title "WBST task: Precision"   
    set ylabel '{/Times-Italic precision}' offset 3
    set output  "toefl-prec-with-bars.eps"

    plot "toefl_results.lin.sorting_by_pmi.csv" using 1:9:10 title 's_{lin}, sorting by pmi' with  errorlines  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:9:10 title 's_{lin}, sorting by lmi' with  errorlines  axes x1y1, \
	 "toefl_results.lin.sorting_by_freq.csv" using 1:9:10 title 's_{lin}, sorting by freq' with  errorlines  axes x1y1, \
	 "toefl_results.lin.sorting_by_entropy.csv" using 1:9:10 title 's_{lin}, sorting by entropy' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_pmi.csv" using 1:9:10 title 's_{cosine}, sorting by pmi' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:9:10 title 's_{cosine}, sorting by lmi' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:9:10 title 's_{cosine}, sorting by freq' with  errorlines  axes x1y1, \
	 "toefl_results.cosine.sorting_by_entropy.csv" using 1:9:10 title 's_{cosine}, sorting by entropy' with  errorlines  axes x1y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.lin.csv" using 1:9:10 title 's_{lin}, filtering triples by counts' with  errorlines  axes x2y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.cosine.csv" using 1:9:10 title 's_{cosine}, filtering triples by counts' with  errorlines  axes x2y1

    ## plot precision (optimistic, field 9), without error bars
    set title "WBST task: Precision"   
    set ylabel '{/Times-Italic precision}' offset 3
    set output  "toefl-prec.eps"

    plot "toefl_results.lin.sorting_by_pmi.csv" using 1:9 title 's_{lin}, sorting by pmi' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:9 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_freq.csv" using 1:9 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:9 title 's_{cosine}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:9 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.lin.csv" using 1:9 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.cosine.csv" using 1:9 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1
#	 "toefl_results.lin.sorting_by_entropy.csv" using 1:9 title 's_{lin}, sorting by entropy' with  linespoints  axes x1y1, \
#	 "toefl_results.cosine.sorting_by_entropy.csv" using 1:9 title 's_{cosine}, sorting by entropy' with  linespoints  axes x1y1, \
#	 "toefl_results.cosine.sorting_by_pmi.csv" using 1:9 title 's_{cosine}, sorting by pmi' with  linespoints  axes x1y1, \

    ## plot recall (optimistic, field 11), without error bars
    set title "WBST task: Recall"   
    set ylabel '{/Times-Italic Recall}' offset 3
    set output  "toefl-rec.eps"

    plot "toefl_results.lin.sorting_by_pmi.csv" using 1:11 title 's_{lin}, sorting by pmi' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:11 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_freq.csv" using 1:11 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:11 title 's_{cosine}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:11 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.lin.csv" using 1:11 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.cosine.csv" using 1:11 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1



    ## plot F1 (optimistic, field 13), without error bars
    set title "WBST task: F1"   
    set ylabel '{/Times-Italic F1}' offset 3
    set output  "toefl-f1.eps"

    plot "toefl_results.lin.sorting_by_lmi.csv" using 1:13 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_freq.csv" using 1:13 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_pmi.csv" using 1:13 title 's_{cosine}, sorting by pmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:13 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.lin.csv" using 1:13 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.cosine.csv" using 1:13 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1

    ## plot tokens (optimistic, field 7)
    set title "WBST task: number of solved instances"   
    set ylabel '{/Times-Italic Solved instances}' offset 3
    set output  "toefl-tokens.eps"
     set yr [0:7000] 

    plot "toefl_results.lin.sorting_by_freq.csv" using 1:7 title 's_{lin}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.lin.sorting_by_lmi.csv" using 1:7 title 's_{lin}, sorting by lmi' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_freq.csv" using 1:7 title 's_{cosine}, sorting by freq' with  linespoints  axes x1y1, \
	 "toefl_results.cosine.sorting_by_lmi.csv" using 1:7 title 's_{cosine}, sorting by lmi' with  linespoints  axes x1y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.lin.csv" using 1:7 title 's_{lin}, filtering triples by counts' with  linespoints  axes x2y1, \
	 "../../filter_triples_by_counts/toefl_evaluation/toefl_results.cosine.csv" using 1:7 title 's_{cosine}, filtering triples by counts' with  linespoints  axes x2y1


