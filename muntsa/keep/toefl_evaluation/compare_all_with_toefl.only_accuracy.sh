#! /bin/bash

DATA_DIR=/home/muntsa/RECERCA/MWE/data/filter_triples_by_counts/filter_triples_min_freq_50/shelves
BIN_DIR=/home/muntsa/RECERCA/Software/MWE

nois=0

# output the first line of the output (key)
printf "thesaurus\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\ttotal\tfirst_not_found\tothers_not_found\tnone-found\ttarget_not_found\n" > all_toefl_results.csv


for suf in cosine lin ; do
 for sort in lmi freq; do

  printf "k\tfound_in_th\tprecision\tcv\trecall\tcv\ttokens-opt\ttypes-opt\tprecision-opt\tcv_optimistic\trecall-opt\tcv_optimistic\tf1\ttotal\n" > toefl_results.$suf.sorting_by_$sort.csv

  for keep in 10 20 30 40 50 100 200 500 1000 2000; do

	fname=$suf.max_triples_$keep.sorting_by_$sort

	echo 
	echo "processing $fname"

        # get the accuraccy for this case  
        # warning: "-1" means that it was not found, not that the similarity is small! It is in fact the biggest similarity,
	#  we changed the position "-1" to 1000000 to simulate it is not the first synonim (before calling awk, 
	#  for stupid problems..)    

	cat  toefl_positions.$fname.csv | awk '{for (i=1; i<=NF; i++) gsub(-1,1000000,$i); print}' | awk -v suffix=$fname -f get_accuracy.awk >> all_toefl_results.csv

	## print precision and recall, number of tokens and number if types, also in a file only for this measure (to be used in gnuplot)
	cat  toefl_positions.$fname.csv | awk '{for (i=1; i<=NF; i++) gsub(-1,1000000,$i); print}' | awk -v suffix=$fname -f get_accuracy.awk | awk -v k=$keep 'BEGIN{OFS="\t"}{if ($7!=0) print k, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, 2*$9*$11/($9+$11), $13}' >> toefl_results.$suf.sorting_by_$sort.csv

  done
 done
done
