BEGIN{OFS="\t"; c11=0; c00=0; cnone=0; c10=0; c01=0; cnone1=0; cnone2=0}
{
  if ($1==$12) { # they agree
    if ($1==0) c00+=1
    else if ($1==1) c11+=1
    else if ($1=="--") cnone+=1
  }

  else { # they disagree
    if ($1==0) {
      if ($12==1) c01+=1
      else if ($12=="--" ) cnone2+=1
    }
    else if ($1==1) {
      if ($12==0) c10+=1
      else if ($12=="--" ) cnone2+=1
    }
    else if ($1=="--") cnone1+=1
  }
}
END{
  total=c11+c00+c10+c01
  print suf, c11, c11/total, c00, c00/total, c11+c00, (c11+c00)/total, c10, c10/total, c01, c01/total, c10+c01, (c10+c01)/total, cnone1, cnone2, cnone1+cnone2
}
