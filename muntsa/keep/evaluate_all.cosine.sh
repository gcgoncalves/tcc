#! /bin/bash

## this version of evaluate_all computes wn similarities for all thesarurus
## (chech the "for" loops to be sure all cases are run)
##  If you are just adding new thesaurus, so you do not need to compute
## similarities for previous ones, use evaluate_all.incremental.sh,
## which is faster.

DATA_DIR="/home/muntsa/MWE/data/keep_only_relevant_triples"
BIN_DIR="/home/muntsa/RECERCA/Software/MWE"

## set k_list, which states the sizes of list used to campute worndet similarities. 
## the scripts will output a set of columns for each k in this list
k_list="5,10,15,20,50,100" # sublists that will be conisdered to compute its similarity in wordnet (parameter for scripts)
#k_list_length=`echo $k_list | awk -F"," '{print NR}'`

echo 
echo "list of k to be studied: " $k_list
#echo "length of the list: " $k_list_length

# we need to know the fields for which we will compute the average. 
# The output of the program that compute WN similarities has 4 columns
# for each k (mean and variance for path and wup measures). We want to compute
# the mean of each mean column, so 2,4,6,8... up to 2xk_list_length
mean_fields=`echo $k_list | awk -F"," '{j=2; for (i=1; i<NF; i++) {printf "%s,%s,", j, j+2; j=j+4 } printf "%s,%s\n", j, j+2}'`
echo "   fields used to compute mean: " $mean_fields


######## freq ranges (low and mid)
freq_ranges="150,500"


for i in 50; do # minimum verb frequency, just 50 for now

 echo "============================"
 echo
 echo min frequency: $i
 date

 for meas in cosine; do

  echo
  echo ..... $meas


#  for sort in entropy freq lmi pmi; do
  for sort in lmi freq pmi; do
    ## print the legend for the file that will summarize all info for this measure
    # per freq
    echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv

    # all verbs
    echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > results.all_verbs.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv


    # compute wn similarity for all number of kept triples and add to file 
    for keep in 10 20 30 40 50 100 200 500 1000; do

	echo
	echo keep $keep relevant triples

	$BIN_DIR/compare_k_first_in_wordnet.py $DATA_DIR/min_freq_$i/shelves/thesaurus_$meas.max_triples_$keep.sorting_by_$sort.shelf -s min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort -k $k_list


	### now that the files with all verbs have been created, compute the average for all verbs and by groups of frequencies
	$BIN_DIR/compute_average_per_freq_select_fields.py min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort.average_wordnet_path.csv /home/muntsa/MWE/data/verb-counts/verb_counts.aprox.min_freq_50.csv $keep $mean_fields -f $freq_ranges >>  results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv
	

	### compute also the average of all verbs
	$BIN_DIR/compute_average_per_freq_select_fields.py min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort.average_wordnet_path.csv /home/muntsa/MWE/data/verb-counts/verb_counts.aprox.min_freq_50.csv $keep $mean_fields -f "1000000" >>  results.all_verbs.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv

    done

    ## once the file is complete, separate the frequency ranges

	# create one file per freq range
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.high_freq.csv
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.mid_freq.csv
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.low_freq.csv

	# separate frequencies
	cat results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv |  awk -v franges=$freq_ranges -v suf=results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort 'BEGIN{split(franges,fr,",")}{if($2==fr[1])  print >> suf".low_freq.csv"; else if($2==fr[2]) print >> suf".mid_freq.csv"; else if($2=="bigger")  print >> suf".high_freq.csv"}'

  done
 done
done


## plot results
gnuplot plot_wn_evaluation.gnuplot




