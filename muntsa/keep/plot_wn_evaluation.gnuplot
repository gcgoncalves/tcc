## plot wn results for keep_only_relevant_triples

## file format:
##              k:      5        10      15      20     50       100                             
##              wn_meas:        path   wup         path     wup     path  ...
## min triple counts	verb freq	verbs	mean	stdev	stdev/sqrt(N)	mean  ...



#set terminal postscript color landscape enhanced dashed lw 1 "Times" 24
set terminal postscript landscape enhanced dashed lw 1 "Times" 20

set pointsize 1.2                             # larger point!
set xlabel '{/Times-Italic p} ' offset 0,0.5

set xr [10:1000] 
set xtics font "Times, 16"
set logscale x

set ytics font "Times, 16"

#set key bottom #reverse 
set key top


do for [minfreq in "50"] {
 do for [sort in "freq lmi pmi entropy"] {
  ### 
  # plot number of verbs (col 3) (lin and cosine measures are equal in that)
  unset yr

  set title "Number of verbs"  
  set ylabel 'Number of verbs\n{/*0.8Keeping {/Times-Italic p} most frequent triples per verb}' offset 3
  set output  "number_of_verbs-sorting_by_".sort."-min_freq_".minfreq.".eps"

  plot    "results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:3 title 'all verbs' with linespoints , \
	  "results.per_freq.lin.keep_only_relevant_triples.all_keeps.sorting_by_".sort.".high_freq.csv" using 1:3 title 'high frequent verbs' with linespoints , \
	  "results.per_freq.lin.keep_only_relevant_triples.all_keeps.sorting_by_".sort.".mid_freq.csv" using 1:3 title 'mid frequent verbs' with linespoints , \
	  "results.per_freq.lin.keep_only_relevant_triples.all_keeps.sorting_by_".sort.".low_freq.csv" using 1:3 title 'low frequent verbs' with linespoints 

  ##########
  ### now plot the wn measures
  set key bottom

  set yr [0:0.25] 

  do for [meas in "lin cosine"] {
#   do for [meas in "lin"] {

    set title "WordNet {/Times-Italic path} Similarity\n{/*0.8Keeping {/Times-Italic p} most frequent triples per verb}"  
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  meas."-all_verbs-path-only_relevant_triples-sorting_by_".sort."-min_freq_".minfreq.".eps"
    set key bottom

    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:4 title 'k=5' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:10 title 'k=10' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:16 title 'k=15' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:22 title 'k=20' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:28 title 'k=50' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:34 title 'k=100' with linespoints

    # plot per freq (just 3 selected k: 5, 10, 50)
    # 5--> field 4
	
	k=5
	    ## plot average for each verb range
	    set title "{/Times-Italic s_{".meas."}}: WordNet {/Times-Italic path} for different frequency ranges. {/Times-Italic k=".k."}"  
set ylabel '{/Times-Italic WN similarity}' offset 3
	    set output  meas."-freq_ranges-path-k".k."-only_relevant_triples-sorting_by_".sort."-min_freq_".minfreq.".eps"
	    plot "results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".high_freq.csv" using 1:4 title 'high frequent verbs' with linespoints , \
		"results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".mid_freq.csv" using 1:4 title 'mid frequent verbs' with linespoints , \
		"results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".low_freq.csv" using 1:4 title 'low frequent verbs' with linespoints 


    # 10--> field 10

	k=10
	    ## plot average for each verb range
	    set title "WordNet {/Times-Italic path} Similarity for different frequency ranges, {/Times-Italic k=".k."}\n{/*0.8Keeping {/Times-Italic p} most frequent triples per verb}"  
set ylabel '{/Times-Italic WN similarity}' offset 3
	    set output  meas."-freq_ranges-path-k".k."-only_relevant_triples-sorting_by_".sort."-min_freq_".minfreq.".eps"
	    plot "results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".high_freq.csv" using 1:10 title 'high frequent verbs' with linespoints , \
		"results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".mid_freq.csv" using 1:10 title 'mid frequent verbs' with linespoints , \
		"results.per_freq.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".low_freq.csv" using 1:10 title 'low frequent verbs' with linespoints 
    


    set title "WordNet  {/Times-Italic wup}  for {/Times-Italic s_{".meas."}}"   
    set ylabel '{/Times-Italic wup}' offset 3
    set output  meas."-all_verbs-wup-only_relevant_triples-sorting_by_".sort."-min_freq_".minfreq.".eps"
    set key top

    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:7 title 'k=5' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:13 title 'k=10' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:19 title 'k=15' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:25 title 'k=20' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:31 title 'k=50' with linespoints , \
         "results.all_verbs.".meas.".keep_only_relevant_triples.all_keeps.sorting_by_".sort.".csv" using 1:37 title 'k=100' with linespoints

  }
 }

 ## draw together all versions of keep (k=10)

    set title "WordNet  {/Times-Italic path} for all systems, k=10"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-all_verbs-path-k10-only_relevant_triples-all-min_freq_".minfreq.".eps"
    set key bottom
  #set yr [0.1:0.2] 
  set yr [0:0.25]  
    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title 's_{lin}, sorting by lmi' with  errorlines, \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title 's_{lin}, sorting by freq' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title 's_{cosine}, sorting by lmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title 's_{cosine}, sorting by freq' with errorlines



 ## draw together all versions of keep and also the frequency filters!! iuhuu

    set title "WordNet  {/Times-Italic path}  for all systems, k=5"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-all_verbs-path-k5-min_freq_".minfreq.".eps"
    set key bottom
#  set yr [0.1:0.2]
  set yr [0:0.25]  
  set xtics nomirror 
  set x2tics font "Times, 16"
  set x2range[1:50]
  set x2label '{/Times-Italic threshold} for triples ' offset 0,-0.5
  set logscale x2
    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:4:6 title 's_{lin}, sorting by lmi' with  errorlines  axes x1y1, \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:4:6 title 's_{lin}, sorting by freq' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:4:6 title 's_{cosine}, sorting by lmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:4:6 title 's_{cosine}, sorting by freq' with errorlines, \
	"../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.lin.filter_triples_all_mins.min_freq_50.csv" using 1:4:6 title 's_{lin}, filter by triple counts' with errorlines axes x2y1, \
	"../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.cosine.filter_triples_all_mins.min_freq_50.csv" using 1:4:6 title 's_{cosine}, filter by triple counts' with errorlines  axes x2y1




 ## k=10: one for lin, one for cosine

    set title "WordNet  {/Times-Italic path}  for all systems, k=10"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-lin-all_verbs-path-k10-min_freq_".minfreq.".eps"
    set key top
  set yr [0:0.2]
#  set yr [0:0.25]  
  set xtics nomirror 
  set x2tics font "Times, 16"
  set x2range[1:50]
  set x2label '{/Times-Italic threshold} for triples ' offset 0,-0.5
  set logscale x2
    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'keeping relevant triples, sorting by pmi' with errorlines , \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title '         sorting by lmi' with  errorlines  axes x1y1, \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '         sorting by freq' with errorlines , \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_entropy.csv" using 1:10:12 title '         sorting by entropy' with errorlines , \
	"../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.lin.filter_triples_all_mins.min_freq_50.csv" using 1:10:12 title 'filtering by triple counts' with errorlines axes x2y1


 ## k=10, cosine

    set title "WordNet  {/Times-Italic path}  for all systems, k=10"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-cosine-all_verbs-path-k10-min_freq_".minfreq.".eps"
    set key top
  set yr [0:0.2]
#  set yr [0:0.25]  
  set xtics nomirror 
  set x2tics font "Times, 16"
  set x2range[1:50]
  set x2label '{/Times-Italic threshold} for triples ' offset 0,-0.5
  set logscale x2
    ## plot average for all verbs, all k in the same plot
    plot "results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'keeping relevant triples, sorting by pmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title '         sorting by lmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '         sorting by freq' with errorlines, \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_entropy.csv" using 1:10:12 title '         sorting by entropy' with errorlines, \
	"../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.cosine.filter_triples_all_mins.min_freq_50.csv" using 1:10:12 title 'filter by triple counts' with errorlines  axes x2y1


 ## k=10, cosine vs lin, without entropy (the worst)


    set title "WordNet  {/Times-Italic path}  for all systems, k=10"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-all_verbs-keep-path-k10-min_freq_".minfreq.".eps"
    set key bottom
  set yr [0.05:0.2]
#  set yr [0:0.25]  

    ## plot average for all verbs, all k in the same plot

   plot "results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'LIN -- sorting by pmi' with errorlines , \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title '         sorting by lmi' with  errorlines  axes x1y1, \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '         sorting by freq' with errorlines , \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_entropy.csv" using 1:10:12 title '         sorting by entropy' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'COSINE -- sorting by pmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title 'COSINE -- sorting by lmi' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '         sorting by freq' with errorlines, \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_entropy.csv" using 1:10:12 title '         sorting by entropy' with errorlines


 ## k=10, cosine vs lin and filtering by min, without entropy (the worst)


    set title "WordNet  {/Times-Italic path}  for all systems, k=10"   
set ylabel '{/Times-Italic WN similarity}' offset 3
    set output  "ALL-all_verbs-path-k10-min_freq_".minfreq.".eps"
    set key bottom
  set yr [0.05:0.2]
#  set yr [0:0.25]  

    ## plot average for all verbs, all k in the same plot

   plot  "../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.lin.filter_triples_all_mins.min_freq_50.csv" using 1:10:12 title 'Filtering by triple freq.  -- s_{lin}             ' with errorlines axes x2y1, \
	"../../filter_triples_by_counts/wordnet_evaluation/results.all_verbs.cosine.filter_triples_all_mins.min_freq_50.csv" using 1:10:12 title '                           -- s_{cosine}         ' with errorlines axes x2y1, \
 	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'Keep p triples -- s_{lin} -- sorting by pmi ' with errorlines axes x1y1 , \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title '                          -- sorting by lmi ' with  errorlines  axes x1y1, \
	"results.all_verbs.lin.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '                          -- sorting by freq' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_pmi.csv" using 1:10:12 title 'Keep p triples -- s_{cosine} -- sorting by pmi ' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_lmi.csv" using 1:10:12 title '               -- s_{cosine} -- sorting by lmi ' with errorlines , \
	"results.all_verbs.cosine.keep_only_relevant_triples.all_keeps.sorting_by_freq.csv" using 1:10:12 title '                          -- sorting by freq' with errorlines

}


