#! /bin/bash

### we will evaluate the thesaurus and do the average only for the verbs that are
#   also in DM (given in a file)
mkdir verbs_in_DM
verbs_file="/home/muntsa/MWE/data/utils/verbs_in_DM_and_BNC"

## this version of evaluate_all does not compute wn similarities if a file
## with them for a given thesarurus already exist. This is useful if we
## are only adding more files, so previous thesaurus are untouched
## and we just need to add the new ones. BUT use evaluate_all.sh
## if some previous thesaurus have changed! (or delete the files
## for the changed thesaurus

DATA_DIR="/home/muntsa/MWE/data/keep_only_relevant_triples"
BIN_DIR="/home/muntsa/RECERCA/Software/MWE"

## set k_list, which states the sizes of list used to compute worndet similarities. 
## the scripts will output a set of columns for each k in this list
k_list="5,10,15,20,50,100" # sublists that will be conisdered to compute its similarity in wordnet (parameter for scripts)
#k_list_length=`echo $k_list | awk -F"," '{print NR}'`

echo 
echo "list of k to be studied: " $k_list
#echo "length of the list: " $k_list_length

# we need to know the fields for which we will compute the average. 
# The output of the program that compute WN similarities has 4 columns
# for each k (mean and variance for path and wup measures). We want to compute
# the mean of each mean column, so 2,4,6,8... up to 2xk_list_length
mean_fields=`echo $k_list | awk -F"," '{j=2; for (i=1; i<NF; i++) {printf "%s,%s,", j, j+2; j=j+4 } printf "%s,%s\n", j, j+2}'`
echo "   fields used to compute mean: " $mean_fields


######## freq ranges (low and mid)
freq_ranges="150,500"


for i in 50; do # minimum verb frequency, just 50 for now

 echo "============================"
 echo
 echo min frequency: $i
 date

 for meas in lin; do

  echo
  echo ..... $meas


#  for sort in  entropy freq lmi pmi; do
   for sort in freq lmi pmi; do

    ## print the legend for the file that will summarize all info for this measure
    # per freq
    echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv

    # all verbs
    echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > verbs_in_DM/results.all_verbs.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv


    # compute wn similarity for all number of kept triples and add to file 
    for keep in 10 20 30 40 50 100 200 500 1000; do

	echo
	echo keep $keep relevant triples


	## this step was maybe already performed, so do it only if the created file does not exist
	FILE=min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort.average_wordnet_path.csv
	if [ ! -f $FILE ]; then
	   echo "file $FILE not found, let's create it"
	   $BIN_DIR/compare_k_first_in_wordnet.py $DATA_DIR/min_freq_$i/shelves/thesaurus_$meas.max_triples_$keep.sorting_by_$sort.shelf -s min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort -k $k_list
	else
	   echo "file $FILE already exist, computing means directly"
	fi


	### now that the files with all verbs have been created, compute the average for all verbs and by groups of frequencies
	#  the -l option gives the list of verbs that will be evaluated
	## (this is redone every time, not optimal but since we are concatenating everything in a single file...)
	$BIN_DIR/compute_average_per_freq_select_fields.py min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort.average_wordnet_path.csv /home/muntsa/MWE/data/verb-counts/verb_counts.aprox.min_freq_50.csv $mean_fields $keep -f $freq_ranges -v $verbs_file >>  verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv
	

	### compute also the average of all verbs
	$BIN_DIR/compute_average_per_freq_select_fields.py min_freq_$i/eval.$meas.max_triples_$keep.sorting_by_$sort.average_wordnet_path.csv /home/muntsa/MWE/data/verb-counts/verb_counts.aprox.min_freq_50.csv $mean_fields $keep -f "1000000" -v $verbs_file >>  verbs_in_DM/results.all_verbs.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv

    done

    ## once the file is complete, separate the frequency ranges

	# create one file per freq range
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.high_freq.csv
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.mid_freq.csv
	echo $k_list | awk -F"," '{printf "##\t\tk:"; for (i=1; i<=NF; i++) printf "\t%s\t\t\t\t\t", $i; printf "\n"; printf "##\t\twn_meas:"; for (i=1; i<=NF; i++) printf "\tpath\t\t\twup\t\t"; printf "\n"; printf "## min triple counts\tverb freq\tverbs"; for (i=1; i<=NF; i++) printf "\tmean\tstdev\tstdev/sqrt(N)\tmean\tstdev\tstdev/sqrt(N)"; printf "\n"}' > verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.low_freq.csv

	# separate frequencies
	cat verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort.csv |  awk -v franges=$freq_ranges -v suf=verbs_in_DM/results.per_freq.$meas.keep_only_relevant_triples.all_keeps.sorting_by_$sort 'BEGIN{split(franges,fr,",")}{if($2==fr[1])  print >> suf".low_freq.csv"; else if($2==fr[2]) print >> suf".mid_freq.csv"; else if($2=="bigger")  print >> suf".high_freq.csv"}'

  done
 done
done





