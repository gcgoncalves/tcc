\select@language {brazilian}
\select@language {english}
\select@language {brazilian}
\contentsline {chapter}{1~Introdu\IeC {\c c}\IeC {\~a}o}{11}{CHAPTER.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}Estrutura do trabalho}{12}{section.1.2}
\contentsline {chapter}{2~Trabalhos Relacionados}{13}{CHAPTER.2}
\contentsline {section}{\numberline {2.1}Modelo de contagem}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Modelo preditivo}{15}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Avalia\IeC {\c c}\IeC {\~a}o}{16}{subsection.2.2.1}
\contentsline {chapter}{3~Materiais e M\IeC {\'e}todos}{18}{CHAPTER.3}
\contentsline {section}{\numberline {3.1}Corpora}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}PukWaC}{19}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}brWaC}{20}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Constru\IeC {\c c}\IeC {\~a}o dos tesauros}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Pr\IeC {\'e}-processamento e filtragem}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Modelo de contagem}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Modelo preditivo}{22}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Avalia\IeC {\c c}\IeC {\~a}o}{23}{section.3.3}
\contentsline {chapter}{\xspace {}Refer{\^e}ncias}{26}{schapter.8}
