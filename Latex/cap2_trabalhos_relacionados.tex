% Trabalhos Relacionados
\chapter{Trabalhos Relacionados}

Neste capítulo é feita uma breve revisão do estado da arte, partindo da identificação de contextos, analisando os modelos atuais e finalmente técnicas de avaliação da qualidade destes. 

Tesauros podem ser vistos como dicionários que, em vez de significados, agrupam palavras semanticamente similares, como hipônimos e hiperônimos.  
Conforme \citet{Lin1998automatic}:

\begin{quote}
O significado de uma palavra desconhecida pode ser inferido de seu contexto. Considerando o exemplo (levemente modificado) abaixo em (Nida, 1975, p.167):

    \begin{quote}
    Uma garrafa de tezgüino está na mesa.

    Todos gostam de tezgüino.

    Tezgüino te deixa bêbado.

    Fazemos tezgüino do milho.
    \end{quote}

Os contextos em que a palavra tezgüino é utilizada sugerem que tezgüino pode ser um tipo de bebida alcoólica feita de milho.

Este é um dos maiores desafios do aprendizado de linguagem natural.
\end{quote}

Tesauros distribucionais são baseados na hipótese distribucional \cite{Harris1954word, Firth1957synopsis}, que diz que palavras são caracterizadas pelo contexto em que aparecem. Isto significa que duas palavras que possuem contextos similares são consideradas similares. Para construir estes tesauros, os contextos de cada palavra são extraídos de um corpus.  

O método tradicional de construção de tesauros é o de contagem, que avalia os contextos das palavras através de suas frequências \cite{baroni2014don}. O método preditivo, mais recente, vem ganhando notoriedade e representa palavras como vetores de seus contextos, utilizando então técnicas de redes neurais para avaliar sua similaridade \cite{mikolov2013distributed}.

\section{Modelo de contagem}

\citet{Lin1998automatic} propõe uma medida de similaridade entre duas palavras baseada na quantidade de informação que compartilham dividida pela informação contida em suas descrições. Utilizando esta medida, Lin construiu um tesauro com os duzentos vizinhos mais próximos de um grupo de palavras de um corpus, e o comparou à WordNet, ao Tesauro de Roget e a outros tesauros construídos utilizando o mesmo grupo de palavras, mas diferentes medidas de similaridade.

Os resultados da comparação apontaram que a medida de similaridade proposta era superior às medidas automatizadas existentes. Os tesauros gerados automaticamente apresentaram maior similaridade com a wordnet do que com o Tesauro de Roget.

Um estudo realizado por \citet{padro2014comparing} construiu diversos tesauros distribucionais com diferentes configurações de filtros de contexto, scores de associação e medidas de similaridade e os comparou entre si e com tesauros construidos com base na WordNet \cite{fellbaum1998wordnet}. Os autores concluíram que o uso de \textit{thresholds} de frequencia das palavras em um corpus reduz o ruído causado pela distribuição zipfiana destas. 

Os resultados deste estudo mostraram que um \textit{threshold} maior pode aumentar a concordância entre diferentes tesauros, a custo de uma queda na sua cobertura, além de apontarem que a escolha do método de associação entre PMI e a PMI normalizada (nPMI) não implica em uma diferença prática sensível no resultado.

Além disso, os resultados apresentaram uma concordância entre as medidas de similaridade analisadas (distância de Lin, distância de Lin normalizada e coseno) em um teste de avaliação semântica. Os autores acreditam que este resultado se deve à natureza do teste, posto que este não limita o número de vizinhos analisados.

Em outro estudo, \citet{padronothing} analisaram diversos filtros de contexto em tesauros de verbos e mostraram que thresholds de frequência e relevância melhoram a qualidade intrínsica e extrínsica de um tesauro, mas em compensação diminuem sua cobertura. Os resultados deste estudo mostraram também que \textit{thresholds} maiores melhoram os resultados no teste realizado para os verbos de frequencia média e alta, porém pioram os resultados de verbos de baixa frequência.

Os autores revelaram também que resultados competitivos podem ser obtidos utilizando apenas os p contextos mais frequentes de cada palavra para gerar o tesauro,  e que filtros mais sofisticados como LMI não aumentam a qualidade do tesauro em relação a frequência, que é um filtro de aquisição mais simples.

Apesar da crescente popularização dos modelos preditivos, \citen{lebret2015computational} afirmam que ainda se pode obter resultados competitivos utilizando modelos de contagem, e que treinar uma rede neural pode ser muito computacionalmente custoso e que pode ser difícil encontrar os hyperparâmetros ótimos para o modelo. Outra vantagem apontada é que modelos de contagem são facilmente extensíveis, bastando adicionar as contagens das novas palavras a serem adicionadas, enquanto o modelo preditivo exige novo treinamento.

Os autores, compararam diferentes medidas de associação, métodos de redução de dimensionalidade e de aquisição de contextos, e encontraram três pontos de impacto na extração de semântica de corpora: (1) é necessário janela de contexto grande; (2) palavras raras devem constar no dicionário de contextos, não apenas as mais frequentes; (3) o método de redução de dimensionalidade, sendo um novo método, o \textit{Stochastic Low-Rank Aproximation} (SLRA), apresentado pelos autores.

\section{Modelo preditivo}

\citet{mikolov2010recurrent} desenvolveu um modelo semântico de linguagem baseado em redes neurais recorrentes (RNR), e um \textit{toolkit}\footnote{Disponível em: http://rnnlm.org/} \cite{mikolov2011rnnlm} para este modelo. A linguagem é representada através de vetores de palavras contendo a probabilidade destas de acompanharem os contextos associados a elas.

\citen{mikolov2013linguistic} explica seu modelo com base na figura \ref{fig:rnr}:

\begin{figure}[h]
    \caption{Rede neural recorrente de utilizada no modelo preditivo.}
    \begin{center}
	\includegraphics[scale=0.8]{neural_network}
    \end{center}
    \label{fig:rnr}
    \legend{Fonte: \citen{mikolov2013linguistic}}
\end{figure}

\begin{quote}
Esta arquitetura consiste de uma camada de entrada, uma camada oculta com conexões recorrentes e as matrizes de pesagem correspondentes. O vetor de entrada w(\textit{t}) representa a palavra recebida em um tempo \textit{t} codificada utilizando a codificação 1-de-N, e a camada de saída y(\textit{t}) produz uma distribuição probabilistica sobre as palavras. A camada oculta s(\textit{t}) mantém a representação do do historico de sentenças. O vetor de entrada w(\textit{t}) e o vetor de saída y(\textit{t}) tem a mesma dimensionalidade do vocabulario. Os valores nas camadas oculta e de saída são computados da seguinte forma:

$ s(t) = f(\textbf{Uw}(t)) + \textbf{Ws}(t-1))) $

$ \textbf{y}(t) = g(\textbf{Vs}(t)), $

onde

$ f(z) = \frac{1}{1+e^{-z'}}, \: \: \: \: g(z_m) = \frac{e^{z_m}}{\sum_k e^{z_k}}. $

Neste framework, as representações de palavras sao encontradas nas coluna de U, com cada coluna representando uma palavra. A RNR é treinada com retro-propagação para maximizar a \textit{log-likelihood} dos dados no modelo. O modelo em si não tem conhecimento de sintaxe, morfologia ou semântica. Incrivelmente, o treinamento de um modelo puramente léxico para maximizar a \textit{likelihood} induz a representação de palavras com propriedades sintáticas e semânticas impressionantes.

\end{quote}

\citen{baroni2014don} avaliaram os modelos preditivo e de contagem construídos utilizando diversas configurações em um conjunto de testes semânticos, nos quais o modelo preditivo obteve resultados muito superiores aos modelos de contagem. Os autores não culpam por isso o modelo de contagem, mas sim o poder do modelo preditivo. 

Também foi concluído que os hiperparâmetros, apesar de influenciarem o resultado final, não tem grande relevância, pois a queda gerade pelo uso de parâmetros não ótimos não é grande - a não ser quando considerada a pior configuração para determinada tarefa, o que também afetou severamente os resultados do modelo de contagem.

\subsection{Avaliação}

\citen{Freitag2005New} afirmam que muitos trabalhos recentes de detecção de sinônimos tem utilizado o \textit{Test of English as a Foreign Language} (TOEFL) como benchmark, e que este teste tem problemas de confiabilidade (em sua versão com 300 questões, um resultado de 82\% de acerto tem um score precisão de mais ou menos 4\% com um intervalo de confiança de 95\%). Os autores, com base no TOEFL desenvolveram o \textit{Wordnet-based synonymy test} (WBST), um teste mais amplo, com cerca de 10.000 perguntas.

\begin{figure}[h]
    \caption{Exemplo de perguntas do WBST}
    \begin{center}
	\includegraphics[scale=0.80]{perguntas_wbst}
    \end{center}
    \label{fig:estrutura}
    \legend{Fonte: \cite{Freitag2005New}}
\end{figure}

As perguntas e respostas foram geradas utilizando o North American News Corpus, selecionando como candidatas palavras com mais de mil ocorrências no corpus e que também constassem na WordNet - qualquer par de candidatas que estivesse relacionado na wordnet era selecionado como pergunta/resposta, enquanto as alternativas incorretas foram selecionadas randomicamente.

Devido à falta de métodos para avaliar tesauros distribucionais para o português, \cite{wilkens2015} propuseram o \textit{Brazilian BabelNet- based Semantic Gold Standard}, um teste similar ao TOEFL que cobre não apenas sinonímia, mas também antonímia, hiperonímia, hoponímia entre outras relações semânticas.  

Os autores utilizaram este teste para avaliar tesauros distribucionais construídos utilizando o modelo de contagem e preditivo, obtendo resultados compatíveis com os de \citen{baroni2014don}, obtendo um resultado superior com o modelo de contagem.

Devido à falta de métodos para avaliar tesauros distribucionais para o português, \cite{wilkens2015} propuseram o \textit{Brazilian BabelNet-based Semantic Gold Standard}, um teste similar ao TOEFL que cobre não apenas sinonímia, mas também antonímia, hiperonímia, hoponímia entre outras relações semânticas.  

Os autores utilizaram este teste para avaliar tesauros distribucionais construídos utilizando o modelo de contagem e preditivo, obtiveram um resultado superior com o modelo de contagem, o que é compatível com os resultados de \citen{baroni2014don}