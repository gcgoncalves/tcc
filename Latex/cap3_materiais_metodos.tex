%Materiais e Métodos
\chapter{Materiais e Métodos}

Neste capítulo são descritas as técnicas de construção automática de tesauros a partir de um corpus, bem como as medidas de similaridades e métodos de simplificação léxica utilizados. Foram construídos tesauros utilizando diferentes setups - \numThesEn{} para a língua inglesa, cujos desempenhos foram comparados com o de tesauros construídos manualmente e entre si, e \numThesPt{} para a lingua portuguesa, cujo desempenho foi comparado com os demais tesauros construídos automaticamente.

\section{Corpora}

Um corpus é um conjunto de textos utilizado para estudar e descrever uma linguagem. Como o tamanho do corpus tem impacto na performance de muitas tarefas em PLN, procura-se utilizar o maior corpus possível. Diversas abordagens para geraçnao de corpora utilizam a web como fonte de dados. Neste sentido a metodologia WaCky (Web as Corpus Kool Yinitiative) prove uma forma rápida e simples de criação de grandes corpora, tendo sido usada para gerar corpora com mais de um bilhão de palavras para diversos idiomas - inglês, alemão, italiano, entre outros. 

Corpora WaCKY são construídos seguindo 4 passos  \cite{baroni2009wacky}:
\begin{enumerate}
  \item \textbf{Coleta de URLs:} para garantir a variedade de conteúdo, bigramas gerados pela seleção aleatória de palavras de conteúdo de média frequência da linguagem alvo são enviados a um mecanismo de busca. Para o inglês, 2000 bigramas foram utilizados (e.g.: \textit{iraq package, soil occurs, elsewhere limit}), para o alemão 1653 e para o italiano, 1000. Para cada bigrama, um máximo de 10 URLs foi extraído. Destas, duplicatas foram removidas e  as restantes enviadas randomicamente a um crawler restrito a páginas do idioma relevante.
  \item \textbf{Limpeza pós-crawling:} Apenas páginas de tamanho médio são mantidas, e duplicatas são removidas. As páginas restantes recebem uma limpeza para eliminar código e elementos de \textit{boilerplates} e então são filtradas de acordo com a densidade de palavras (cada página deve ter ao menos dez tipos, trinta tokens e 25\% de palavras de conteúdo).
  \item \textbf{Detecção e remoção de quase-duplicatas:} contando uma sobreposição de n-gramas, utilizando 25 5-gramas entre cada par de documentos.
  \item \textbf{Anotação:} Informações adicionais como \textit{part-of-speech} e parseamento são inseridas no texto.
\end{enumerate}

Seguindo a metodologia WaCky, neste trabalho foram utilizados os corpora PukWaC \cite{johansson2008dependency} para o inglês e e brWaC \cite{boos2014brwac}  para o português. 

\subsection{PukWaC}

O PukWaC é um corpus da lingua inglesa construído com textos da web encontrados no domínio .uk, e possui mais de dois bilhões de palavras. Suas sentenças foram anotadas utilizando o MaltParser, que também gerou suas árvores de dependência.

As sentenças do PukWaC são dispostas conforme a tabela \ref{tbl:exemplo_pukwac}:

\begin{quote}
\begin{table}[h]
    \caption{Sentença ``\textit{Not many prawn sandwiches were eaten.}''  extraída do PukWaC pelo MaltParser}
    \begin{center}
	\begin{tabular}{llllll}
<s> \\
Not  &  not  &  RB  &  1  & 4  &  NMOD \\
many  &  many  &  JJ  &  2  &  4  &  NMOD \\
prawn  &  prawn  &  NN  &  3  &  4  &  NMOD \\
sandwiches  &  sandwich  &  NNS  &  4  &  5  &  SBJ \\
were  &  be  &  VBD  &  5  &  0  &  ROOT \\
eaten  &  eat  &  VVN  &  6  &  5  &  VC \\
.  &  .  &  SENT  &  7  &  5  &  P \\
</s>
	\end{tabular}
    \end{center}
    \label{tbl:exemplo_pukwac}
\end{table}

\end{quote}

A árvore de dependência da frase disposta na tavela \ref{tbl:exemplo_pukwac} pode ser representada graficamente como na figura \ref{fig:arvore}.

\begin{figure}[h]
    \caption{Árvore de dependência da frase ``\textit{Not many prawn sandwiches were eaten.}" pelo MaltParser}
    \begin{center}
	\includegraphics{dep_tree}
    \end{center}
    \label{fig:arvore}
    \legend{Fonte: Os autores}
\end{figure}

\subsection{brWaC}

O brWaC é um corpus da lingua portuguesa construído com textos da web encontrados no domínio .br, e possui mais de três bilhões de palavras. Suas sentenças receberam anotação de rótulo morfosintático do TreeTagger \cite{schmid1995improvements}.

As sentenças do brWaC são dispostas conforme a tabela \ref{tbl:exemplo_brwac}:

\begin{quote}

\begin{table}[h]
    \caption{Sentença ``\textit{Vamos avaliar este ponto de vista.}''  extraída do brWaC pelo TreeTagger}
    \begin{center}
	\begin{tabular}{lll}
		Vamos	& I		&	vamos \\
		avaliar	& V		&	avaliar \\
		esse		& DET	&	esse \\
		ponto	& NOM	&	ponto \\
		de		& PRP	&	de \\
		vista		& NOM	& 	vista \\
		.   SENT    . 	
	\end{tabular}
    \end{center}
    \label{tbl:exemplo_brwac}
\end{table}

\end{quote}

\section{Construção dos tesauros}

Para a construção dos tesauros, foram utilizados dois métodos, o de contagem e o preditivo. Para ambos os métodos foi utilizado o mesmo pré-processamento para identificar os contextos das palavras dos corpora.

\subsection{Pré-processamento e filtragem}

 São selecionados critérios para definir as palavras que irão compor o tesauro, e extraídas de um corpus as palavras que se encaixam nestes critérios juntamente com os contextos em que elas aparecem, em pares palavra-contexto.
 
Dois tipos de seleção de contexto foram utilizados: palavras com relação de dependência com o alvo e palavras à volta do alvo como uma bag-of-words (BoW), isto é, uma janela de até n palavras de contexto antes e depois do alvo dentro de uma mesma sentença, conforme demonstrado na figura \ref{fig:bow}. Para este trabalho foram gerados tesauros utilizando dois tamanhos de janela, 5 e 10. Foram descartadas palavras com ocorrência menor do que 10 no corpus.

\begin{figure}[h]
    \caption{BoW de tamanho 3 da palavra \textit{galaxy}.}
    \begin{center}
	\includegraphics[scale=0.60]{join_me}
    \end{center}
    \label{fig:bow}
\end{figure}

Desta forma uma tupla de contexto \textit{(p, c, t)} é a ocorrência de palavra \textit{p}, contexto \textit{c} e marcação \textit{t}, e o número de ocorrências de uma tupla de contexto em um corpus é representado por $|$$|$\textit{p, c, t}$|$$|$. Por exemplo, analisando os verbos, a frase \textit{“O menino chutou a bola”} gera duas tuplas: (\textit{p = “chutar”, c = “menino”, t = “substantivo”}) e (\textit{p = “chutar”, c = “bola”, t = “substantivo”}). São filtradas as tuplas que possuem $|$$|$\textit{p, c, t}$|$$|$ < 2. 

\subsection{Modelo de contagem}\label{contagem}

Os tesauros baseados no modelo de contagem foram construidos utilizando duas ferramentas de análise distribucional: Minimantics\footnote{Disponível em http://github.com/ceramisch/minimantics/}, e DISSECT\footnote{Disponível em http://clic.cimec.unitn.it/composes/toolkit/index.html} (DIStributional Semantics Composition Toolkit), seguindo as indicações estudadas em \cite{padronothing}.

As tuplas identificadas na etapa anterior são utilizadas para calcular a \textit{pointwise mutual information} (PMI) entre as palavras e seur respectivos contextos. Somente foram utilizados os contextos que possuem um PMI maior do que zero. Para cada palavra foi gerado um vetor com as PMIs entre elas e seus contextos, e estes vetores formam um espaço vetorial que pode ser interpretado como um espaço semântico (figura \ref{fig:espaco}), onde cada posição representa um significado. 

\begin{figure}[h]
    \caption{Vetores de palavras formando um espaço semântico}
    \begin{center}
	\includegraphics{vetores}
    \end{center}
    \label{fig:espaco}
    \legend{Fonte: Os autores}
\end{figure}

É calculado então o cosseno entre estes vetores (figura \ref{fig:angulos}). Quanto mais próximo de 1 for o cosseno entre dois vetores de palavras (ou seja: quanto menor for a distância entre estes vetores no espaço), maior a similaridade entre elas.

\begin{figure}[h]
    \caption{Angulos entre vetores de palavras}
    \begin{center}
	\includegraphics[scale=0.8]{angulos}
    \end{center}
    \label{fig:angulos}
    \legend{Fonte: Os autores}
\end{figure}

%\begin{figure}[h]
%    \caption{Vetores em um espaço semântico}
%    \begin{center}
%	\includegraphics[scale=0.8]{espaco_semantico}
%    \end{center}
%    \label{fig:vetores}
%    \legend{Fonte: Os autores}
%\end{figure}

O processo de construção de tesauros utilizando o modelo de contagem pode melhor visualizado na figura \ref{fig:processo_contagem}.

\begin{figure}[h]
    \caption{Processo do modelo de contagem.}
    \begin{center}
	\includegraphics[scale=0.8]{pipe_minimantics}
    \end{center}
    \label{fig:processo_contagem}
    \legend{Fonte: Os autores}
\end{figure}

\subsection{Modelo preditivo}\label{preditivo}

A construção de tesauros através de modelos preditivos também inicia-se com a construção de vetores, porém na ordem inversa: ao invés de pesar os contextos e gerar vetores, a pesagem é feita diretamente durante a contrução dos vetores para melhor prever os contextos em que as palavras correspondentes tendem a aparecer. São utilizadas técnicas de redes neurais, que analisam a probabilidade de uma palavra ocorrer em determinado contexto de maneira otimizada \cite{baroni2014don}. 

A ferramenta utilizada foi o Word2Vec\footnote{Disponível em: https://code.google.com/p/word2vec/}, que permite dois tipos de aquisição de contexto: o skip-gram e o continuous bag of words (CBOW). Neste trabalho, o modelo utilizado foi o CBOW, por ser mais eficiente em grandes corpora \cite{mikolov2013efficient}.

\section{Avaliação}

Os tesauros distribucionais foram aplicados em uma série de testes para verificar sua performance em diversas tarefas, como identificação de composicionalidade, sinonimia, entre outras correlações semânticas. São estas:

\textbf{Identificação de similaridade:} consiste em identificar palavras semanticamente associadas (por exemplo, tigre e animal, peixe e oceano e areia e praia). Os principais \textit{gold standards} desta tarefa foram construídos manualmente. O \textit{data set} mais antigo, \textit{Rubenstein and Goodenough} (RG) \cite{rubenstein1965contextual}, contém 65 pares de substantivos; o WordSim353 \cite{finkelstein2001placing} contém 353 pares; os \textit{data sets} WordSim-353-sim e WordSim-353-rel \cite{agirre2009study} contém 203 e 252 pares, respectivamente; o \textit{Miller and Charles} (MC)\cite{miller1991contextual} contém 30 pares; o MEN \cite{bruni2014multimodal} possui 3000 pares; e o SCWS \cite{huang2012improving}  possui 2003 pares com sentenças de contexto. 

\textbf{Detecção de relações:} esta tarefa consiste em classificar as palavras segundo uma determinada relação (por exemplo, sinonímia). A construção destes recursos pode ser simplificada com o uso de recursos como a WordNet, devido as relações entre palavras já estar explicita. O teste WBST \cite{Freitag2005New} é composto de um conjunto de palavras e quatro alternativas de sinônimos, sendo apenas uma correta - o sinônimo mais próximo do significado original da palavra ao qual ele está associado. O WBST e possui quatro \textit{data sets} distintos: um para substantivos, com 9887 perguntas; um para verbos, com 7398 perguntas; um para adjetivos, com 5824 perguntas; e um para advérbios, com 461 perguntas.

\textbf{Compocisionalidade:} 

\citen{reddy2011dynamic} utilizaram operações vetoriais para extrair informações de composicionalidade semântica de modelos distribucionais. Os autores mostraram que simples adição vetorial não é suficiente, pois há muito ruído envolvido. Como por exemplo, na expressão \textit{house hunting}, que possui os vetores constituintes (Tabela \ref{tbl:vetores_reddy}):

\begin{table}[h]
    \caption{Vetores das palavras constituintes do composto \textit{house hunting}}
    \begin{center}
	\begin{tabular}{lllllll}
	\cline{2-7}
        	          & {\it Animal} & {\it Buy} & {\it apartment} & {\it Price} & {\it Rent} & {\it Kill} \\ \hline
		{\bf \it House}   & 30           & 60        & 90              & 55          & 45         & 10         \\
		{\bf \it Hunting} & 90           & 25        & 12              & 20          & 33         & 90   
	\end{tabular}
    \end{center}
    \label{tbl:vetores_reddy}
    \legend{Fonte: \cite{reddy2011dynamic}}
\end{table}

O vetor resultante da soma destes constituintes possui alta relevância das palavras \textit{Animal} e \textit{Kill}, e portanto não representa o verdadeiro significado de \textit{house hunting} (que significa procurar uma casa para compra ou aluguel). 

Os autores sugerem que a solução para este problema envolve dois passos:

\begin{enumerate}
	\item Construir vetores separados para cada sentido das palas constituintes.
	\item Selecionar os vetores relevantes destas palavras para o composto, e então realizar a composição utilizando estes vetores.
\end{enumerate}

Para isto, são sugeridos dois métodos: o primeiro aplica \textit{Word Sense Induction} (WSI) para identificar diferentes sentidos e o segundo representa cada palavra constituinte com um vetor construído dinamicamente ativando apenas os contextos relevantes para o composto na presença da outra constituinte.
    
Ambos os métodos foram avaliados utilizando um teste de composicionalidade. Os resultados deste indicaram uma larga vantagem do segundo método sobre o primeiro, mostrando a impostância da escolha de vetores relevantes na composicionalidade. Foi mostrado também que a multiplicação de vetores atinge melhores resultados do que a soma destes para representação de composicionalidade.

Com base neste experitmento, \citet{villavicencio2015} desenvolveram um teste de composicionalidade para tesauros distribucionais com 90 substantivos compostos  anotados manualmente.

%\begin{tikzpicture} 
%    \begin{axis}[enlargelimits=false,
%    scatter/classes={
%        L={mark=*,draw=blue},
%        N={mark=*,draw=red}}] 
%    \addplot+[scatter,only marks]
%    table[meta=label] 
%    {color.dat}; 
%    \end{axis} 
%\end{tikzpicture}