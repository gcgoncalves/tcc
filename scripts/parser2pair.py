## -*- coding: utf-8 -*-
import argparse
import re
import pyprind
import subprocess
from copy import deepcopy

parser = argparse.ArgumentParser(description='Transform a corpus on a coocurrence \
                                            triple in the form w1 \\t w2 \\t frequency.')
parser.add_argument('-f', type=str, help='corpus file')
parser.add_argument('-l', action='store_true', default=False, help='use unlemmatized words')
parser.add_argument('-i', type=str, help='file with the interest Parts of Speech separated by comma')
parser.add_argument('-t', type=str, help='file with the punctuations separated by comma')
parser.add_argument('-o', type=str, help='output file name', default='triples.csv')
args = parser.parse_args()

interest_list = ['nn', 'jj', 'rb', 'vv']
target_list = ['nn']
relation_list = ['nmod', 'pmod', 'coord', 'dep', 'prn', 'vc', 'sbj', 'obj', 'prd', 'prn', 'exp', 'root']

if args.l:
    word_index = 0
else:
    word_index = 1

def getRelatedWord(word, other_word, sentence):
    #word = [0: lexema, 1: PoS, 2: id, 3: id_relacao, 4: tipo_relacao]
    if other_word[1] in interest_list:
        return other_word
    else:
        for third_word in sentence:
            if third_word[3] == other_word[2]:
                if third_word and third_word[1] in interest_list:
                    return third_word
#                elif not third_word[1] in interest_list:
#                    return getRelatedWord(other_word, third_word, sentence)

def list_contains(a_list, search_item):
    for element in a_list:
        if element in search_item:
            return True
    return False

def points_to(a, b):
    #word = [0: lexema, 1: PoS, 2: id, 3: id_relacao, 4: tipo_relacao]
    pointers = a[3].split('X')
    targets = b[2].split('X')
    if len(pointers+targets) != len(set(pointers+targets)):
        return True
    return False

#def old_points_to(a, b):
#    #word = [0: lexema, 1: PoS, 2: id, 3: id_relacao, 4: tipo_relacao]
#    pointers = a[3].split('X')
#    targets = b[2].split('X')
#    for pointer in pointers:
#        if pointer in targets:
#            return True
#    return False

def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def getInterest():
    interest_file = open(args.sw)
    interest_list = interest_file.readline()
    interest_list = interest_list.split(',')
    interest_file.close()
    return interest_list

def getTarget():
    target_file = open(args.pos)
    target_list = target_file.readline()
    target_list = target_list.split(',')
    target_file.close()
    return target_list

if args.i:
    interest_list = getInterest()
if args.t:
    target_list = getTarget()

print("counting file lines. this may take a while.")
line_cnt = file_len(args.f)
print("done.")

# instantiating new progress bar object
my_bar = pyprind.ProgBar(line_cnt - 1)

corpus = open(args.f, 'r')
output_file = open(args.o, 'w')

sentence = []
rels = []
is_word = re.compile('^[a-zX]+([_-][a-zX]+)*$', re.UNICODE)
for line in corpus:
    if '</s>' in line:
	#word = [0: word, 1: PoS, 2: id, 3: relation_id, 4: relation_type]
        for word in sentence:
            if list_contains(target_list, word[1]):
                for other_word in sentence:
                    if points_to(other_word, word):
                        output_file.write(word[0] + " " + other_word[0] + "--" + other_word[1][0] + "\n")
                        #output_file.write(word[0] + " " + other_word[0] + "\n")
                    elif points_to(word, other_word) and not list_contains(target_list, other_word[1]):
                        output_file.write(word[0] + " " + other_word[0] + "--" + word[1][0] + "\n")
                        #output_file.write(word[0] + " " + other_word[0] + "\n")
        sentence = []
    elif not line[0] == '<':
	#line = [0: word, 1: lemma, 2: PoS, 3: id, 4: relation_id, 5: relation_type]
        line = line[0:-1].lower().replace(' ', 'X').split('\t')
        if len(line) == 6 and len(line[1]) < 50 and is_word.match(line[1]) and \
                list_contains(interest_list, line[2]) and list_contains(relation_list, line[5]):
            sentence.append((line[word_index], line[2],  line[3], line[4], line[5]))
	# update the progress bar
    my_bar.update()


corpus.close()
output_file.close()

print("done.")

