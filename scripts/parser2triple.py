## -*- coding: utf-8 -*-
import argparse
import sys
from copy import deepcopy

parser = argparse.ArgumentParser(description='Transform a corpus on a coocurrence \
											triple in the form w1 \\t w2 \\t frequency.')
parser.add_argument('-f', type=str, help='corpus file')
parser.add_argument('-i', type=str, help='file with the interest Parts of Speech separated by comma')
parser.add_argument('-t', type=str, help='file with the punctuations separated by comma')
parser.add_argument('-o', type=str, help='output file name', default='triples.csv')
args = parser.parse_args()

interest_list = ['VV', 'VVD', 'VVG', 'VVN', 'VVP', 'VVZ', 'NN', 'NNS', 'NP', 'NPS']
target_list = ['VV', 'VVD', 'VVG', 'VVN', 'VVP', 'VVZ']
relation_list = ['OBJ', 'SBJ']

if args.i:
	interest_list = getInterest()
if args.t:
	target_list = getTarget()

corpus = open(args.f, 'r')

print('Reading corpus and removing stopwords.')
total_words = 0
sentence_list = []
sentence = []
for line in corpus:
    if '</s>' in line:
        sentence_list.append(deepcopy(sentence))
        total_words += len(sentence)
        sentence = []
    elif not line[0] == '<':
        line = line.split('\t')
        if line[2] in interest_list:
            sentence.append((line[0], line[2],  line[3], line[4], line[5]))
            #sentence.append((line[1].lower(), line[2],  line[3], line[4], line[5]))
print('done')
corpus.close()

progress_counter = 0
print("total_words:", total_words)
print("total_sentences:", len(sentence_list))

triple = dict()
for sentence in sentence_list:
    for word in sentence:
        if word[1] in target_list:
            if not word[0] in triple:
                triple[word[0]] = dict()
            for other_word in sentence:
                if other_word[3] == word[2]:
                    if other_word[0] in triple[word[0]]:
                        triple[word[0]][other_word[0]][0] += 1
                    else:
                        triple[word[0]][other_word[0]] = [1, other_word[4]]
        progress_counter += 1
        print("\r%.2f%%" % ((100*progress_counter)/total_words))
        sys.stdout.flush()
#print("\r100%\n")
#sys.stdout.flush()

output_file = open(args.o, 'w')
for w1 in triple:
    for w2 in triple[w1]:
        output_file.write(w1+'\t'+w2+'\t'+str(triple[w1][w2][0])+'\t'+str(triple[w1][w2][1])) #+'\n')
output_file.close()

def getInterest():
    interest_file = open(args.sw)
    interest_list = interest_file.readline()
    interest_list = interest_list.split(',')
    interest_file.close()
    return interest_list

def getTarget():
    target_file = open(args.pos)
    target_list = target_file.readline()
    target_list = target_list.split(',')
    target_file.close()
    return target_list