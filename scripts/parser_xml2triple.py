## -*- coding: utf-8 -*-
import argparse
import sys
import pyprind
from copy import deepcopy

parser = argparse.ArgumentParser(description='Transform a corpus on a coocurrence \
                                            triple in the form w1 \\t w2 \\t frequency.')
parser.add_argument('-f', type=str, help='corpus file')
parser.add_argument('-i', type=str, help='file with the interest Parts of Speech separated by comma')
parser.add_argument('-t', type=str, help='file with the punctuations separated by comma')
parser.add_argument('-o', type=str, help='output file name', default='triples.csv')
args = parser.parse_args()

interest_list = ["jj", "jjr", "jjs", "rb", "rbr", "rbs", "in", "vv", "vvd", "vvg", "vvn", "vvp", "vvz"]
target_list = ['nn', 'nns', 'np', 'nps']
#relation_list = ['obj', 'sbj']

def getRelatedWord(word, other_word, sentence):
    if other_word[1] in interest_list:
        return other_word
    else:
        for third_word in sentence:
            if third_word[3] == other_word[2]:
                if third_word and third_word[1] in interest_list:
                    return third_word
                else: 
                    return getRelatedWord(other_word, third_word, sentence)

def getInterest():
    interest_file = open(args.sw)
    interest_list = interest_file.readline()
    interest_list = interest_list.split(',')
    interest_file.close()
    return interest_list

def getTarget():
    target_file = open(args.pos)
    target_list = target_file.readline()
    target_list = target_list.split(',')
    target_file.close()
    return target_list

if args.i:
    interest_list = getInterest()
if args.t:
    target_list = getTarget()

corpus = open(args.f, 'r')

print('Reading corpus and removing stopwords.')
total_words = 0
sentence_list = []
sentence = []
for line in corpus:
    if '</s>' in line:
        sentence_list.append(deepcopy(sentence))
        total_words += len(sentence)
        sentence = []
    elif not line[0] == '<':
        line = line[0:-1].lower().split('\t')
        sentence.append((line[1], line[2],  line[3], line[4], line[5]))
print('done')
corpus.close()

progress_counter = 0
progbar = pyprind.ProgBar(total_words, title="processing words.")

triple = dict()
for sentence in sentence_list:
    for word in sentence:
        if word[1] in target_list:
            if not word[0] in triple:
                triple[word[0]] = dict()
            for other_word in sentence:
                if other_word[3] == word[2]:
                    related_word = getRelatedWord(word, other_word, sentence)
                    if related_word and related_word[4] in relation_list:
                        if related_word[0] in triple[word[0]]:
                            triple[word[0]][related_word[0]][0] += 1
                        else:
                            triple[word[0]][related_word[0]] = [1, related_word[4]]
        progress_counter += 1
        progbar.update(item_id="%s" %progress_counter)

output_file = open(args.o, 'w')
for w1 in triple:
    for w2 in triple[w1]:
        output_file.write(w1+'\t'+w2+'\t'+str(triple[w1][w2][0])+'\t'+str(triple[w1][w2][1])+'\n')
output_file.close()