relation_file = open('min_pukwac_pairs.csv', 'r')
relations = dict()
keys = []
for line in relation_file:
    position = line.rfind("--")
    relation = line[position+2:]
    if not relation in relations:
        relations[relation] = 1
        keys.append(relation)
    else:
        relations[relation] += 1
for key in keys:
    print str(relations[key])+": "+key[:-1]
