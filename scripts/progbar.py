import time
import sys

for i in range(100):
    time.sleep(0.1)
    sys.stdout.flush()
    print("\r%.2f%%" % (100*i/100))
    sys.stdout.flush()
print("\r100%\n")
sys.stdout.flush()